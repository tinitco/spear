(function () {
  'use strict';

  angular
    .module('analysis-realigns.services')
    .factory('AnalysisRealignsService', AnalysisRealignsService)
    .factory('AnalysisRealignsSearchService', AnalysisRealignsSearchService);

  AnalysisRealignsService.$inject = ['$resource'];

  function AnalysisRealignsService($resource) {
    return $resource('./api/analysis/:annRelId', { 
      annRelId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }

  AnalysisRealignsSearchService.$inject = ['$resource'];

  function AnalysisRealignsSearchService($resource) {
    return $resource('./api/analysisSearch/:annRelId', { 
      annRelId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
})();
