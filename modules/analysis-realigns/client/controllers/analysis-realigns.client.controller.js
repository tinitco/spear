(function () {
  'use strict';

  angular
    .module('analysis-realigns')
    .controller('AnalysisRealignsController', AnalysisRealignsController);
    // .config(function($mdDateLocaleProvider) {
    //   $mdDateLocaleProvider.formatDate = function(date) {
    //     // return moment(date).format('ddd, M/D/YYYY');
    //   };
    // });

  AnalysisRealignsController.$inject = ['$scope', '$attrs', '$state', 'Authentication',
   'AnalysisRealignsService', '$mdDialog', '$mdMedia', 'moment'];

  function AnalysisRealignsController($scope, $attrs, $state, Authentication, AnalysisRealignsService,
    $mdDialog, $mdMedia, moment) {

    $scope.type = $attrs.type;
    $scope.relative = '';
    if($attrs.prev){ $scope.relative= 'prev'; }

    $scope.monthsRange =[0,1,2];

    var vm = this;
    vm.spear = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    $scope.type = 'day';
    $scope.quarterType = 1;
    $scope.ana_res = [];
    $scope.inserted = null;
    $scope.quarterTypes = ['quarterMo1', 'quarterMo2', 'quarterMo3'];

    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.$on('spearChanged', function (event,data) {
      vm.class = data.type;
      $scope.type = data.type;
      if($scope.type === 'quarter') $scope.quarterType = 1;
      var spearId = data.spear_id;
      if ($scope.relative === 'prev'){
        $scope.ana_res = [];
        $scope.emptyAnnRelInContaiers($scope);
        $scope.inserted = null;
        $scope.spear = null;
        spearId = data.prevSpearId;
      }
      if(spearId){
        $scope.ana_res = AnalysisRealignsService.query({ spear:spearId }, function(){
          $scope.spear = data.spear_id;
          $scope.inserted = null;
          $scope.emptyAnnRelInContaiers($scope);
          $scope.processResults();
        });
      }
    });

    $scope.processResults = function () {
      if($scope.type === 'quarter'){
        $scope.months=[];
        for(var $i=1;$i<4;$i++){
          var $t = [];
          $t.month = $i;
          $scope.segregateAnnRel($t,$i);
          $scope.months.push($t);
        }
        $scope.quarterAnnRelChanged($scope.quarterType);
      }else{
        $scope.segregateAnnRel($scope);
      }
    };

    $scope.emptyAnnRelInContaiers = function($container){
      $container.realigns = [];
      $container.analyses_success = [];
      $container.analyses_fail = [];
    };

    $scope.segregateAnnRel = function($container, $month){
      $month = typeof $month !== 'undefined' ? $month : null;
      $container.realigns = $.grep($scope.ana_res, function( a ) {
        return ( a.type === 'realign' && a.month === $month );
      });
      $container.analyses_success = $.grep($scope.ana_res, function( a ) {
        return ( a.type === 'analysisSuccess' && a.month === $month );
      });
      $container.analyses_fail = $.grep($scope.ana_res, function( a ) {
        return ( a.type === 'analysisFailure' && a.month === $month );
      });
    };

    $scope.addEditAnnRelDialog = function(annRel){
      $mdDialog.show({
        controller: NewAnnRelDialogController,
        templateUrl: 'modules/analysis-realigns/client/views/edit-annRel.client.template.html',
        parent: angular.element(document.body),
        locals:{
          annRel: annRel,
        },
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(todo_data) {
        $scope.updateAnnRel(annRel);
      }, function(){
        $scope.updateAnnRel(annRel);
      });
    };

    $scope.updateAnnRel = function(annRel,data){
      //console.log(annRel);
      if(annRel === $scope.inserted){
        if(annRel.text !== ""){
          annRel.$save({},function(){
            $scope.inserted = '';
            // append the returned
            return true;
          }, function(err){
            // notification.error('Problem saving Analysis realign' );
            // $scope.inserted
          });
        }else{
          removeAnnRelFromList(annRel);
          $scope.inserted = null;
        }
      }else{
        if(data === ""){
          $scope.deleteAnnRel(annRel);
        }else{
          annRel.$update({},function(){
            return true;
          });
        }
      }
    };

    $scope.checkLength = function(container){
      if (container && container.length<=3){return true;}
      return false;
    };

    $scope.showLimitDialog = function(Title, limit){
      $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Too Many '+ Title)
            .textContent('Sorry you cannot have more than '+limit +' '+Title+' in a SPEAR.')
            .ariaLabel('Too Many '+ Title)
            .ok('OK')
        );
    };

    $scope.addFail = function(month, data){
        if($scope.inserted){return;}
        // if($scope.analyses_fail && $scope.analyses_fail.length >= 3){
        //   $scope.showLimitDialog('Failures', 3);
        //   return;
        // }
        $scope.inserted = $scope.createNewAnalysis(false);
        if($scope.type === 'quarter'){
            $scope.inserted.month = $scope.quarterType;
            $scope.months[$scope.quarterType-1].analyses_fail.push($scope.inserted);
        }else{
            $scope.analyses_fail.push($scope.inserted);
        }
        $scope.addEditAnnRelDialog($scope.inserted);
    };
    $scope.addSuccess = function(month, data){
        if($scope.inserted){return;}
        // if($scope.analyses_success && $scope.analyses_success.length >= 3){
        //   $scope.showLimitDialog('Successes', 3);
        //   return;
        // }
        $scope.inserted = $scope.createNewAnalysis(true);
        if($scope.type === 'quarter'){
            $scope.inserted.month = $scope.quarterType;
            $scope.months[$scope.quarterType-1].analyses_success.push($scope.inserted);
        }else{
            $scope.analyses_success.push($scope.inserted);
        }
        $scope.addEditAnnRelDialog($scope.inserted);
    };

    $scope.createNewAnalysis=function($success){
      var $ann = new AnalysisRealignsService();
      $ann.id = '';
      $ann.spear = $scope.spear;
      $ann.type = $success?"analysisSuccess":"analysisFailure";
      // $ann.analysis_success = $success?"1":"0";
      $ann.text =  "";
      return $ann;
    };

    $scope.addRealign = function (month,data) {
        if($scope.inserted){return;}
        // if($scope.realigns && $scope.realigns.length >= 6){
        //   $scope.showLimitDialog('Realigns', 6);
        //   return;
        // }
        $scope.inserted = new AnalysisRealignsService();
        // console.log($scope.inserted);
        $scope.inserted.id = '';
        $scope.inserted.spear = $scope.spear;
        $scope.inserted.type ="realign";
        $scope.inserted.text =  "";
        if($scope.type === 'quarter'){
            $scope.inserted.month = $scope.quarterType;
            $scope.months[$scope.quarterType-1].realigns.push($scope.inserted);
        }else{
            $scope.realigns.push($scope.inserted);
        }
        $scope.addEditAnnRelDialog($scope.inserted);
    };

    $scope.quarterAnnRelChanged = function(qtype){
      $scope.quarterType = qtype;
      $scope.analyses_success = $scope.months[qtype -1].analyses_success;
      $scope.analyses_fail = $scope.months[qtype -1].analyses_fail;
      $scope.realigns = $scope.months[qtype -1].realigns;
    };

    // $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
    //   if ($scope.inserted){
    //       if ($scope.inserted.type === "realign"){
    //           $(".realign-table td .editable-click").last().click();
    //       }else if ($scope.inserted.type === "analysisSuccess"){
    //         $(".analyse-table .an_success td .editable-click").last().click();
    //       }else if ($scope.inserted.type === "analysisFailure"){
    //           $(".analyse-table .an_fail td .editable-click").last().click();
    //       }
    //   }
    // });

    $scope.deleteAnnRel = function (annRel) {
      annRel.$delete({},function(){
        removeAnnRelFromList(annRel);
        return true;
      });
    };

    function removeAnnRelFromList(annRel){
      if(annRel.type === 'realign'){
        if(annRel.month > 0 ){
            $scope.months[annRel.month -1].realigns.splice( $scope.months[annRel.month-1].realigns.indexOf(annRel), 1 );
        }else{
            $scope.realigns.splice( $scope.realigns.indexOf(annRel), 1 );
        }
      }else if(annRel.type === 'analysisFailure'){
        if(annRel.month > 0 ){
            $scope.months[annRel.month-1].analyses_fail.splice( $scope.months[annRel.month-1].analyses_fail.indexOf(annRel), 1 );
        }else{
            $scope.analyses_fail.splice( $scope.analyses_fail.indexOf(annRel), 1 );
        }
      }else if(annRel.type === 'analysisSuccess' ){
        if(annRel.month > 0 ){
            $scope.months[annRel.month-1].analyses_success.splice( $scope.months[annRel.month-1].analyses_success.indexOf(annRel), 1 );
        }else{
            $scope.analyses_success.splice( $scope.analyses_success.indexOf(annRel), 1 );
        }
      }
    }

    $scope.toggleCheck = function(plan) {
      // $scope.loading = true;
      plan.active=!plan.active;
      $scope.updatePlan(plan);
    };

  }
})();

function NewAnnRelDialogController($scope, $mdDialog, annRel) {
  'use strict';

  $scope.annRel = annRel;
  $scope.aligntext = angular.copy($scope.annRel.text);

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $scope.annRel.text = $scope.aligntext;
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
