(function (app) {
  'use strict';

  app.registerModule('analysis-realigns');
  app.registerModule('analysis-realigns.services');
  app.registerModule('analysis-realigns.routes', ['ui.router', 'analysis-realigns.services']);
})(ApplicationConfiguration);
