'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Todo Schema
 */
var AnalysisRealignSchema = new Schema({
  // Todo model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  text: {
    type: String,
    trim: true,
    // required: 'Title cannot be blank'
  },
  month: {
    type: Number,
    default: null
    // required: 'Set Max Number of Users'
  },
  account: {
    type: Schema.ObjectId,
    ref: 'Account',
    required: 'Account Not set'
  },
  spear: {
    type: Schema.ObjectId,
    ref: 'Spear',
    required: 'Spear Not Set'
  },
  type: {
    type: String,
    enum: ['analysisSuccess', 'analysisFailure', 'realign'],
    default: 'analysisSuccess',
    required: 'Please provide one analysis-realign Type'
  },
  quarterType: {
    type: String,
    enum: ['quarterMo1', 'quarterMo2', 'quarterMo3', 'quarterMo4'],
    default: 'quarterMo1',
    required: 'Please provide one analysis-realign Type'
  },
  reportIn: {
    type: String,
    enum: ['null', 'week','month', 'quarter', 'annual'],
    default: 'null'
    // required: 'Please provide one analysis-realign Type'
  },
  belongsTo: {
    type: Schema.ObjectId,
    ref: 'User',
    required: 'User Not Set'
  }
});

mongoose.model('AnalysisRealign', AnalysisRealignSchema);
