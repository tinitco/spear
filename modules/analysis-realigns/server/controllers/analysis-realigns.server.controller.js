'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  moment = require('moment'),
  async = require('async'),
  Spear = mongoose.model('Spear'),  
  User = mongoose.model('User'),  
  AnalysisRealign = mongoose.model('AnalysisRealign'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));



exports.getMetricsFor = function(type, user, callback){
  var md = moment();
  // console.log('at metrics');
  Spear.find({
    'spear_type':type, 
    'begin': { '$gte': md.startOf('quarter').toISOString() },
    'end': { '$lte':  md.endOf('quarter').toISOString()},
    'belongs_to': user 
  }).populate('').exec(function (err, spear) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
    } else {
      var spearIds = spear.map(function(sp){
        return sp._id;
      });
      // console.log(spearIds);
      AnalysisRealign.find({
        'spear':{$in: spearIds},
        'belongsTo': user 
      }).populate('').exec(function (err, anaRelcount) { 
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          // console.log('till here');
          callback(null);
        }else {
          var success = anaRelcount.filter(function(anaRel){
            return anaRel.type === 'analysisSuccess';
          });
          var fail = anaRelcount.filter(function(anaRel){
            return anaRel.type === 'analysisFailure';
          });
          var realigns = anaRelcount.filter(function(anaRel){
            return anaRel.type === 'realign';
          });
          callback({'success': success.length, 'fail':fail.length,'realigns':realigns.length});  
        }
      });
    }
  });
};
/**
 * Create a 
 */
exports.create = function (req, res) {
  // console.log(req.body);
  var anaRel = new AnalysisRealign(req.body);
  // console.log(anaRel);
  anaRel.belongsTo = req.user;
  // anaRel.assignedTo = req.user;
  anaRel.account = req.user.account;

  anaRel.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(anaRel);
    }
  });
};

/**
 * Show the current 
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var anaRel = req.anaRel ? req.anaRel.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // anaRel.isCurrentUserOwner = req.user && anaRel.user && anaRel.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(anaRel);
};

/**
 * Update a 
 */
exports.update = function (req, res) {
  var anaRel = req.anaRel;
  console.log(req.user.account);
  // anaRel.
  if(!anaRel.account){anaRel.account = req.user.account;}
  anaRel.text = req.body.text;
  // anaRel.active = req.body.active;
  // anaRel.max_users = req.body.max_users;

  anaRel.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(anaRel);
    }
  });

};

exports.updateReportIn = function (req, res) {
  var anaRel = req.anaRel;
  // console.log(req.user.account);
  // anaRel.
  if(!anaRel.account){anaRel.account = req.user.account;}
  anaRel.reportIn = req.body.reportIn;
  // anaRel.active = req.body.active;
  // anaRel.max_users = req.body.max_users;

  anaRel.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(anaRel);
    }
  });

};

/**
 * Delete an 
 */
exports.delete = function (req, res) {
  var anaRel = req.anaRel;

  anaRel.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(anaRel);
    }
  });

};

/**
 * List of 
 */
exports.list = function (req, res) {
  if (req.query.spear){
    // var queryDate = moment(req.query.date).toISOString();
    AnalysisRealign.find({
      'spear':req.query.spear//,
      // 'belongsTo': req.user 
    }).populate('').exec(function (err, anaRel) {
      if (err) {
          console.log(err);
          console.log('error from list');
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else {
        res.json(anaRel);  
      }
    });

  }else if (req.query.metrics) {
    module.exports.getMetricsFor('week', req.user,function(weekMetrics){
      if (!weekMetrics){
        return res.status(400).send({message: 'cannot find metircs'});
      }else{
        module.exports.getMetricsFor('day', req.user,function(dayMetrics){
          if (!dayMetrics){
            return res.status(400).send({message: 'cannot find metircs'});
          }else{
            res.json([{'weekMetrics': weekMetrics, 'dayMetrics': dayMetrics}]);
          }
        });
      }
    });
  }else if (req.query.managerMetrics) {
    if(req.user.roles.indexOf('manager') !== -1){
      User.find({manager : req.user}).exec(function (err, users) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else{
          var userData = []; 
          async.forEachOf(users, function (user, key, callback) {
            getMetricsForUser(user,function(data){
              if(data === null){callback("err: cannot find metircs");}
              else{
                userData.push({user: user, data: data});
                callback(null, "");
              }
            });
          },function (err, results) {
            if (err){
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            }else{
              res.json(userData);
            }
          });
        }
      });
    }
  }else{
    AnalysisRealign.find({ 'belongs_to': req.user }).sort('-created').populate('account', 'title').exec(function (err, anaRel) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(anaRel);
      }
    });
  }
};

function getMetricsForUser(user, callbackfn){
  var data = {};
  async.parallel([
    function(callback){
      module.exports.getMetricsFor('day', user,function(dayMetrics){
        if (dayMetrics === null){
          callback('cannot find metircs');
        }else{
          data.dayMetrics = dayMetrics;
          callback(null, dayMetrics);
        }
      });
    },
    function(callback){
      module.exports.getMetricsFor('week', user,function(weekMetrics){
        if (weekMetrics === null){
            callback('cannot find metircs');
        }else{
          data.weekMetrics = weekMetrics;
          callback(null, weekMetrics);
        }
      });
    },
    function(callback){
      module.exports.getMetricsFor('quarter', user,function(quarterMetrics){
        if (quarterMetrics === null){
          callback('cannot find metircs');
        }else{
          data.quarterMetrics = quarterMetrics;
          callback(null, quarterMetrics);
        }
      });
    },], 
      function(err, results){
        if(err) {callbackfn(null);}
        else{callbackfn(data);}
        // var resArray = {};
        // results.forEach(function(element){
        //   resArray = _.extend(resArray, element);
        // });
    });
}

/**
 * Article middleware
 */
exports.anaRelByID = function (req, res, next, id) {
  // console.log(id);
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'AnalysisRealign is invalid'
    });
  }
  AnalysisRealign.findById(id).populate('belongsTo', 'displayName').exec(function (err, anaRelC) {
    if (err) {
      return next(err);
    } else if (!anaRelC) {
      return res.status(404).send({
        message: 'No AnalysisRealign with that identifier has been found'
      });
    }
    req.anaRel = anaRelC;
    next();
  });
};