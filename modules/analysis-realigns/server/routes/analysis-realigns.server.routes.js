'use strict';

var analysis = require('../controllers/analysis-realigns.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/analysis')
    .get(analysis.list)
    .post(analysis.create);

  // Single article routes
  app.route('/api/analysis/:analysisId')
    .get(analysis.read)
    .put(analysis.update)
    .delete(analysis.delete);

  app.route('/api/analysisSearch/:analysisId')
    // .get(analysis.read)
    .put(analysis.updateReportIn);
    // .delete(analysis.delete);

  // Finish by binding the article middleware
  app.param('analysisId', analysis.anaRelByID);
};
