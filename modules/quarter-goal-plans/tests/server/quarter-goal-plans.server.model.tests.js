'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  QuarterGoalPlans = mongoose.model('QuarterGoalPlans');

/**
 * Globals
 */
var user, quarterGoalPlans;

/**
 * Unit tests
 */
describe('Quarter goal plans Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() { 
      quarterGoalPlans = new QuarterGoalPlans({
        // Add model fields
        // ...
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      return quarterGoalPlans.save(function(err) {
        should.not.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) { 
    QuarterGoalPlans.remove().exec();
    User.remove().exec();

    done();
  });
});
