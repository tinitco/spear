'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * QuarterGoalPlans Schema
 */
var QuarterGoalPlansSchema = new Schema({
  // QuarterGoalPlans model fields   
  // ...
});

mongoose.model('QuarterGoalPlans', QuarterGoalPlansSchema);
