(function () {
  'use strict';

  angular
    .module('spear.services')
    .factory('SpearService', SpearService)
    .factory('SpearReports', SpearReports)
    .factory('SpearSearch', SpearSearch)
    .service('SpearBroadCastService', function() {
      var spear_type = '';
      var spear_obj = '';
      var user_id = '';

      var setSpear = function(spear) {
        spear_obj = spear;
      };
      var getSpear = function(){
        return spear_obj;
      };
      var setUser = function(user) {
        user_id = user;
      };
      var getUser = function(){
        return user_id;
      };
      return {
        setSpear: setSpear,
        getSpear: getSpear,
        setUser: setUser,
        getUser: getUser
      };
    });

  SpearService.$inject = ['$resource'];

  function SpearService($resource) {
    return $resource('./api/spear/:spearId', {
      spearId: '@_id'
    },{
      update: { method: 'PUT' },
      getMonth: {method: 'GET', isArray : true, url:'api/reports/month/'},
      getAllSpearData: {method: 'GET', isArray : true, url:'api/reports/allData/'},
      emailReport: {method: 'GET', isArray : false, url:'api/emailreports'}
    });
  }

  // SpearService.$inject = ['$resource'];

  function SpearSearch($resource) {
    return $resource('./api/spearsearch/:spearId', {
      spearId: '@_id'
    },{
      update: { method: 'PUT' }
    });
  }

  SpearReports.$inject = ['$resource'];

  function SpearReports($resource) {
    return $resource('./api/reports/:spearId', {
      spearId: '@_id'
    },{
      pdf: {
        method: 'GET',
        headers: {
            accept: 'application/pdf'
        },
        responseType: 'arraybuffer',
        cache: false,
        transformResponse: function (data) {
            var pdf;
            if (data) {
                pdf = new Blob([data], {
                    type: 'application/pdf'
                });
            }
            return {
                response: pdf
            };
        }
    }
    });
  }
})();
