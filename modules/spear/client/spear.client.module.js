(function (app) {
  'use strict';

  app.registerModule('spear');
  app.registerModule('spear.services');
  app.registerModule('spear.routes', ['ui.router', 'spear.services']);
})(ApplicationConfiguration);
