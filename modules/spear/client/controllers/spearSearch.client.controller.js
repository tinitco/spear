(function () {
  'use strict';

  angular
    .module('spear')
    .controller('SpearSearchController', SpearSearchController)
    .config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        // return moment(date).format('ddd, M/D/YYYY');
      };
    });

  SpearSearchController.$inject = ['$scope', '$stateParams', '$rootScope', '$state', 'Authentication',
   'SpearSearch', 'TodoSearchService', 'AnalysisRealignsSearchService', '$mdDialog', '$mdMedia', '$timeout', 'moment','Users', 'Notification'];

  function SpearSearchController($scope, $stateParams, $rootScope, $state, Authentication, SpearSearch, TodoSearchService, AnalysisRealignsSearchService,
    $mdDialog, $mdMedia, $timeout, moment, Users, Notification) {

    var vm = this;
    // vm.readonly = false;
    // vm.userId = null;

    // if($stateParams.reports && $stateParams.reports !== null){
    //   vm.reports = true;
    //   Users.query({list: 'adminUsers'}, function (data) {
    //     vm.users = data;
    //   });
    //   $('#mainDisplayArea').addClass('reportpage');
    // }

    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    // vm.accountSelected = null;
    // vm.authentication = Authentication;
    // vm.error = null;
    // vm.form = {};
    // vm.toggleStatus = toggleStatus;
    // vm.shareReport = shareReport;

    // $scope.progress = 10;
    // vm.progress = 50;
    vm.showAnaRelbuttons = showAnaRelbuttons;
    vm.showMonthbuttons = showMonthbuttons;
    vm.disableButtonType = disableButtonType;
    vm.type = 'quarter';
    $scope.type = 'quarter';
    vm.periods = ['day', 'week','quarter'];
    vm.elemtypes = ['Plan', 'Goal','Success', 'Failure', 'Realign'];
    // vm.type = $stateParams.type;
    // $scope.type = vm.type;
    // console.log(vm.type);
    vm.date = moment().toDate();
    vm.displayElements = [];
    $scope.itemsByPage=10;
    $scope.itemsOptions = [10,25,50,100];


    // vm.day_num = moment().day();
    // vm.week_num = getWeek(moment());
    // vm.quarter_num = moment().quarter();
    vm.dateChanged = dateChanged;
    vm.SpearChanged = SpearChanged;
    // vm.spearUpType = spearUpType;
    // vm.updateReportType = updateReportType;
    // vm.getTypely = getTypely;

    vm.prevSpear = prevSpear;
    vm.nextSpear = nextSpear;

    // Get this spear
    vm.spear = null;// SpearService.query();

    // vm.save = save;
    function SpearChanged(spear_type){
      if (vm.type !== spear_type){
        vm.type = spear_type;
        $scope.type = vm.type;
        dateChanged("");
      }
    }

    // function updateReportType(type){
    //   $scope.type = type;

    // }

    // function shareReport() {
    //   var action = '';
    //   var email = '';
    //   var prevDate = moment(getPrevDate()).format("YYYY-MM-DD");
    //   var data = {date: vm.date1, type: vm.type};
    //   if(vm.type === 'day' || vm.type === 'week'){data.prev = prevDate;}
    //   var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
    //   $mdDialog.show({
    //     controller: ShareReportDialogController,
    //     templateUrl: 'modules/spear/client/views/spear-report-share.client.template.html',
    //     parent: angular.element(document.body),
    //     locals:{
    //       users: vm.users,
    //       action: action,
    //       email: email,
    //       data: data
    //     },
    //     clickOutsideToClose:false,
    //     fullscreen: useFullScreen
    //   })
    //   .then(function(return_data) {
    //     SpearReports.pdf({type:vm.type, email: return_data.email, date:vm.date1},function(data){
    //       // var blob = data.response.blob;
    //       // $window.saveAs(blob, 'report.pdf');
    //     },function(){

    //     });
    //   });
    // }
    $scope.reportIn = function(anaRel, period){
      if(anaRel.reportIn === period){period = 'null';}
      var ar = new AnalysisRealignsSearchService();
      ar._id = anaRel.id;
      ar.reportIn = period;
      ar.$update(function(){
        if(period === 'month'){
          addMonthToTotals(anaRel.spear_month);
          $scope.monthTotals[anaRel.spear_month][anaRel.type]++;
        }else if (period === 'null') {
          $scope.monthTotals[anaRel.spear_month][anaRel.type]--;
        }
        Notification.success('Currently have ' + $scope.monthTotals[anaRel.spear_month][anaRel.type] +' ' + anaRel.type +' in this ' + (anaRel.spear_month + 1) + 'th month');
        anaRel.reportIn = ar.reportIn;
      });
    };

    $scope.addToMonth = function(todo){
      var td = new TodoSearchService();
      td._id = todo.id;
      td.reportInmonth = !todo.month;
      td.$update(function(){
        todo.month = !todo.month;
        if(todo.month){
          addMonthToTotals(todo.spear_month);
          $scope.monthTotals[todo.spear_month].plans++;
        }else{
          $scope.monthTotals[todo.spear_month].plans--;
        }
        Notification.success('Currently have ' + $scope.monthTotals[todo.spear_month].plans +' Plan in this ' + (todo.spear_month + 1) + 'th month');

      });

    };

    $scope.getTypely = function(){
      if (vm.type === 'day') return 'daily';
      if (vm.type === 'week') return 'weekly';
      if (vm.type === 'quarter') return 'quarterly';
      return vm.type;
    };

    $scope.getType = function(){
      // if (vm.type === 'day') return 'daily';
      return vm.type;
    };

    $scope.dateFilterPredicate = function(date) {
      var day = date.getDay();
      if(vm.type === 'week'){return day === 1;}
      return true;
    };

    // update
    // function toggleStatus(ev, account) {
    //   // Appending dialog to document.body to cover sidenav in docs app
    //   var confirm = $mdDialog.confirm()
    //         .title('Change Account Status?')
    //         .textContent('Are you sure you want to change the Account Status?')
    //         .ariaLabel('Change Status')
    //         .targetEvent(ev)
    //         .ok('Yes')
    //         .cancel('Cancel');
    //   $mdDialog.show(confirm).then(function() {
    //     account.active =!account.active;
    //     account.$update();
    //   }, function() {});
    // }

    function getWeek(day){
      var week = parseInt(day.week()%13);
      week = week === 0?13:week;
      return week;
    }

    function updateProgress() {
      var d = moment(vm.date);
      vm.day_num = d.day();
      vm.week_num = getWeek(d);
      vm.quarter_num = d.quarter();

      if (vm.type === 'day'){
        $scope.progress = vm.day_num*100/6;
        $scope.type_num = vm.day_num +1;
        $scope.type_max = 7;
      }
      if (vm.type === 'week'){
        $scope.progress = vm.week_num*100/13;
        $scope.type_num = vm.week_num;
        $scope.type_max = 13;
      }
      if (vm.type === 'quarter'){
        $scope.progress = vm.quarter_num*100/4;
        $scope.type_num = vm.quarter_num;
        $scope.type_max = 4;
      }
    }

    function nextSpear(){
      vm.relative = 'next';
      vm.date = getNextDate();
      dateChanged("");
    }

    function getNextDate(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return moment(vm.date)
          .add(1, 'days').toDate();
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return moment(vm.date)
          .add(7, 'days').toDate();
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return moment(vm.date)
          .add(3, 'months').toDate();
      }
    }

    function prevSpear(){
      vm.relative = 'prev';
      vm.date = getPrevDate();
      dateChanged('');
    }

    function getPrevDate(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return moment(vm.date)
          .subtract(1, 'days').toDate();
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return moment(vm.date)
          .subtract(7, 'days').toDate();
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return moment(vm.date)
          .subtract(3, 'months').toDate();
      }
    }

    function dateChanged(day){
      updateProgress();
      //var d = $.datepicker.parseDate("MM dd, DD - yy",  $scope.date);
      //$scope.date1 = $.datepicker.formatDate( "yy-mm-dd", d);
      updateSpear();
    }

    function spearUpType(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return 'week';
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return 'quarter';
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return 'annual';
      }else if(vm.type === 'annual'|| vm.type === "annaul"){
        return '3 year';
      }
      return null;
    }

    function updateSpear() {
      vm.date1 = moment(vm.date).format("YYYY-MM-DD");
      var ret = SpearSearch.query({date:vm.date1}, function(data){
        processTodos(ret[0].todos);
        processAnaRel(ret[0].anaRels);
        processGoals(ret[0].goals);
        console.log($scope.monthTotals);
      });
    }
    updateSpear();
    updateProgress();

    $scope.monthTotals = {"annual": {"Success": 0, "Failure": 0, "Realign": 0}};
    function addMonthToTotals(month){
      if (!(month in $scope.monthTotals)){$scope.monthTotals[month] = {
        "plans": 0, "Success": 0, "Failure": 0, "Realign": 0};}
    }

    function processTodos(todos){
      var spear_month = 0;
      for (var i = todos.length - 1; i >= 0; i--) {
        spear_month = moment(todos[i].spear.begin).month();
        if(todos[i].reportInmonth){
          addMonthToTotals(spear_month);
          $scope.monthTotals[spear_month].plans++;
        }
        vm.displayElements.push({
          "id": todos[i]._id,
          "description": todos[i].title,
          "period": todos[i].spear.spear_type,
          "month": todos[i].reportInmonth,
          "spear_month": spear_month,
          "type": "Plan",
          "active": todos[i].active,
          "date": moment(todos[i].spear.begin).format("MM-DD-YYYY")
        });
      }
    }

    var anaRelTypes = {
      "analysisSuccess": "Success",
      "analysisFailure": "Failure",
      "realign": "Realign"
    };

    function processAnaRel(anaRels){
      var spear_month = 0;
      for (var i = anaRels.length - 1; i >= 0; i--) {
        spear_month = moment(anaRels[i].spear.begin).month();
        if(anaRels[i].reportIn === 'month'){
          addMonthToTotals(spear_month);
          $scope.monthTotals[spear_month][anaRelTypes[anaRels[i].type]]++;
        }else if (anaRels[i].reportIn === 'annual') {
          $scope.monthTotals["annual"][anaRelTypes[anaRels[i].type]]++;
        }

        vm.displayElements.push({
          "id": anaRels[i]._id,
          "description": anaRels[i].text,
          "period": anaRels[i].spear.spear_type,
          "reportIn": anaRels[i].reportIn,
          "spear_month": spear_month,
          "type": anaRelTypes[anaRels[i].type],
          "active": true,
          "date": moment(anaRels[i].spear.begin).format("MM-DD-YYYY")
        });
      }
    }

    function processGoals(goals){
      for (var i = goals.length - 1; i >= 0; i--) {
        vm.displayElements.push({
          "id": goals[i]._id,
          "description": goals[i].title,
          "period": goals[i].spear.spear_type,
          "type": "Goal",
          "active": goals[i].active,
          "date": moment(goals[i].spear.begin).format("MM-DD-YYYY")
        });
      }
    }

    var limits = {
      "monthPlans": 6,
      "monthSuccesses":3,
      "monthFailures":3,
      "monthRealigns":3,

      "annualSuccesses":3,
      "annualFailures":3,
      "annualRealigns":3,
    };

    function Checklimits(){

    }

    function showAnaRelbuttons(todo){
      if(['Plan', 'Goal'].indexOf(todo.type) === -1){return true;}
      return false;
    }

    function showMonthbuttons(todo){
      if(todo.type === 'Plan' && (['day', 'week'].indexOf(todo.period) != -1)){return true;}
      return false;
    }

    function disableButtonType(todo, type){
      if (type === 'week' && ['week', 'quarter'].indexOf(todo.period) != -1){return true;}
      if (todo.period === 'quarter' && ['month','quarter'].indexOf(type) != -1){return true;}
      return false;
    }

    function broadcast(){

    }

    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
      // if(vm.readonly){
      //   $timeout(function(){
      //     $('table  button').prop("disabled", true);
      //     $('li  button').prop("disabled", true);
      //   },100);
      //   // document.getElementById('msg').innerHTML = 'Hello';
      // }
    });
  }
})();
