(function () {
  'use strict';

  angular
    .module('spear')
    .controller('SpearMonthReportController', SpearMonthReportController)
    .config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        // return moment(date).format('ddd, M/D/YYYY');
      };
    });

  SpearMonthReportController.$inject = ['$scope', '$stateParams', '$rootScope', '$state', 'Authentication',
   'SpearService', 'SpearReports', '$mdDialog', '$mdMedia', '$timeout', 'moment','Users'];

  function SpearMonthReportController($scope, $stateParams, $rootScope, $state, Authentication, SpearService, SpearReports,
    $mdDialog, $mdMedia, $timeout, moment, Users) {

    var vm = this;
    vm.readonly = false;
    vm.userId = null;

    if($stateParams.userId && $stateParams.userId !== null){
      vm.readonly = true;
      vm.userId = $stateParams.userId;
    }

    if($stateParams.reports && $stateParams.reports !== null){
      vm.reports = true;
      Users.query({list: 'adminUsers'}, function (data) {
        vm.users = data;
      });
      $('#mainDisplayArea').addClass('reportpage');
    }

    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.range4 = [0,1,2,3];

    vm.accountSelected = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    // vm.toggleStatus = toggleStatus;
    vm.shareReport = shareReport;

    $scope.progress = 10;
    // vm.progress = 50;

    vm.type = 'month';
    vm.type = $stateParams.type;
    $scope.type = vm.type;
    // console.log(vm.type);
    vm.date = moment().toDate();

    vm.day_num = moment().day();
    vm.week_num = getWeek(moment());
    vm.quarter_num = moment().quarter();
    vm.dateChanged = dateChanged;
    vm.SpearChanged = SpearChanged;
    vm.spearUpType = spearUpType;
    // vm.updateReportType = updateReportType;
    // vm.getTypely = getTypely;

    vm.prevSpear = prevSpear;
    vm.nextSpear = nextSpear;

    // Get this spear
    vm.spear = null;// SpearService.query();

    // vm.save = save;
    function SpearChanged(spear_type){
      if (vm.type !== spear_type){
        vm.type = spear_type;
        $scope.type = vm.type;
        dateChanged("");
      }
    }

    // function updateReportType(type){
    //   $scope.type = type;

    // }

    function shareReport() {
      var action = '';
      var email = '';
      var prevDate = moment(getPrevDate()).format("YYYY-MM-DD");
      var data = {date: vm.date1, type: vm.type};
      if(vm.type === 'day' || vm.type === 'week'){data.prev = prevDate;}
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: ShareReportDialogController,
        templateUrl: 'modules/spear/client/views/spear-report-share.client.template.html',
        parent: angular.element(document.body),
        locals:{
          users: vm.users,
          action: action,
          email: email,
          data: data
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(return_data) {
        SpearReports.pdf({type:vm.type, email: return_data.email, date:vm.date1},function(data){
          // var blob = data.response.blob;
          // $window.saveAs(blob, 'report.pdf');
        },function(){

        });
      });
    }

    $scope.getTypely = function(){
      if (vm.type === 'day') return 'daily';
      if (vm.type === 'week') return 'weekly';
      if (vm.type === 'month') return 'monthly';
      if (vm.type === 'quarter') return 'quarterly';
      return vm.type;
    };

    $scope.getType = function(){
      // if (vm.type === 'day') return 'daily';
      return vm.type;
    };

    $scope.dateFilterPredicate = function(date) {
      var day = date.getDay();
      if(vm.type === 'week'){return day === 1;}
      return true;
    };

    function getWeek(day){
      var week = parseInt(day.week()%13);
      week = week === 0?13:week;
      return week;
    }

    function updateProgress() {
      var d = moment(vm.date);
      vm.day_num = d.day();
      vm.week_num = getWeek(d);
      vm.quarter_num = d.quarter();

      if (vm.type === 'day'){
        $scope.progress = vm.day_num*100/6;
        $scope.type_num = vm.day_num +1;
        $scope.type_max = 7;
      }
      if (vm.type === 'week'){
        $scope.progress = vm.week_num*100/13;
        $scope.type_num = vm.week_num;
        $scope.type_max = 13;
      }
      if (vm.type === 'quarter'){
        $scope.progress = vm.quarter_num*100/4;
        $scope.type_num = vm.quarter_num;
        $scope.type_max = 4;
      }
    }

    function nextSpear(){
      vm.relative = 'next';
      vm.date = getNextDate();
      dateChanged("");
    }

    function getNextDate(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return moment(vm.date)
          .add(1, 'days').toDate();
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return moment(vm.date)
          .add(7, 'days').toDate();
      }else if(vm.type === 'month'|| vm.type === "monthly"){
        return moment(vm.date)
          .add(1, 'months').toDate();
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return moment(vm.date)
          .add(3, 'months').toDate();
      }
    }

    function prevSpear(){
      vm.relative = 'prev';
      vm.date = getPrevDate();
      dateChanged('');
    }

    function getPrevDate(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return moment(vm.date)
          .subtract(1, 'days').toDate();
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return moment(vm.date)
          .subtract(7, 'days').toDate();
      }else if(vm.type === 'month'|| vm.type === "monthly"){
        return moment(vm.date)
          .subtract(1, 'months').toDate();
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return moment(vm.date)
          .subtract(3, 'months').toDate();
      }
    }

    function dateChanged(day){
      updateProgress();
      //var d = $.datepicker.parseDate("MM dd, DD - yy",  $scope.date);
      //$scope.date1 = $.datepicker.formatDate( "yy-mm-dd", d);
      updateSpear();
    }

    function spearUpType(){
      if(vm.type === 'day'|| vm.type === "daily"){
        return 'week';
      }else if(vm.type === 'week'|| vm.type === "weekly"){
        return 'quarter';
      }else if(vm.type === 'month'|| vm.type === "monthly"){
        return 'quarter';
      }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
        return 'annual';
      }else if(vm.type === 'annual'|| vm.type === "annaul"){
        return '3 year';
      }
      return null;
    }

    function updateSpear() {
      vm.date1 = moment(vm.date).format("YYYY-MM-DD");
      var prevDate = moment(getPrevDate()).format("YYYY-MM-DD");
      var nextDate = moment(getNextDate()).format("YYYY-MM-DD");
      if(!moment(vm.date1).isValid() || !moment(prevDate).isValid() || !moment(nextDate).isValid()){
        console.log(vm.date1);
        return;
      }
      vm.todos = SpearService.getMonth({userId: vm.userId, type:vm.type, date:vm.date1}, function(){
        // if(!vm.spear ||(ret[0]._id !== vm.spear._id)){
          // vm.spear = ret[0];
          var upType = spearUpType();
          if (upType){
          var spearUp = SpearService.query({userId: vm.userId, type:upType, date:vm.date1}, function(){
            $rootScope.$broadcast('spearChanged',{
              date:vm.date1, type:vm.type,
              day:vm.day_num, week: vm.week_num, quarter_num: vm.quarter_num, spear_up: spearUp[0]._id});
          });
          }else{
            $rootScope.$broadcast('spearChanged',{
              date:vm.date1, type:vm.type, 
              day:vm.day_num, week: vm.week_num, quarter_num: vm.quarter_num, spear_up: null
            });
          }
        // }
      });
      if (vm.type === 'month'){
        var start = moment(vm.date).startOf('month').format("YYYY-MM-DD");
        var end = moment(vm.date).endOf('month').format("YYYY-MM-DD");
      vm.downData = SpearService.getAllSpearData({userId: vm.userId, type: "week", startDate: start, endDate: end}, function(){
        vm.downData = vm.downData[0];
        // console.log(vm.downData[0]);
      });
      }
    }
    updateSpear();
    updateProgress();

    function broadcast(){

    }

    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
      if(vm.readonly){
        $timeout(function(){
          $('table  button').prop("disabled", true);
          $('li  button').prop("disabled", true);
        },100);
        // document.getElementById('msg').innerHTML = 'Hello';
      }
    });
  }
})();

function NewAccountDialogController($scope, $mdDialog, AccountTypesService) {
  'use strict';

  $scope.accountTypes = AccountTypesService.query();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}

function ShareReportDialogController($scope, $mdDialog, users, action, email, data) {
  'use strict';

  $scope.users = users;
  $scope.data = data;
  $scope.email = '';


  $scope.selectAction = function(act){
    action = act;
    if (act === 'email'){
      $mdDialog.hide({action: act, email: $scope.email});
    }else {
      $mdDialog.hide({action: act});
    }
  };

  $scope.minDate = new Date();
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
