(function () {
  'use strict';

  angular
    .module('spear.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('dayspear', {
        url: '/spear/day',
        params:{type:{value: 'day'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('weekspear', {
        url: '/spear/week',
        params:{type:{value: 'week'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('quarterspear', {
        url: '/spear/quarter',
        params:{type:{value: 'quarter'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('annualspear', {
        url: '/spear/annual',
        params:{type:{value: 'annual'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('quarterPlanner', {
        url: '/spear/quarterPlanner',
        params:{type:{value: 'quarter'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter-planner.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('annualPlanner', {
        url: '/spear/annualPlanner',
        params:{type:{value: 'annual'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter-planner.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('dailyReports', {
        url: '/reports/day',
        params:{type:{value: 'day'}, reports:{value:true}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-reports-day.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Reports'
        }
      })
      .state('weeklyReports', {
        url: '/reports/week',
        params:{type:{value: 'week'}, reports:{value:true}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-reports-day.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Reports'
        }
      })
      .state('monthlyReports', {
        url: '/reports/month',
        params:{type:{value: 'month'}, reports:{value:true}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-reports-month.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Reports'
        }
      })
      .state('quarterReports', {
        url: '/reports/quarter',
        params:{type:{value: 'quarter'}, reports:{value:true}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-reports-quarter.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Reports'
        }
      })
      .state('annualReports', {
        url: '/reports/annual',
        params:{type:{value: 'annual'}, reports:{value:true}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-reports-annual.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Reports'
        }
      })
      .state('Userdayspear', {
        url: '/spear/user/day/:userId',
        params:{type:{value: 'day'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('Userweekspear', {
        url: '/spear/user/week/:userId',
        params:{type:{value: 'week'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('Userquarterspear', {
        url: '/spear/user/quarter/:userId',
        params:{type:{value: 'quarter'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('Userannualspear', {
        url: '/spear/user/annual/:userId',
        params:{type:{value: 'annual'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('UserquarterPlanner', {
        url: '/spear/user/quarterPlanner/:userId',
        params:{type:{value: 'quarter'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter-planner.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('UserannualPlanner', {
        url: '/spear/user/annualPlanner/:userId',
        params:{type:{value: 'annual'}},
        hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-quarter-planner.client.view.html',
        data: {
          roles: ['superAdmin', 'manager'],
          pageTitle: 'Spearity :'
        }
      })
      .state('SpearSearch', {
        url: '/spear/search',
        // params:{type:{value: 'annual'}},
        controller: 'SpearSearchController',
        controllerAs: 'vm',
        // hiddenParam: 'YES',
        templateUrl: 'modules/spear/client/views/spear-search.client.view.html',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Spearity : Search :'
        }
      })
      ;
  }

  getUser.$inject = ['$stateParams', 'SpearService'];

  function getUser($stateParams, SpearService) {
    return SpearService.get({
      userId: $stateParams.userId
    }).$promise;
  }

  newSpear.$inject = ['SpearService'];

  function newSpear(SpearService) {
    return new SpearService();
  }
})();
