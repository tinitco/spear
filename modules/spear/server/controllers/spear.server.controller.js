'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var fs = require('fs');
var pdf = require('html-pdf');
var handlebars = require('handlebars');
var path = require('path'),
  mongoose = require('mongoose'),
  moment = require('moment'),
  Spear = mongoose.model('Spear'),
  Todo = mongoose.model('Todo'),
  QGoals = mongoose.model('QuarterGoals'),
  User = mongoose.model('User'),
  async = require('async'),
  AnalysisRealign = mongoose.model('AnalysisRealign'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  config = require(path.resolve('./config/config')),
  nodemailer = require('nodemailer'),
  smtpTransport = nodemailer.createTransport(config.mailer.options);
var schedule = require('node-schedule');

// Spear Helper functions

var getBeginAndEndDates = function(date, type){
  // console.log(date);
  var md = moment(date, 'YYYY-MM-DD');
  // console.log(type);

  if (!type){ type = 'day'; }
  if (type === 'day'){
    return [md.startOf('day').toISOString(), md.endOf('day').toISOString()];
  }if (type === 'week'){
    return [md.startOf('week').toISOString(), md.endOf('week').toISOString()];
  }if (type === 'month'){
    return [md.startOf('month').toISOString(), md.endOf('month').toISOString()];
  }if (type === 'quarter'){
    return [md.startOf('quarter').toISOString(), md.endOf('quarter').toISOString()];
  }
  return [md.startOf('year').toISOString(), md.endOf('year').toISOString()];
};

/**
* Async function - please use callbacks
*
*/
var findSpearByDate = function(date, type, user, callback){
  var queryDate = moment(date).toISOString();
  Spear.findOne({
    'spear_type':type,
    'begin': { '$lte': queryDate },
    'end': { '$gte': queryDate },
    'belongs_to': user
  }).populate('type', 'title').exec(function (err, spear) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      callback(null);
    } else {
      callback(spear);
    }
  });
};

exports.findOrCreateSpearByDate = function(type, date, user, callback){
  findSpearByDate(date, type, user, function(spear){
    if (!spear){
        console.log('spear created');
        var dates = getBeginAndEndDates(date, type);
        var newspear = new Spear();
        newspear.belongs_to = user;
        newspear.begin = dates[0];
        newspear.end = dates[1];
        newspear.spear_type = type;
        newspear.save(function (err) {
          if (err) {
            console.log(errorHandler.getErrorMessage(err));
            return null;
          } else {
            callback(newspear);
          }
      });
    }else{
      callback(spear);
    }
  });
};

/**
 * Create a
 */
exports.create = function (req, res) {
  var spear = new Spear(req.body);
  // console.log(spear);
  // spear.user = req.user;
  spear.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(spear);
    }
  });
};

/**
 * Show the current
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var spear = req.spear ? req.spear.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // spear.isCurrentUserOwner = req.user && spear.user && spear.user._id.toString() === req.user._id.toString() ? true : false;
  res.json(spear);
};

/**
 * Update a
 */
exports.update = function (req, res) {
  var spear = req.spear;

  spear.title = req.body.title;
  spear.active = req.body.active;
  spear.type = req.body.type;

  // spear.max_users = req.body.max_users;

  spear.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(spear);
    }
  });

};

/**
 * Delete an
 */
exports.delete = function (req, res) {
  var spear = req.spear;

  spear.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(spear);
    }
  });
};



/**
 * List of
 */
exports.list = function (req, res) {
  if (!req.user){
    return res.status(400).send({
          message: errorHandler.getErrorMessage('need to be logged in')
        });
  }
  var user = req.user;
  if (req.query.userId && req.query.userId !== null){
    user = req.query.userId;
  }
  if (req.query.date && req.query.type){
    var retSpears = [];
    module.exports.findOrCreateSpearByDate(req.query.type, req.query.date, user, function(curSpear){
      if (!curSpear){
        return res.status(400).send({
          message: 'cannot find spear'
        });
      }else if(req.query.next){
        module.exports.findOrCreateSpearByDate(req.query.type, req.query.next,  user, function(nxtSpear){
          if (!nxtSpear){
            return res.status(400).send({
                message: 'cannot find spear'
              });
          }else if(req.query.prev){
            findSpearByDate(req.query.prev, req.query.type, user,
              function(prevSpear){
                res.json([curSpear, nxtSpear, prevSpear]);
            });
          }else{
            res.json([curSpear, nxtSpear]);
          }
        });
      }else{
        res.json([curSpear]);
      }
    });
  }else{
    // console.log('from list');
    Spear.find().sort('-created').populate('spear_type', 'title').exec(function (err, spear) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(spear);
      }
    });
  }
};

exports.spearByDateAndType = function(req, res, next){
  console.log('from date and time');
  Spear.findOne({
    'spear_type':req.params.type,
    'begin': { '$lt': req.params.date },
    'end': { '$gte': req.params.date }
  }).populate('type', 'title').exec(function (err, spear) {
    if (err) {
      return next(err);
    } else if (!spear) {
      return res.status(404).send({
        message: 'No spear with that identifier has been found'
      });
    }
    req.spear = spear;
    next();
  });
};

/**
 * Spear middleware
 */
exports.spearByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Spear is invalid'
    });
  }

  Spear.findById(id).populate('type', 'title').exec(function (err, spear) {
    if (err) {
      return next(err);
    } else if (!spear) {
      return res.status(404).send({
        message: 'No spear with that identifier has been found'
      });
    }
    req.spear = spear;
    next();
  });
};

/**
 * Spear reports
 */

function generatePdf(date,user, type, callbackfn){
  var file = getHtmlFilefromType(type);
  var html = fs.readFileSync(file, 'utf8');
  var css = fs.readFileSync('modules/spear/server/views/reportsrender.css', 'utf8');
  var img = fs.readFileSync('modules/spear/server/img/FSDg2p.png', 'base64');
  var template = handlebars.compile(html);
  // var user = req.user;
  var uptype = getSpearUptype(type);
  var context = {
    "name": user.firstName,
    "css": css,
    "date":moment(date, 'YYYY-MM-DD').format('MM-DD-YYYY'),
    "img": img,
    "upType": uptype,
    "upTypeTitle": getSpearTypeTitle(uptype),
    "currentType": type,
    "currentTypeTitle": getSpearTypeTitle(type),
    "typeTitle": uptype,
    "previousPlans":[
      {"title": ""}
    ],
    "previousSuccesses":[],
    "previousFailures":[],
    "previousRealigns":[],
  };
  handlebars.registerHelper("inc", function(value, options)
  {
      return parseInt(value) + 1;
  });

  if (date && type){
    reportsDispatch(date, type, uptype, user, function(data){
        context = _.extend(context, data);
        var result = template(context);
        // fs.writeFile("test_res.html", result, function(err){ });
        var options = { format: 'A4', "border": "1cm",
      "base": "/home/satwik/projects/livep/spear2/modules/spear/server/views/",};
        pdf.create(result, options).toStream(function(err, stream){
          // res.setHeader('Content-Length', );
          if(err){callbackfn(null);}
          callbackfn(stream);
        });
    });
  }
}

function emailReportFn(date, user, type, toemail, callback){
  generatePdf(date, user, type, function(stream){
    if(!stream){
      return callback(null);
    }
    // console.log('till here');
    // var user = req.user;
    var filename = "Realignment Report "+ type+ "-" + user.firstName + "-" + date + ".pdf";

    var html = fs.readFileSync('modules/spear/server/templates/send-report-email.server.view.html', 'utf8');
    var template = handlebars.compile(html);
    var context = {
      name: user.firstName,
      appName: config.app.title,
    };
    var result = template(context);


    // res.render(path.resolve("modules/spear/server/templates/send-report-email"), {
    //     name: user.firstName,
    //     appName: config.app.title,
    //   }, function (err, emailHTML) {
    //     // done(err, emailHTML, user);
    //     if (err){
    //       // console.log(err);
    //       return callback(null);
    //     }
        var mailOptions = {
            to: toemail,
            from: config.mailer.from,
            replyTo: user.email,
            subject: 'Sharing My Report',
            html: result,
            attachments:[{
              filename: filename,
              content: stream,
              contentType: 'application/pdf'
            }]
          };
          smtpTransport.sendMail(mailOptions, function (err) {
            if (!err) {
              return callback("success");
            } else {
            console.log(err);
              return callback(null);
            }
            // done(err);
          });
      // });

  });
}

exports.emailReport = function(req, res){
  emailReportFn(req.query.date, req.user, req.query.type,
    req.query.email, function(response){
      if(!response){
        return res.status(404).send({
          message: 'Error Generating Report'
        });
      }
      res.send({
        message: 'An email has been sent to the provided email.'
      });
  });
};

exports.getreport = function(req, res){
  generatePdf(req.query.date, req.user, req.query.type, function(stream){
    if(!stream){
    }else{
      var filename = "Realignment Report "+ req.query.type+ "-" + req.user.firstName + "-" + req.query.date + ".pdf";
      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader("Content-Disposition", "attachment; filename="+ filename);
      stream.pipe(res);
    }
  });
};

function getPrevDate(date, type){
  if(type === 'day'|| type === "daily"){
    return moment(date)
      .subtract(1, 'days').toDate();
  }else if(type === 'week'|| type === "weekly"){
    return moment(date)
      .subtract(7, 'days').toDate();
  }else if(type === 'month'|| type === "monthly"){
    return moment(date)
      .subtract(1, 'months').toDate();
  }else if(type === 'quarter'|| type === "quarterly"){
    return moment(date)
      .subtract(3, 'months').toDate();
  }
}

function reportsDispatch(date, type, uptype, user,callbackfn){
  if(['day', 'week','quarter'].indexOf(type) !== -1){
    var prev = getPrevDate(date, type);
      getDataforDayWeekQuarter(date, type, prev, uptype, user, function(data){
        callbackfn(data);
        // context = _.extend(context, data);
    });
    }else if(type === 'month'){
      getDataforMonth(date, type, uptype, user, function(data){
        callbackfn(data);
        // context = _.extend(context, data);
      });
    }else if(type === 'annual'){
      getDataforAnnual(date, type, user, function(data){
        callbackfn(data);
      });
    }
}

function getDataforAnnual(date, type, user, callbackfn){
  var context = {};
  var dates = getBeginAndEndDates(date, 'annual');
  findSpearByDate(date, 'annual', user,
      function(spear){
        if(spear){
          populateSpearData(spear,false, true, 'current', function(data){
            if(!data)callbackfn(null);
            else{
              // context.upPlans = data.todos;
              context.currentPlans = data.todos;
              // context.currentsucces = data.anaRels.
              // console.log(data.anaRels);
              context.currentsucces = data.anaRels.analysisSuccess;
              context.currentfail = data.anaRels.analysisFailure;
              context.currentrealign = data.anaRels.realign;

              getAllSpearDatafn(
                'quarter', dates[0], dates[1], user, function(err, results){
                  if (err) {callbackfn(null);}
                  else{
                    if('0' in results){
                      context.week0plans = results['0'].todos;
                      context.week0succes = results['0'].anaRels.analysisSuccess;
                      context.week0fail = results['0'].anaRels.analysisFailure;
                      context.week0realign = results['0'].anaRels.realign;
                    }
                    if('1' in results){
                      context.week1plans = results['1'].todos;
                      context.week1succes = results['1'].anaRels.analysisSuccess;
                      context.week1fail = results['1'].anaRels.analysisFailure;
                      context.week1realign = results['1'].anaRels.realign;
                    }
                    if('2' in results){
                      context.week2plans = results['2'].todos;
                      context.week2succes = results['2'].anaRels.analysisSuccess;
                      context.week2fail = results['2'].anaRels.analysisFailure;
                      context.week2realign = results['2'].anaRels.realign;
                    }
                    if('3' in results){
                      context.week3plans = results['3'].todos;
                      context.week3succes = results['3'].anaRels.analysisSuccess;
                      context.week3fail = results['3'].anaRels.analysisFailure;
                      context.week3realign = results['3'].anaRels.realign;
                    }

                        callbackfn(context);
                    // getAllInMonthFn(dates, req.user, function(todos){
                    //   if (!todos) {
                    //     callbackfn(null);
                    //   }else{
                    //     // res.json(todos);
                    //     context.currentPlans = todos;
                    //   }
                    // });
                  }
                });
            }
          });
        }else{
          callbackfn(null);
        }
   });
}

function getDataforMonth(date, type, uptype, user, callbackfn){
  var context = {};
  var dates = getBeginAndEndDates(date, 'month');
  findSpearByDate(date, uptype, user,
      function(upSpear){
        if(upSpear){
          populateSpearData(upSpear,false, true, 'upspear', function(data){
            if(!data)callbackfn(null);
            else{
              context.upPlans = data.todos;
              getAllSpearDatafn(
                'week', dates[0], dates[1], user, function(err, results){
                  if (err) {callbackfn(null);}
                  else{
                    // for (var i=0;i<4;i++){
                    //   if
                    //   context['week'+i] =
                    // }
                    context.week0plans = results['0'].todos;
                    context.week0succes = results['0'].anaRels.analysisSuccess;
                    context.week0fail = results['0'].anaRels.analysisFailure;
                    context.week0realign = results['0'].anaRels.realign;

                    context.week1plans = results['1'].todos;
                    context.week1succes = results['1'].anaRels.analysisSuccess;
                    context.week1fail = results['1'].anaRels.analysisFailure;
                    context.week1realign = results['1'].anaRels.realign;

                    context.week2plans = results['2'].todos;
                    context.week2succes = results['2'].anaRels.analysisSuccess;
                    context.week2fail = results['2'].anaRels.analysisFailure;
                    context.week2realign = results['2'].anaRels.realign;

                    context.week3plans = results['3'].todos;
                    context.week3succes = results['3'].anaRels.analysisSuccess;
                    context.week3fail = results['3'].anaRels.analysisFailure;
                    context.week3realign = results['3'].anaRels.realign;

                    // context.week1 = results['1'];
                    // context.week2 = results['2'];
                    // context.week3 = results['3'];
                    // console.log(context.week2);
                    getAllInMonthFn(dates, user, function(todos){
                      if (!todos) {
                        callbackfn(null);
                      }else{
                        // res.json(todos);
                        context.currentPlans = todos;
                        callbackfn(context);
                      }
                    });
                  }
                });
            }
          });
        }else{
          callbackfn(null);
        }
   });
}

function getDataforDayWeekQuarter(date, type, prev, uptype, user, callbackfn){
  var context = {};
  async.parallel([
      function(callback){
        findSpearByDate(date, type, user,
          function(currSpear){
            if(currSpear){
              populateSpearData(currSpear, 'current', false, true, function(data){
                if (!data) {callback('error occured');}
                else{
                  context.currentPlans = data.todos;
                  if(['quarter', 'annual'].indexOf(currSpear.spear_type) !== -1){
                    context.previousSuccessesmo1 = data.anaRels.quarterMo1.analysisSuccess;
                    context.previousFailuresmo1 = data.anaRels.quarterMo1.analysisFailure;
                    context.previousRealignsmo1 = data.anaRels.quarterMo1.realign;

                    context.previousSuccessesmo2 = data.anaRels.quarterMo2.analysisSuccess;
                    context.previousFailuresmo2 = data.anaRels.quarterMo2.analysisFailure;
                    context.previousRealignsmo2 = data.anaRels.quarterMo2.realign;

                    context.previousSuccessesmo3 = data.anaRels.quarterMo3.analysisSuccess;
                    context.previousFailuresmo3 = data.anaRels.quarterMo3.analysisFailure;
                    context.previousRealignsmo3 = data.anaRels.quarterMo3.realign;
                  }
                }
                callback(null,"");
              });
            }else{
              callback('Cannot Find Spear',{});
            }
        });},
      function(callback){
        if(!prev){
          callback(null, {'previousPlans': ""});
        }else{
          findSpearByDate(prev, type, user,
            function(prevSpear){
              if(prevSpear){
                populateSpearData(prevSpear, false, true, 'previous', function(data){
                  if (!data) {callback('error occured');}
                  else{
                    context.previousPlans = data.todos;
                    if('anaRels' in data){
                      context.previousSuccesses = data.anaRels.analysisSuccess;
                      context.previousFailures = data.anaRels.analysisFailure;
                      context.previousRealigns = data.anaRels.realign;
                    }
                    callback(null, {'previousPlans': data.todos});
                  }
                });
              }else{
                callback('Cannot Find Spear',{});
              }
         });
      }},
      function(callback){
        if(!uptype){
          callback(null, {'upPlans': ""});
        }else{
        findSpearByDate(date, uptype, user,
          function(upSpear){
            if(upSpear){
              populateSpearData(upSpear,false, true, 'upspear', function(data){
                if(!data)callback('error');
                else{
                  context.upPlans = data.todos;
                  callback(null,"");
                }
              });
            }else{
              callback('Cannot Find Spear',{});
            }
       });
      }},
      ],
      function(err, results){
        var resArray = {};
        results.forEach(function(element){
          resArray = _.extend(resArray, element);
        });
        callbackfn(context);
    });
}

function populateSpearData(spear, type, full, split ,callback){
  if(['quarter', 'annual'].indexOf(spear.spear_type) !== -1){
    findGoalsForSpear(spear._id, function(goals){
      if (!goals) callback(null);
      else{
        var pGoals = processGoals(goals);
        if(type === 'current' || full){
          findAnaRelForSpear(spear._id, function(anaRels){
            if (!anaRels) callback(null);
            var pAnaRels = processAnnaRel(anaRels, split?spear.spear_type: null);
            callback({todos: pGoals, anaRels: pAnaRels});
          });
        }else{callback({todos: pGoals});}
      }
    });
  }else{
    findTodoForSpear(spear._id, function(todos){
      if (!todos) callback(null);
      else{
        var ptodos = processTodos(todos);
        if(type === 'previous' || full){
          findAnaRelForSpear(spear._id, function(anaRels){
            if (!anaRels) callback(null);
            var pAnaRels = processAnnaRel(anaRels);
            callback({todos: ptodos, anaRels: pAnaRels});
          });
        }else{callback({todos: ptodos});}
      }
    });
  }
}

exports.sendreport = function(req, res){

};

function findTodoForSpear(spear_id, callback){
  // console.log(spear_id);
  if( Object.prototype.toString.call( spear_id ) !== '[object Array]' ) {
    spear_id = [spear_id];
  }
  Todo.find({
      'spear':{$in: spear_id},
      // 'belongsTo': req.user
    }).sort('precedence').populate('type', 'title').exec(function (err, todos) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      callback(null);
    } else {
      callback(todos);
    }
  });
}

function findGoalsForSpear(spear_id, callback){
  // console.log(spear_id);
  if( Object.prototype.toString.call( spear_id ) !== '[object Array]' ) {
    spear_id = [spear_id];
  }
  QGoals.find({
      'spear':{$in: spear_id},
      // 'belongsTo': req.user
    }).populate('type', 'title').exec(function (err, goal) {
      if (err) {
        console.log(errorHandler.getErrorMessage(err));
        callback(null);
      }else {
        callback(goal);
      }
    });
}

function findAnaRelForSpear(spear_id, callback){
  if( Object.prototype.toString.call( spear_id ) !== '[object Array]' ) {
    spear_id = [spear_id];
  }
  AnalysisRealign.find({
    'spear':{$in: spear_id}
  }).populate('title').exec(function (err, anaRel) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      callback(null);
    } else {
      callback(anaRel);
    }
  });
}

function processTodos(todos){
  var resArray = [];
  todos.forEach(function(todo){
    resArray.push({"title": todo.title});
  });
  return resArray;
}

function processGoals(goals){
  var resArray = [];
  goals.forEach(function(goal){
    resArray.push({"title": goal.title, "progress": goal.progress});
  });
  return resArray;
}

function processAnnaRel(anaRels, spear_type){
  if (typeof spear_type === 'undefined') {
    spear_type = "day";
  }
  var resArray = {};
  // var success = [];
  // var fail = [];
  var type = '';
  if(spear_type === 'quarter') { //|| spear_type === 'annual'
    resArray['quarterMo1'] = [];
    resArray['quarterMo2'] = [];
    resArray['quarterMo3'] = [];
  }
  var container = resArray;
  anaRels.forEach(function(anaRel){
    if(spear_type === 'quarter') { // || spear_type === 'annual'
      if (!(anaRel.quarterType in resArray)){
        resArray[anaRel.quarterType] = [];
      }
      container = resArray[anaRel.quarterType];
    }
    type = anaRel.type;
    if (!(type in container)){container[type] = [];}
    container[type].push({"title": anaRel.text});
  });
  // console.log(resArray);
  return resArray;
}

function getSpearUptype(type){
  var ret = {'day': 'week', 'week': 'quarter', 'month': 'quarter', 'quarter': 'annual'};
  if (type in ret )return ret[type];
  return null;
}

function getSpearTypeTitle(type){
  var ret = {'day': 'daily', 'week': 'weekly','month': 'monthly', 'quarter': 'quarterly', 'annual':'annual'};
  if (type in ret )return ret[type];
  return type;
}

function getPlanorGoal(type){
  var ret = {'day': 'Plan', 'week': 'Plan', 'month':'Plan', 'quarter': 'Goal', 'annual':'Goal'};
  if (type in ret )return ret[type];
  return 'Plan';
}

function getHtmlFilefromType(type){
  if (type === 'day' || type === 'week'){
    return 'modules/spear/server/views/report-day.html';
  }
  if (type === 'month'){
    return 'modules/spear/server/views/report-month.html';
  }
  if (type === 'quarter'){
    return 'modules/spear/server/views/report-quarter.html';
  }
  if (type === 'annual'){
    return 'modules/spear/server/views/report-annual.html';
  }
  return 'modules/spear/server/views/report-day.html';
  // return
}

exports.getAllInQuarter = function(req, res){
  var date = moment().format('YYYY-MM-DD');
  if(req.query.date){
    date = req.query.date;
  }
  var dates = getBeginAndEndDates(req.query.date, 'quarter');
  Spear.find({
    'begin': { '$gte': dates[0] },
    'end': { '$lte': dates[1] },
    'belongs_to': req.user
  }).populate('type', 'title').exec(function (err, spears) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      var spearIds = spears.map(function(sp){
        return sp._id;
      });
      async.parallel([
        function(callback){
          Todo.find({
            'spear':{$in: spearIds},
            'belongsTo': req.user
          }).populate('spear', 'spear_type begin').exec(function (err, todos) {
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callback(err);
            }else{
              callback(null, {"todos": todos});
            }
          });
        },
        function(callback){
          AnalysisRealign.find({
            'spear':{$in: spearIds}
          }).populate('spear', 'spear_type begin').exec(function (err, anaRels) {
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callback(err);
            }else{
              callback(null, {"anaRels":anaRels});
            }
          });
        },
        function(callback){
          QGoals.find({
            'spear':{$in: spearIds}
          }).populate('spear', 'spear_type begin').exec(function (err, goals) {
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callback(err);
            }else{
              callback(null, {"goals": goals});
            }
          });
        },
      ], function(err, results){
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }
        var resArray = {};
        results.forEach(function(element){
          resArray = _.extend(resArray, element);
        });
        res.json([resArray]);
      });
    }
  });
};

exports.getAllInMonth = function(req,res){
  var date = moment().format('YYYY-MM-DD');
  if(req.query.date){
    date = req.query.date;
  }
  var dates = getBeginAndEndDates(req.query.date, 'month');
  getAllInMonthFn(dates, req.user, function(todos){
    if (!todos) {
      return res.status(400).send({
        message: 'something went wrong'
      });
    }else{
      res.json(todos);
    }
  });
};

function getAllInMonthFn(dates, user, callback){
  Spear.find({
    'spear_type':{$in: ['day','week']},
    'begin': { '$gte': dates[0] },
    'end': { '$lte': dates[1] },
    'belongs_to': user
  }).populate('type', 'title').exec(function (err, spears){
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      callback(null);
    } else {
      var spearIds = spears.map(function(sp){
        return sp._id;
      });
      Todo.find({
        'spear':{$in: spearIds},
        'belongsTo': user,
        'reportInmonth': true
      }).populate('spear', 'spear_type begin').exec(function (err, todos) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(null);
        }else{
          callback(todos);
        }
      });
    }
  });
}

function getAllSpearDatafn(type, startDate, endDate, user, callbackfn){
  Spear.find({
    'spear_type': type,
    'begin': { '$gte': startDate, '$lte': endDate },
    // 'end': { '$lte': endDate },
    'belongs_to': user
  }).sort('-begin').populate('type', 'title').exec(function (err, spears){
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      // return res.status(400).send({
      //   message: errorHandler.getErrorMessage(err)
      // });
      callbackfn(null);
    } else {
      // var spearIds = spears.map(function(sp){
      //   return sp._id;
      // });
      var retVal = {};
      async.forEachOf(spears, function (spear, key, callback) {
        populateSpearData(spear,type, true, false, function(data){
          if(!(key in retVal)){retVal[key] = {};}
          if(!data){callback("error in finding todos");}
          else{
            retVal[key].anaRels = data.anaRels;
            if(type === 'quarter'){retVal[key].goals = data.todos;}
            else{retVal[key].todos = data.todos;}
            callback(err, "");
          }
        });
      //   async.parallel([
      //   function(callbackpl){
      //     if(type === 'quarter'){
      //       findGoalsForSpear(spear._id, function(goals){
      //         if(goals){
      //           if(!(key in retVal)){retVal[key] = {};}
      //           retVal[key].goals = goals;
      //           callbackpl(null);
      //         }else{
      //           callbackpl("error in finding goals");
      //         }
      //       });
      //     }else{
      //       findTodoForSpear(spear._id, function(todos){
      //         if(todos){
      //           if(!(key in retVal)){retVal[key] = {};}
      //           retVal[key].todos = todos;
      //           callbackpl(null);
      //         }else{
      //           callbackpl("error in finding todos");
      //         }
      //       });
      //     }
      //   },
      //   function(callbackpl){
      //     findAnaRelForSpear(spear._id, function(anaRels){
      //       if(anaRels){
      //           if(!(key in retVal)){retVal[key] = {};}
      //           var proc = processAnnaRel(anaRels);
      //           retVal[key].anaRels = proc;
      //           callbackpl(null);
      //         }else{
      //           callbackpl("error in finding anaRels");
      //         }
      //     });
      //   },
      // ], function(err, results){
      //   if (err) {
      //     callback(null);
      //   }
      //   callback(err, results);
      // });

      },function (err, results) {
        if (err) callbackfn(err.message);
        // configs is now a map of JSON data
        // doSomethingWith(configs);
        // res.json([retVal]);
        // console.log(retVal);
        callbackfn(null, retVal);
      });
    }
  });
}

exports.getAllSpearData = function(req, res){
  if(req.query.startDate && req.query.endDate){}
  getAllSpearDatafn(
    req.query.type, req.query.startDate,
    req.query.endDate, req.user, function(err, results){
      if (err) {
        console.log(errorHandler.getErrorMessage(err));
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else{
        res.json([results]);
      }
  });
};

var j = schedule.scheduleJob('30 6 * * *', function(){
  var today = moment();
  console.log('digest called');
  async.parallel([
      function(callback){
        // day Realignment Reports sent to admin / manager/ supervisor
        generateUserReportsofType('day', function(){
          callback('',"result");
        });
      },
      function(callback){
        // day user digest Reports sent to the user
        generateUserDigestReportsofType('day', function(){
          callback('',"result");
        });
      },
      function(callback){
        // weekly Realignment Reports sent to admin / manager/ supervisor
        if(IsTodayStartOf(today, 'week')){
          generateUserReportsofType('week', function(){
            callback('',"result");
          });
        }
      },
      function(callback){
        // week user digest Reports sent to the user
        if(IsTodayStartOf(today, 'week')){
          generateUserDigestReportsofType('week', function(){
            callback('',"result");
          });
        }
      },
      function(callback){
        // month Realignment Reports sent to admin / manager/ supervisor
        if(IsTodayEndOf(today, 'month')){
          generateUserReportsofType('month', function(){
            callback('',"result");
          });
        }
      },
      function(callback){
        // quarter Realignment Reports sent to admin / manager/ supervisor
        if(IsTodayEndOf(today, 'quarter')){
          generateUserReportsofType('quarter', function(){
            callback('',"result");
          });
        }
      },
      function(callback){
        // annual Realignment Reports sent to admin / manager/ supervisor
        if(IsTodayEndOf(today, 'year')){
          generateUserReportsofType('annual', function(){
            callback('',"result");
          });
        }
      },

    ],function(err,results){

  });
});

// Realignment Reports sent to admin / manager/ supervisor
function generateUserReportsofType(period, callbackfn){
  User.find({adminReports: period}).populate('account', 'Admin').populate('Admin','email').exec(function(err, users){
    if(!users){return callbackfn(null);}
    var date = moment().format('YYYY-MM-DD');
    console.log(period);
    console.log(users.length);
    async.forEachOf(users, function (user, key, callback) {
      //generate pdf and email them..
      // var email = user.account.admin.email;
      var recvUser = user.manager?user.manager:user.account.Admin;
      User.findById(recvUser, function(err, admin){
        if(!admin || !admin.email){return callbackfn(null);}
          // console.log(user.account.Admin.email);
          emailReportFn(date, user, period, admin.email, function(data){
            if(!data){console.log("been some problem sending report email");
            }else{
              // console.log('report sent successfully');
            }
            callback();
          });
      });

    }, function(err){
      if(err)return callbackfn(err);
      callbackfn("");
    });
  });
}

function generateUserDigestReportsofType(period, callbackfn){
  var query = {'userDailyReport': true};
  if (period === 'week'){query = {'userWeeklyReport': true};}
  User.find(query).populate('account', 'Admin').populate('Admin','email').exec(function(err, users){
    if(!users){return callbackfn(null);}
    var date = moment().format('YYYY-MM-DD');
    async.forEachOf(users, function (user, key, callback) {
      console.log(user.firstName);
      var data = {};
      data.day = {};
      data.week = {};
      async.parallel([
        function(callbackpll){
          // plans for current period day/week
          module.exports.findOrCreateSpearByDate(period, date, user, function(curSpear){
            if (curSpear && curSpear._id){
              findTodoForSpear(curSpear._id, function(todos){
                data[period] = todos;
                // console.log(todos);
                callbackpll(null, curSpear);
              });
            }else{
              console.log('err');
              callbackpll('err',curSpear);
            }
          });
        },
        function(callbackpll){
          // plans for the week if the period is day
          if (period === 'day') {
            module.exports.findOrCreateSpearByDate('week', date, user, function(curSpear){
              if (curSpear && curSpear._id){
                findTodoForSpear(curSpear._id, function(todos){
                // console.log(todos);
                  data.week = todos;
                callbackpll(null, curSpear);
                });
              }
            });
          }else{
              console.log('err');
            callbackpll(null, "day");
          }
        },
        function(callbackpll){
          // todos due this week
          Todo.find({'belongsTo': user, 'spear': null, 'quarterGoal': null,
          'due_by': {'$lte':moment().endOf('week').toISOString(),
              '$gte': moment().toISOString()}}).exec(function(err, todos){
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callbackpll(err);
            }else{
                // console.log(todos);
              data.todoDue = todos;
              callbackpll(null, todos);
            }
          });
        },
        function(callbackpll){
          // todos past due date
          Todo.find({'belongsTo': user, 'spear': null, 'quarterGoal': null,
          'due_by': {'$lte':moment().toISOString()}}).exec(function(err, todos){
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callbackpll(err);
            }else{
                // console.log(todos);
              data.todosPastDie = todos;
              callbackpll(null, todos);
            }
          });
        },
        ], function(err, result){
          if (err) {
              console.log('err');
            console.log({
              message: errorHandler.getErrorMessage(err)
            });
            callback(err);
          }
          var html = fs.readFileSync('modules/spear/server/templates/user-digest-email.server.view.html', 'utf8');
          var template = handlebars.compile(html);
          var context = {
            user: user,
            displayDay: period === 'day'?true:false,
            day: data.day,
            week: data.week,
            todoDue: data.todoDue,
            todoPastDue: data.todosPastDie,
          };
          handlebars.registerHelper('dateFormat', function(date, block) {
              return moment(date).format('MM-DD-YYYY');
          });
          var rendered = template(context);
          var title = 'Daily';
          if (period === 'week'){title = 'Weekly';}
          console.log('generated template');
          var mailOptions = {
              to: user.email,
              from: config.mailer.from,
              // replyTo: nonoe,
              subject: title + ' Digest for '+  moment().format("YYYY-MM-DD"),
              html: rendered,
              // attachments:[{
              //   filename: filename,
              //   content: stream,
              //   contentType: 'application/pdf'
              // }]
            };
            smtpTransport.sendMail(mailOptions, function (err) {
              if (!err) {
                console.log("Digest Report sent successfully");
                callback();
              } else {
              console.log('err');
              console.log(err);
              callback();
                // return console.log();
              }
              // done(err);
            });
        });
        }, function(err){
          if(err)return callbackfn(err);
          callbackfn("");
        });
      });
}

function IsTodayStartOf(today, period){
  return moment(today).isSame(moment().startOf(period), 'day');
  // today = moment(today).startOf('day');
  // if (today === moment(today).startOf(period)){return true;}
  // return false;
}
function IsTodayEndOf(today, period){
  return moment(today).isSame(moment().endOf(period), 'day');
  // today = moment(today).endOf('day');
  // if (today === moment(today).endOf(period)){return true;}
  // return false;
}
