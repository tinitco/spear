'use strict';

var spear = require('../controllers/spear.server.controller');

module.exports = function(app) {
  // Routing logic
  // ...
  app.route('/api/spear')
    .get(spear.list)
    .post(spear.create);

  app.route('api/spear/:date/:type')
    .get(spear.read);

  // Single article routes
  app.route('/api/spear/:spearId')
    .get(spear.read)
    .put(spear.update)
    .delete(spear.delete);

  // Finish by binding the article middleware
  app.param('spearId', spear.spearByID);
  app.param(['date', 'type'], spear.spearByID);

  app.route('/api/reports')
    .get(spear.getreport);

  app.route('/api/emailreports')
    .get(spear.emailReport);

  app.route('/api/reports/month')
    .get(spear.getAllInMonth);

  app.route('/api/reports/allData')
    .get(spear.getAllSpearData);

  app.route('/api/spearsearch')
    .get(spear.getAllInQuarter);
};
