'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Spear Schema
 */
var SpearSchema = new Schema({
  // Spear model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  begin: {
    type: Date,
    // default: Date.now
  },
  end: {
    type: Date,
    // default: Date.now
  },
  spear_type: {
    type: String,
    trim: true,
    required: 'Spear Type cannot be blank'
  },
  account: {
    type: Schema.ObjectId,
    ref: 'Account',
    // required: 'You\'ll need to choose an account'
  },
  belongs_to: {
    type: Schema.ObjectId,
    ref: 'User',
    required: 'User Not set'
  }

});

mongoose.model('Spear', SpearSchema);
