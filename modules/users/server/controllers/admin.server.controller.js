'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  nodemailer = require('nodemailer'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var smtpTransport = nodemailer.createTransport(config.mailer.options);

/**
 * Show the current user
 */
exports.read = function (req, res) {
  res.json(req.model);
};


/**
 * Create a User - for System Managers / super users only
 */
exports.create = function (req, res) {
  var user = new User(req.body);
  var pass = 'dummyPass_12';
  user.password = pass;
  if(req.body.account && req.body.account === 'none'){
    user.account = req.user.account;
  }
  user.displayName = user.firstName + ' ' + user.lastName;
  // console.log(user.email);
  // user.user = req.user;
  user.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      user.password = undefined;
      user.salt = undefined;
      sendWelcomeMail(user,res, function(err, message){
        if(err){}else {}
      });
      res.json(user);
    }
  });
};

function sendWelcomeMail(user, res, callback) {
  // body...
  var httpTransport = 'http://';
  if (config.secure && config.secure.ssl === true) {
    httpTransport = 'https://';
  }
  res.render(path.resolve('modules/users/server/templates/user-welcome-email'), {
    name: user.displayName,
    appName: config.app.title,
    username: user.username,
    // password: pass,
    // url: httpTransport + req.headers.host + '/api/auth/reset/' + token
  }, function (err, emailHTML) {
    // done(err, emailHTML, user);
    var mailOptions = {
      to: user.email,
      from: config.mailer.from,
      subject: 'Your Account Created',
      html: emailHTML
    };
    smtpTransport.sendMail(mailOptions, function (err) {
      if (!err) {
        // res.json(user);
        // res.send({
        //   message: 'An email has been sent to the provided email with further instructions.'
        // });
        callback(null, {
          message: 'An email has been sent to the provided email with further instructions.'
        });
      } else {
        // return res.status(400).send({
        //   message: err
        // });
        callback(err);
      }
  });
  });
}

/**
 * Update a User
 */
exports.update = function (req, res) {
  var user = req.model;
  // console.log('check to go');
  checkRoles(user, req, function(go){
    // console.log('just got the go');
    if(go){
      //For security purposes only merge these parameters
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.displayName = user.firstName + ' ' + user.lastName;

      user.roles = req.body.roles;
      user.adminReports = req.body.adminReports;
      user.manager = req.body.manager;
      if(user.email !== req.body.email){
        sendWelcomeMail(user, res, function(err, message){
          if (err) {} else {}
        });
        user.email = req.body.email;
      }

      user.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }
        user.password = undefined;
        user.salt = undefined;
        res.json(user);
      });
    }else{
      res.json(user);
      // return res.status(400).send({
      //   message: 'could not change roles'
      // });
    }
  });
};

function checkRoles(user, req, callback) {
  // body...
  if (req.body.roles.indexOf('admin')!== -1){
    return callback(true);
  }
  else if((user.roles.indexOf('admin') !== -1) && (req.body.roles.indexOf('admin') === -1)){
    // if the user was admin and not any more..
    // we need to check if the account has any more admins other than this person
    User.find({'account': user.account, 'roles': { '$in': ['admin'] }}).exec(function (err, users) {
      if(err){callback(false);}
      else if(users.length>1){
        console.log(users);
         callback(true);
      }
      callback(false);
    });
  }
  callback(false);
}

/**
 * Delete a user
 */
exports.delete = function (req, res) {
  var user = req.model;

  user.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    res.json(user);
  });
};



/**
 * User middleware
 */
exports.userByID = function (req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'User is invalid'
    });
  }

  User.findById(id, '-salt -password').exec(function (err, user) {
    if (err) {
      return next(err);
    } else if (!user) {
      return next(new Error('Failed to load user ' + id));
    }

    req.model = user;
    next();
  });
};
