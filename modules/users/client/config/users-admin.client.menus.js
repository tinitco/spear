'use strict';

// Configuring the Users module
angular.module('users.admin').run(['Menus',
  function (Menus) {
    // Menus.addSubMenuItem('topbar', 'admin', {
    //   title: 'Account Users',
    //   state: 'admin.users'
    // });

    Menus.addSubMenuItem('topbar', 'superAdmin', {
      title: 'All Users',
      state: 'admin.allusers'
    });

    Menus.addSubMenuItem('topbar', 'superAdmin', {
      title: 'Super Users',
      state: 'admin.superUsers'
    });
  }
]);
