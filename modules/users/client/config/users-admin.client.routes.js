'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/admin-list-users.client.view.html',
        // controller: 'UserListController',
        data: {

          pageTitle: 'Manage Account Users '
        },
        params:{
          context: 'adminUsers'
        }
      })
      .state('admin.allusers', {
        url: '/allusers',
        templateUrl: 'modules/users/client/views/admin/allusers-list-users.client.view.html',
        controller: 'UserListController',
        data: {
          roles: ['superAdmin'],
          pageTitle: 'All Users '
        },
        params:{
          context: 'allUsers'
        }
      })
      .state('admin.superUsers', {
        url: '/superusers',
        templateUrl: 'modules/users/client/views/admin/superusers-list-users.client.view.html',
        controller: 'UserListController',
        data: {
          roles: ['superAdmin'],
          pageTitle: 'Super Users '
        },
        params:{
          context: 'superUsers'
        }
      })
      .state('admin.user', {
        url: '/users/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        },
        data: {
          pageTitle: 'Edit {{ userResolve.displayName }}'
        }
      })
      .state('admin.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        },
        data: {
          pageTitle: 'Edit User {{ userResolve.displayName }}'
        }
      });
  }
]);
