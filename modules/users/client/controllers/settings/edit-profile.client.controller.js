'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$http', '$location', 'Users', 'Authentication', '$mdDialog',
  function ($scope, $http, $location, Users, Authentication, $mdDialog) {
    $scope.user = Authentication.user;

    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = new Users($scope.user);

      user.$update(function (response) {
        $scope.$broadcast('show-errors-reset', 'userForm');

        $scope.success = true;
        Authentication.user = response;
      }, function (response) {
        $scope.error = response.data.message;
      });
    };

    $scope.changePassword = function(){
      $mdDialog.show({
        templateUrl: 'modules/users/client/views/settings/change-password.client.view.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,
        fullscreen: true
      });
    };

    $scope.toggleDigest = function(digest){
      if(digest === 'day'){$scope.user.userDailyReport = !$scope.user.userDailyReport;}
      if(digest === 'week'){$scope.user.userWeeklyReport = !$scope.user.userWeeklyReport;}
      $scope.updateUserProfile(true);
    };

  }
]);
