'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', '$mdDialog','$timeout', 'Authentication', 'PasswordValidator',
  function ($scope, $http, $mdDialog,$timeout, Authentication, PasswordValidator) {
    $scope.user = Authentication.user;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    // Change user password
    $scope.changeUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'passwordForm');

        return false;
      }

      $http.post('/api/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.$broadcast('show-errors-reset', 'passwordForm');
        $scope.success = true;
        $scope.passwordDetails = null;
        $timeout(function(){
          $mdDialog.hide();
        },600);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
  }
]);
