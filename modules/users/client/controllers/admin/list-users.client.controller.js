(function () {
'use strict';

angular
  .module('users.admin')
  .controller('UserListController', UserListController);

 UserListController.$inject = ['$scope', '$stateParams','Authentication',  '$filter', 'Admin',
 '$mdDialog', '$mdMedia', 'Notification'];

function UserListController ($scope, $stateParams, Authentication, $filter, Admin, $mdDialog, $mdMedia, Notification) {

    $scope.context = 'adminUsers';

    $scope.isUser = function(user, role){
      if (user.roles.indexOf(role)!== -1){return true;}
      return false;
    };


    $scope.managerView = false;
    if($scope.isUser(Authentication.user,'manager')){
      $scope.managerView = true;
    }
    if($scope.isUser(Authentication.user,'admin')){
      $scope.managerView = false;
    }


    if ($stateParams.context){$scope.context = $stateParams.context;}
    // if ($attrs.context){$scope.context = $attrs.context;}
    Admin.query({list: $scope.context}, function (data) {
      $scope.users = data;
      $scope.getManagers();
      $scope.buildPager();
    });

    $scope.getManagers = function(){
      $scope.managers = $.grep($scope.users, function(a){
        return (a.roles.indexOf('manager') !== -1);
      });
    };


    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.showUser = function(user) {
      if(!user.manager){return 'Not Set';}
        var selected = $.grep($scope.users, function( a ) {
            return (a._id === user.manager);
        });
        return (selected.length) ? selected[0].firstName : 'Not set';
    };

    $scope.editUser = function(user){
      var data = {
        title:'User'
      };
      var managers = $scope.managers;
      if ($scope.context === 'adminUsers'){managers = {};}
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: EditUserPopupDialogController,
        templateUrl: 'modules/users/client/views/admin/edit-user.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals:{
          data: data,
          user_orig: user,
          managers: $scope.managers,
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(editedUser) {
        if(editedUser.delete){
          $scope.deleteUser(user);
          return;
        }
        user.firstName = editedUser.firstName;
        user.lastName = editedUser.lastName;
        user.email = editedUser.email;
        user.roles = editedUser.roles;
        user.adminReports = editedUser.adminReports;
        user.manager = editedUser.manager;
        user.displayName = user.firstName + " " + user.lastName;
        $scope.updateUser(user);
        // $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });

    };

    $scope.shouldDisplayUser = function(user){
      if(!$scope.managerView){return true;}
      if(user.manager === Authentication.user._id){return true;}
      return false;
    };

    $scope.deleteUser = function (user) {
      var confirm = $mdDialog.confirm()
            .title('Delete User ?')
            .textContent('Do you really want to delete this User? \n All of the user\'s data will be lost')
            .ariaLabel('Delete')
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        user.active = !user.active;
        user.$delete({},function(){
            $scope.users.splice( $scope.users.indexOf(user), 1 );
        });
      }, function() {});
    };

    $scope.updateAssignedTo= function (user, data) {
      if(data){
        user.manager = data;
        $scope.updateUser(user);
      }
    };

    $scope.toggleUserActive = function (user) {
      var confirm = $mdDialog.confirm()
            .title('Change User Status ?')
            .textContent('Are you sure you want to change this User\'s Status?')
            .ariaLabel('change Status')
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        user.active = !user.active;
        $scope.updateUser(user);
      }, function() {});
    };

    $scope.assignManager = function(user){
      var confirm = $mdDialog.confirm()
            .title('Change User Role ?')
            .textContent('Are you sure you want to change this User\'s Status?')
            .ariaLabel('change Status')
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        // user.active = !user.active;
        if($scope.isUser(user, 'manager')){
          user.roles.splice( user.roles.indexOf('manager'), 1 );
        }else{
          user.roles.push('manager');
        }
        $scope.updateUser(user);
      }, function() {});
    };

    $scope.toggleUserRole = function (user) {
      var confirm = $mdDialog.confirm()
            .title('Change User Role ?')
            .textContent('Managers would be able to access Account Settings?')
            .ariaLabel('change Status')
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        // user.active = !user.active;
        // user.toggleRole = true;
        if($scope.isUser(user, 'admin')){
          user.roles.splice( user.roles.indexOf('admin'), 1 );
        }else{
          user.roles.push('admin');
          if(user.roles.indexOf('manager') == -1){
            user.roles.push('manager'); // an admin should be a manager as well.
          }
        }
        $scope.updateUser(user);
      }, function() {});

      // var dlg = dialogs.confirm("Change User Role?","Managers would be able to access Account Settings?", {size:'md'});
      // dlg.result.then(function(btn){
      //     user.toggleRole = true;
      //     $scope.updateUser(user);
      // },function(btn){});
    };

    $scope.reportIn = function(user, period){
      if(user.adminReports.indexOf(period) === -1){
        user.adminReports.push(period);
      }else{
        user.adminReports.splice( user.adminReports.indexOf(period), 1 );
      }
      $scope.updateUser(user);
    };

    $scope.updateUser = function(user){
      user.$update(function(){
        Notification.success('User Saved Successfully');
      });
    };

    $scope.reportSelected = function(user, period){
      if(user.adminReports.indexOf(period) !== -1){return true;}
      return false;
    };

    $scope.createSuperUser = function () {
      var data = {
        title:'System Manager'
      };
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewSuperUserPopupDialogController,
        templateUrl: 'modules/users/client/views/admin/new-superuser.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals:{
          data: data
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo) {
        // $scope.status = 'You said the information was "' + answer + '".';
        if(data.username && data.email){
          var ac = new Admin();
          ac.username = data.username;
          ac.email = data.email;
          ac.firstName = data.firstName;
          ac.lastName = data.lastName;
          ac.roles = ['superAdmin'];
          ac.provider = 'local';
          ac.$save({}, function () {
            $scope.users.push(ac);
            Notification.success('Super User Created Successfully');
          }, function(err){
            Notification.error('Problem creating Super User ' + err.data.message);

          });
        }
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });

    };


    $scope.createAccountUser = function () {
      var data = {
        title:'Account User'
      };
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewSuperUserPopupDialogController,
        templateUrl: 'modules/users/client/views/admin/new-superuser.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals:{
          data: data
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo) {
        // $scope.status = 'You said the information was "' + answer + '".';
        if(data.username && data.email){
          var ac = new Admin();
          ac.username = data.email;
          ac.email = data.email;
          ac.firstName = data.firstName;
          ac.lastName = data.lastName;
          ac.roles = ['user'];
          ac.account = 'none';
          ac.provider = 'local';
          ac.$save({}, function () {
            $scope.users.push(ac);
            Notification.success('Account User Created Successfully');
          }, function(err){
            Notification.error('Problem creating Account User ' + err.data.message);
          });
        }
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });

    };

  }

})();


function NewSuperUserPopupDialogController($scope, $mdDialog, data) {
  'use strict';
  $scope.data = data;
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    if (!$scope.data.email || !$scope.data.firstName || !$scope.data.lastName){
      $scope.errors = 'Please fill All Three fields';
      return;
    }
    $scope.data.username = $scope.data.email;
    $mdDialog.hide(answer);
  };
}

function EditUserPopupDialogController($scope, $mdDialog, data, user_orig, managers) {
  'use strict';
  $scope.data = data;
  $scope.user = $.extend(true,{},user_orig);
  $scope.managers = managers;

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  var user = $scope.user;

  $scope.toggleActiveCheck = function(){
    user.active = !user.active;
  };
  $scope.isUser = function(user,role){
    if (user.roles.indexOf(role)!== -1){
      return true;
    }
    return false;
  };

  $scope.toggleAdminCheck = function(){
    if($scope.isUser(user, 'admin')){
      user.roles.splice( user.roles.indexOf('admin'), 1 );
    }else{
      user.roles.push('admin');
      if($scope.isUser(user, 'manager')){
        user.roles.push('manager');
      }
    }
  };

  $scope.toggleManagerCheck = function(){
    if($scope.isUser(user, 'manager')){
      user.roles.splice( user.roles.indexOf('manager'), 1 );
    }else {
      user.roles.push('manager');
    }
  };
  $scope.reportIn = function(user, period){
    if($scope.reportSelected(user, period)){
      user.adminReports.push(period);
    }else{
      user.adminReports.splice( user.adminReports.indexOf(period), 1 );
    }
  };
  $scope.reportSelected = function(user, period){
    if(user.adminReports.indexOf(period) !== -1){return true;}
    return false;
  };

  $scope.deleteUser = function () {
    $mdDialog.hide({delete: true});
  }

  // $scope.userRoles = function(){
  //     for (var i = 0; i < this.length; i++) {
  //       if (this[i] == element){
  //   Array.prototype.contains = function (element) {
  //         return true;
  //       }
  //     }
  //     return false;
  //   };
  //   if(user.roles.contains('admin')) {
  //     return true;
  //   }
  //   if(user.roles.contains('manager')) {
  //     return true;
  //   }
  // };

  // $scope.userInfo = $.extend(true,{},user);

  $scope.save = function(user) {
    $mdDialog.hide($scope.user);
  };
}
