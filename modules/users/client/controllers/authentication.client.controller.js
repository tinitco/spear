'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$state', 
  '$stateParams', '$http', '$location', '$window', 'Authentication', 'PasswordValidator', 
  'AccountTypesService', 'AccountsService', 'Users',
  function ($scope, $state, $stateParams, $http, $location, $window, 
    Authentication, PasswordValidator, AccountTypesService, AccountsService, Users) {

    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();
    $scope.accountTypes = AccountTypesService.query();
    $scope.accountId = null;
    $scope.showProviders = false;
    if($stateParams.accountId && $stateParams.accountId !== null){
      $scope.accountId = $stateParams.accountId;
    }


    // Get an eventual error defined in the URL query string:
    $scope.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    function getAccountType(type){
      type = typeof type!== 'undefined'? type: 'Professional';
      for (var i = 0; i < $scope.accountTypes.length; i++) {
        if ($scope.accountTypes[i].title === type) {return $scope.accountTypes[i];}
      }
    }

    $scope.signup = function (isValid) {
      $scope.error = null;
      $scope.credentials.lastName = 'lastName';

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }
      var account = new AccountsService();
      account.title =  $scope.credentials.firstName + "\'s account";
      account.type =  getAccountType()._id;
      $http.post('/api/auth/signup', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        var usr = new Users(response);
        account.Admin = usr;
        // usr.roles.push("admin");
        account.$save({}, function(){
          usr.account = account._id; 
          usr.$update();
          account.$update({},function(){
            $window.location.reload();
            $state.go($state.previous.state.name || 'home', {}, {
              reload: true,
              inherit: false,
              notify: true
            });
          }, function(response){
            $scope.error = response.message;
          });
        }, function(response){
            $scope.error = response.message;
        }, function(response) {
          // body...
          $scope.error = response.message;
        });

        // And redirect to the previous or home page

        // $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    $scope.signin = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    // OAuth provider request
    $scope.callOauthProvider = function (url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    };
  }
]);
