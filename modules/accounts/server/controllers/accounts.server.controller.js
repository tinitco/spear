'use strict';

/**
 * Module dependencies.
 */
/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  Account = mongoose.model('Account'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  User = mongoose.model('User'),
  TodoCategories = mongoose.model('TodoCategories'),
  Spear = mongoose.model('Spear'),
  Todo = mongoose.model('Todo'),
  AnalysisRealign = mongoose.model('AnalysisRealign'),
  UserSettings = mongoose.model('UserSettings'),
  UserCalendars = mongoose.model('UserCalendars'),
  QGoals = mongoose.model('QuarterGoals');

var schedule = require('node-schedule'),
  moment = require('moment');
var fs = require('fs'),
  config = require(path.resolve('./config/config')),
  async = require('async'),
  nodemailer = require('nodemailer'),
  smtpTransport = nodemailer.createTransport(config.mailer.options);
var handlebars = require('handlebars');

/**
 * Create a
 */
exports.create = function (req, res) {
  var account = new Account(req.body);
  // console.log(req.user);
  var user = req.user;
  account.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      user.roles.push("admin");
      user.roles.push("manager");
      user.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(account);
        }
      });
    }
  });
};

/**
 * Show the current
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var account = req.account ? req.account.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // account.isCurrentUserOwner = req.user && account.user && account.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(account);
};

/**
 * Update a
 */
exports.update = function (req, res) {
  var account = req.account;

  account.title = req.body.title;
  account.active = req.body.active;
  account.type = req.body.type;
  account.Admin = req.body.Admin;

  // account.max_users = req.body.max_users;

  account.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(account);
    }
  });

};

exports.merge = function (req, res) {
  Account.findById(req.body.masterAccount._id).populate('type', 'title').exec(function (err, masterAccount) {
    if (err) {
      return res.status(400).send({message: errorHandler.getErrorMessage(err)});
    } else if (!masterAccount) {
      return res.status(404).send({message: 'No masterAccount with that identifier has been found'});
    }else{
      // var masterAccount = masterAccount;
      var accountIds = req.body.accounts.map(function(sp){
        return sp._id;
      });
      if(accountIds.indexOf(masterAccount._id)!== -1){
        accountIds.splice(accountIds.indexOf(masterAccount._id), 1);
      }
      // console.log(accountIds);
      // console.log(accountIds.indexOf(masterAccount._id));
      // res.status(200).send({message: 'Account Merged seccessfully'});
      User.update({account:{$in: accountIds}}, {account: masterAccount}, {multi:true}).exec(function (err, account) {
        if (err) {
          return res.status(400).send({message: errorHandler.getErrorMessage(err)});
        } else {
          TodoCategories.find({account: masterAccount}).exec(function(err, todoCategories){
            if (err) {
              return res.status(400).send({message: errorHandler.getErrorMessage(err)});
            }else{
              var todoTitles = todoCategories.map(function(sp){
                return sp.title;
              });
              TodoCategories.update({account:{$in: accountIds}},
                {account: masterAccount}, {multi:true}).exec(function (err, todoCategories) {
                if (err) {
                  return res.status(400).send({message: errorHandler.getErrorMessage(err)});
                } else {
                  Account.find({_id: {$in:accountIds}}).remove().exec(function(err, success){
                  TodoCategories.find({account: {$in:accountIds}}).remove().exec(function(err, success){
                  res.status(200).send({message: 'Account Merged seccessfully'});
                  });
                  });
                }
              });
            }
          });
          // res.json(account);
        }
      });
    }
  });
};

/**
 * Delete an
 */
exports.delete = function (req, res) {
  var account = req.account;
  // delete users, spears, todos, todo categories,
  // anarel, q goals, user settings, calenders and finally the account
  if (!account){
    return res.status(400).send({message: "No acocunt to delete"});
  }else{
    // var data = [];
    User.find({account: account}).exec(function(err, users){
      if (err) {
        return res.status(400).send({message: errorHandler.getErrorMessage(err)});
      }else{
        var userIds = users.map(function(sp){
          return sp._id;
        });
        // async.forEachOf(users, function(user, index, callback){
          // var userData = {user:user};
        async.parallel([
          function(callback){
            Todo.find({'belongsTo': {$in: userIds} }).remove().exec(function (err, todo) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          },
          function(callback){
            Spear.find({'belongs_to': {$in: userIds} }).remove().exec(function (err, todo) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          },
          function(callback){
            QGoals.find({'belongsTo': {$in: userIds} }).remove().exec(function (err, todo) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          },
          function(callback){
            UserSettings.find({'belongsTo': {$in: userIds}}).remove().exec(function (err, settings) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          },
          function(callback){
            UserCalendars.find({'belongsTo': {$in: userIds}}).remove().exec(function (err, calendars) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          },
          function(callback){
            AnalysisRealign.find({'belongsTo': {$in: userIds} }).remove().exec(function (err, anaRelcount) {
              if (err) {callback(err);}
              else {callback(null, "");}
            });
          }],function(err){
          if(err){
            return res.status(400).send({message: errorHandler.getErrorMessage(err)});
          }else{
            User.find({account: account}).remove().exec(function(err, users){
              if (err) {
                return res.status(400).send({message: errorHandler.getErrorMessage(err)});
              }else{
                TodoCategories.find({'account': account }).remove().exec(function (err, todo) {
                  if (err) {
                    return res.status(400).send({message: errorHandler.getErrorMessage(err)});
                  }else{
                      // data.push(todo);
                    account.remove(function (err) {
                      if (err) {
                        return res.status(400).send({message: errorHandler.getErrorMessage(err)});
                      } else {
                        res.json(account);}
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
};

/**
 * List of
 */
exports.list = function (req, res) {
  if(req.query.self){
  // console.log(req.query);
    Account.find({_id:req.user.account}).sort('-created').populate('type', '').exec(function (err, account) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(account);
      }
    });
  }else{

  Account.find().sort('-created').populate('type', '').populate('Admin').exec(function (err, account) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(account);
    }
  });
  }
};


/**
 * Article middleware
 */
exports.accountByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Account is invalid'
    });
  }

  Account.findById(id).populate('type', 'title').exec(function (err, account) {
    if (err) {
      return next(err);
    } else if (!account) {
      return res.status(404).send({
        message: 'No account with that identifier has been found'
      });
    }
    req.account = account;
    next();
  });
};

// schedule job to send super admin status reports

var j = schedule.scheduleJob('0 6 * * *', function(){
  var md = moment().add(-1, 'days');
  var dates = [moment(md).startOf('day').toISOString(), moment(md).endOf('day').toISOString()];
  // console.log(md);
  var data = {};
  console.log('admin report called');
  async.parallel([
    function(callback){
      Account.find({'created': { '$gte': dates[0],  '$lte': dates[1] }}).exec(function (err, accounts) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          data.accountsday = accounts;
          callback(null, {"accountsDay": accounts});
        }
      });
    },
    function(callback){
      var week = [moment(md).startOf('week').toISOString(), moment(md).endOf('week').toISOString()];
      // console.log(md);
      Account.find({'created': { '$gte': week[0],  '$lte': week[1] }}).exec(function (err, accounts) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          data.accountsweek = accounts;
          callback(null, {"accountsWeek": accounts});
        }
      });
    },
    function(callback){
      var quarter = [moment(md).startOf('quarter').toISOString(), moment(md).endOf('quarter').toISOString()];
      // console.log(md);
      Account.find({'created': { '$gte': quarter[0],  '$lte': quarter[1] }}).exec(function (err, accounts) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          data.accountsquarter = accounts;
          callback(null, {"accountsQuarter": accounts});
        }
      });
    },
    function(callback){
      User.find({'created': { '$gte': dates[0],  '$lte': dates[1] }}).exec(function (err, users) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          data.users = users;
          callback(null, {"users": users});
        }
      });
    },
    function(callback){
      User.find({'lastLogin': { '$gte': dates[0]}}).exec(function (err, users) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          // console.log(users);
          data.logedInUsers = users;
          callback(null, {"users": users});
        }
      });
    },
    function(callback){
      User.find({'lastLogin': { '$lte': moment(md).add(-2, 'days').toISOString() }}).exec(function (err, users) {
        // console.log(md);
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          // console.log(users);
          data.lateUsers = users;
          callback(null, {"users": users});
        }
      });
    },
    function(callback){
      User.find({'roles': { '$in': ['superAdmin'] }}).exec(function (err, supers) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        }else{
          data.sendemails = supers.map(function(superuser){
              return superuser.email;
          });
          data.sendemails = data.sendemails.join();
          // data.users = users;
          callback(null, {"users": supers});
        }
      });
    },
    ], function(err, result){
      if (err) {
        return console.log({
          message: errorHandler.getErrorMessage(err)
        });
      }
      var html = fs.readFileSync('modules/accounts/server/templates/admin-report-email.server.view.html', 'utf8');
      var template = handlebars.compile(html);
      var context = {
        accounts: data.accounts,
        users: data.users,
        logedInUsers: data.logedInUsers,
        lateUsers: data.lateUsers,
      };
      handlebars.registerHelper('dateFormat', function(date, block) {
          return moment(date).format('MM-DD-YYYY');
      });
      var rendered = template(context);
      // console.log(md);
      console.log(md.format("YYYY-MM-DD"));
      var mailOptions = {
          to: data.sendemails,
          from: config.mailer.from,
          // replyTo: nonoe,
          subject: 'Admin Report for '+  md.format("YYYY-MM-DD"),
          html: rendered,
          // attachments:[{
          //   filename: filename,
          //   content: stream,
          //   contentType: 'application/pdf'
          // }]
        };
        smtpTransport.sendMail(mailOptions, function (err) {
          if (!err) {
            return console.log("Admin Report sent successfully");
          } else {
          console.log(err);
            // return console.log();
          }
          // done(err);
        });
    });

  // var sendemails = '';

});
