'use strict';

var accounts = require('../controllers/accounts.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/accounts')
    .get(accounts.list)
	.post(accounts.create);

  // Single article routes
  app.route('/api/accounts/:accountId')
    .get(accounts.read)
    .put(accounts.update)
    .delete(accounts.delete);

  app.route('/api/accountmerge')
    .post(accounts.merge);

  // Finish by binding the article middleware
  app.param('accountId', accounts.accountByID);
};
