'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Account Schema
 */
var AccountSchema = new Schema({
  // Account model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  },
  title: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  profileImageURL: {
    type: String,
    default: 'modules/users/client/img/profile/default.png'
  },
  type: {
    type: Schema.ObjectId,
    ref: 'AccountTypes',
    required: 'You\'ll need to choose an account type'
  },
  Admin: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Account', AccountSchema);
