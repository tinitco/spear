(function () {
  'use strict';

  angular
    .module('accounts')
    .controller('AccountsViewController', AccountsViewController);

  AccountsViewController.$inject = ['$scope', '$state', 'Authentication',
   'AccountsService', 'AccountTypesService', '$mdDialog', '$mdMedia'];

  function AccountsViewController($scope, $state, Authentication, AccountsService, AccountTypesService,
    $mdDialog, $mdMedia) {
    var vm = this;

    $scope.accountTypes = AccountTypesService.query();

    var accounts = AccountsService.query({self:true}, function(){
      $scope.account = accounts[0];
      $scope.account.days = 6;
      $scope.account.weekStart = 'Sunday';
    });

    $scope.daysInWeek = [5,6,7];
    $scope.weekStratsOn = ['Sunday', 'Monday'];
    
    $scope.showAccountType = function() {
      if(!$scope.account || !$scope.account.type){return 'Not Set';}
        var selected = $.grep($scope.accountTypes, function( a ) {
            return (a._id === $scope.account.type._id || a._id === $scope.account.type);
        });
        return ($scope.account.type && selected.length) ? selected[0].title : 'Not set';
    };

    $scope.updateAccount = function(){
      $scope.account.$update(function(){});
    };

    $scope.isUserAdmin = function(){
      if((Authentication.user.roles.indexOf('admin') !== -1 ||
        Authentication.user.roles.indexOf('manager') !== -1)
        // ||($scope.account.admin && $scope.account.admin === Authentication.user)
        ) {return true;}
      return false;
    };
  }

})();
