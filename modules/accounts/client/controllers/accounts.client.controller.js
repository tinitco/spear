(function () {
  'use strict';

  angular
    .module('accounts')
    .controller('AccountsController', AccountsController);

  AccountsController.$inject = ['$scope', '$state', 'Authentication',
   'AccountsService','AccountTypesService', 'AccountMergeService', '$mdDialog', '$mdMedia', 'Notification'];

  function AccountsController($scope, $state, Authentication, AccountsService, AccountTypesService, AccountMergeService,
    $mdDialog, $mdMedia, Notification) {
    var vm = this;

    vm.accountSelected = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.createAccount = createAccount;
    vm.mergeAccounts = mergeAccounts;
    vm.accountSelected = accountSelected;
    vm.toggleStatus = toggleStatus;
    vm.editAccount = editAccount;
    vm.showAccountType = showAccountType;
    // Get
    vm.accounts = AccountsService.query();

    vm.selectedAccounts = [];
    // vm.save = save;
    $scope.accountTypes = AccountTypesService.query();

    function showAccountType(account){
      if(!account.type){return 'Not Set';}
        var selected = $.grep($scope.accountTypes, function( a ) {
            return (a._id === account.type || a._id === account.type._id);
        });
        return (account.type && selected.length) ? selected[0].title : 'Not set';
    }

    // update
    function toggleStatus(ev, account) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
            .title('Change Account Status?')
            .textContent('Are you sure you want to change the Account Status?')
            .ariaLabel('Change Status')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        account.active =!account.active;
        account.$update();
      }, function() {});
    }

    function accountSelected(account){
      if(vm.selectedAccounts.indexOf(account)!== -1){
        vm.selectedAccounts.splice( vm.selectedAccounts.indexOf(account), 1 );
        account.selected = false;
        return;
      }
      account.selected = true;
      vm.selectedAccounts.push(account);
    }

    function mergeAccounts(ev){
      if (!vm.selectedAccounts){
          Notification.error('You need to select some accounts first..'); 
      }
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: MergeAccountDialogController,
        templateUrl: 'modules/accounts/client/views/merge-account.client.template.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals:{
          accounts: vm.selectedAccounts
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(account) {
        // $scope.status = 'You said the information was "' + answer + '".';
        var ac = $.grep(vm.accounts, function( a ) {
          return (a._id === account.masterAccount.$viewValue);
        });
        ac = ac?ac[0]:null;

        console.log(ac);
        var acm = new AccountMergeService();
        // acm._id = ac._id;
        acm.masterAccount = ac;
        if(vm.selectedAccounts.indexOf(ac)!== -1){
          vm.selectedAccounts.splice( vm.selectedAccounts.indexOf(ac), 1 );
        }
        acm.accounts = vm.selectedAccounts;
        acm.$save({},function(){
          vm.selectedAccounts.forEach(function(sac){
            removeAccountFromList(sac);
            sac.selected = false;
          });
          Notification.success('Accounts successfully Merged..'); 
        vm.selectedAccounts = [];
        });
        // ac.title = account.accountName.$viewValue;
        // ac.type = account.masterAccount.$viewValue;
        // ac.billing_cycle = accountType.accountBillingCycle.$viewValue;
        // ac.$save({}, function () {
        //   vm.accounts.push(ac);
        // });

      }, function() {
        // vm.selectedAccounts = [];
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    function editAccount(account){
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewEditAccountDialogController,
        templateUrl: 'modules/accounts/client/views/edit-account.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals:{
          account: account
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(data) {
        // $scope.status = 'You said the information was "' + answer + '".';
        // var ac = new AccountsService();
        account.title = data.title;
        account.type = data.type;
        // ac.billing_cycle = accountType.accountBillingCycle.$viewValue;
        account.$update({}, function () {
          // vm.accounts.push(ac);
          Notification.success('Account successfully updated..'); 
        });
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    // create
    function createAccount(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      var ac = new AccountsService();
      $mdDialog.show({
        controller: NewAccountDialogController,
        templateUrl: 'modules/accounts/client/views/new-account.client.template.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals:{
          account: ac
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(account) {
        // $scope.status = 'You said the information was "' + answer + '".';
        // var ac = new AccountsService();
        ac.title = account.accountName.$viewValue;
        ac.type = account.accountType.$viewValue;
        // ac.billing_cycle = accountType.accountBillingCycle.$viewValue;
        ac.$save({}, function () {
          vm.accounts.push(ac);
          Notification.success('Account successfully created..'); 
        });
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    function removeAccountFromList(account){
      vm.accounts.splice( vm.accounts.indexOf(account), 1 );
    }

    // Delete  existing Account
    function remove(account) {
      var confirm = $mdDialog.confirm()
            .title('This Deletes every thing! ')
            .textContent('Are you sure you want to Delete the Account? All users and spear data will be lost.. Non Recoverable')
            .ariaLabel('Delete')
            // .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        account.$delete({},function(){
          removeAccountFromList(account);
          Notification.success('Account successfully Deleted..'); 
        });
      }, function() {
      });
    }
  }
})();

function NewEditAccountDialogController($scope, $mdDialog, AccountTypesService, account) {
  'use strict';
  $scope.account = account;
  $scope.accountpxy = {type:account.type._id, title: account.title};
  // $scope.accountTitle = account.title;

  // console.log($scope.account);
  $scope.accountTypes = AccountTypesService.query();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide($scope.accountpxy);
  };
}

function NewAccountDialogController($scope, $mdDialog, AccountTypesService, account) {
  'use strict';
  // $scope.account = account;
  // $scope.accountpxy = {type:account.type._id, title: account.title};
  // $scope.accountTitle = account.title;

  // console.log($scope.account);
  $scope.accountTypes = AccountTypesService.query();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}

function MergeAccountDialogController($scope, $mdDialog, AccountTypesService, accounts) {
  'use strict';

  $scope.accounts = accounts;
  $scope.accountOptions = accounts;
  // $.grep($scope.accounts, function( a ) {
  //   return (['Professional', 'Enterprise'].indexOf(a.type.title) !== -1);
  // });

  $scope.accountTypes = AccountTypesService.query();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    if(!$scope.data.masterAccount){
      $scope.error = 'Need to Select a Master Account';
      return;
    }
    $mdDialog.hide(answer);
  };
}