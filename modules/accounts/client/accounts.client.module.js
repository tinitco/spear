(function (app) {
  'use strict';

  app.registerModule('accounts', ['core.admin']);
  app.registerModule('accounts.services');
  app.registerModule('accounts.routes', ['ui.router', 'accounts.services']);
})(ApplicationConfiguration);
