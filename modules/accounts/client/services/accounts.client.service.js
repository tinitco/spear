(function () {
  'use strict';

  angular
    .module('accounts.services')
    .factory('AccountsService', AccountsService)
    .factory('AccountMergeService', AccountMergeService);

  AccountMergeService.$inject = ['$resource'];
  AccountsService.$inject = ['$resource'];

  function AccountsService($resource) {
    return $resource('./api/accounts/:accountId', { 
      accountId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
  function AccountMergeService($resource) {
    return $resource('./api/accountmerge/:accountId', { 
      accountId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
})();
