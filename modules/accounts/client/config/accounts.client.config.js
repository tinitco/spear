(function () {
  'use strict';

  angular
    .module('accounts')
    .run(menuConfig);
  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'superAdmin', {
      title: 'Accounts',
      state: 'accounts.list'
    });

    // Menus.addSubMenuItem('topbar', {
    //   title: 'Settings',
    //   state: 'accounts.settings',
    //   roles: ['user', 'admin']
    // });
    // Menus.addMenuItem('topbar', {
    //   title: 'Accounts',
    //   state: 'accounts',
    //   type: 'dropdown',
    //   roles: ['superAdmin']
    // });

    // // Add the dropdown list item
    // Menus.addSubMenuItem('topbar', 'accounts', {
    //   title: 'List accounts',
    //   state: 'accounts.list'
    // });

    // // Add the dropdown create item
    // Menus.addSubMenuItem('topbar', 'accounts', {
    //   title: 'Create Article',
    //   state: 'accounts.create',
    //   roles: ['user']
    // });
  }
})();
