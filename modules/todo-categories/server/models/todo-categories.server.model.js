'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * TodoCategories Schema
 */
var TodoCategoriesSchema = new Schema({
  // TodoCategories model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    trim: true,
    unique: 'Category already exists',
    required: 'Title cannot be blank'
  },
  account: {
    type: Schema.ObjectId,
    ref: 'Account',
    required: 'Account cannot be blank'
  }
});

mongoose.model('TodoCategories', TodoCategoriesSchema);
