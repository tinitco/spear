'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  TodoCategories = mongoose.model('TodoCategories'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a 
 */
exports.create = function (req, res) {
  // console.log(req.body);
  var todoCat = new TodoCategories(req.body);
  todoCat.account = req.user.account;
  // console.log(todoCat);
  // todoCat.user = req.user;
  todoCat.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todoCat);
    }
  });
};

/**
 * Show the current 
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var todoCat = req.todoCat ? req.todoCat.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // todoCat.isCurrentUserOwner = req.user && todoCat.user && todoCat.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(todoCat);
};

/**
 * Update a 
 */
exports.update = function (req, res) {
  var todoCat = req.todoCat;

  todoCat.title = req.body.title;
  todoCat.active = req.body.active;
  todoCat.max_users = req.body.max_users;

  todoCat.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todoCat);
    }
  });

};

/**
 * Delete an 
 */
exports.delete = function (req, res) {
  var todoCat = req.todoCat;

  todoCat.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todoCat);
    }
  });

};

/**
 * List of 
 */
exports.list = function (req, res) {
  TodoCategories.find({'account': req.user.account}).sort('title').populate('account', 'title').exec(function (err, todoCat) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todoCat);
    }
  });

};

/**
 * Article middleware
 */
exports.todoCatByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Todo Category is invalid'
    });
  }

  TodoCategories.findById(id).populate('account', 'title').exec(function (err, todoC) {
    if (err) {
      return next(err);
    } else if (!todoC) {
      return res.status(404).send({
        message: 'No Todo Category with that identifier has been found'
      });
    }
    req.todoCat = todoC;
    next();
  });
};