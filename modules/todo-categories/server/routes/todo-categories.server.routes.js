'use strict';

var todoCat = require('../controllers/todo-categories.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/todoCat')
    .get(todoCat.list)
    .post(todoCat.create);

  // Single article routes
  app.route('/api/todoCat/:todoCatId')
    .get(todoCat.read)
    .put(todoCat.update)
    .delete(todoCat.delete);

  // Finish by binding the article middleware
  app.param('todoCatId', todoCat.todoCatByID);
};
