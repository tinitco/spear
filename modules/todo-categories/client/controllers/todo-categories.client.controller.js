(function () {
  'use strict';

  angular
    .module('todo-categories')
    .controller('TodoCategoriesController', TodoCategoriesController);

  TodoCategoriesController.$inject = ['$scope', '$state', 'Authentication',
  'TodoCategoriesService', '$mdDialog', '$mdMedia'];

  function TodoCategoriesController($scope, $state, Authentication,
  TodoCategoriesService, $mdDialog, $mdMedia) {
    var vm = this;

    // vm.accountTypeSelected = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    $scope.inserted = null;
    // vm.createTodoCat = createTodoCat;
    // vm.toggleStatus = toggleStatus;
    // vm.remove = remove;
    // vm.save = save;

    // list
    $scope.todoCategories = TodoCategoriesService.query();

    $scope.addTodoCategory = function () {
        if($scope.inserted){return;}
        $scope.inserted = new TodoCategoriesService();
        $scope.inserted.name = "";
        // $scope.todoCategories.push($scope.inserted);
        createTodoCat($scope.inserted);
    };

    $scope.updateCategory = function (todoCategory, data) {
        if(todoCategory === $scope.inserted){
          if(todoCategory.title){
            todoCategory.$save({},function(){
              $scope.todoCategories.push(todoCategory);
            });
          }
          $scope.inserted = '';
        }else{
            if(data === ""){
              $scope.deleteCategory(todoCategory);
            }else{
                todoCategory.$update();
            }
        }
    };

    $scope.deleteCategory=function(todoCategory){
        todoCategory.$delete({},function(){
            $scope.todoCategories.splice( $scope.todoCategories.indexOf(todoCategory), 1 );
        });
    };

    // update
    // function toggleStatus(ev, todoCat) {
    //   // Appending dialog to document.body to cover sidenav in docs app
    //   var confirm = $mdDialog.confirm()
    //         .title('Change Todo Category Status?')
    //         .textContent('Are you sure you want to change the Todo Category Status?')
    //         .ariaLabel('Change Status')
    //         .targetEvent(ev)
    //         .ok('Yes')
    //         .cancel('Cancel');
    //   $mdDialog.show(confirm).then(function() {
    //     todoCat.active =!todoCat.active;
    //     todoCat.$update();
    //   }, function() {});
    // }

    $scope.editCategory = function(todoCat){
      createTodoCat(todoCat);
    };
    // Create
    function createTodoCat(todoCategory) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewTodoCategoryDialogController,
        templateUrl: 'modules/todo-categories/client/views/new-todo-category.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        clickOutsideToClose:false,
        fullscreen: useFullScreen,
        locals:{category:todoCategory},
      })
      .then(function(todoCat) {
        // $scope.status = 'You said the information was "' + answer + '".';
        // var ac = new TodoCategoriesService();
        todoCategory.title = todoCat.todoCatName.$viewValue;
        $scope.updateCategory(todoCategory);
      }, function() {
        // cancelled
        $scope.updateCategory(todoCategory);
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    // // delete existing Account Type
    // function remove(ev, todoCat) {
    //   var confirm = $mdDialog.confirm()
    //         .title('Delete Account Type?')
    //         .textContent('Are you sure you want to Delete the Account Type ?')
    //         .ariaLabel('Delete')
    //         .targetEvent(ev)
    //         .ok('Yes')
    //         .cancel('Cancel');
    //   $mdDialog.show(confirm).then(function() {
    //     todoCat.$remove(
    //       vm.todoCategories.splice(vm.todoCategories.indexOf(todoCat), 1)
    //     );
    //   }, function() {});
    // }

    // // Save Account Type
    // function save(isValid) {
    //   if (!isValid) {
    //     $scope.$broadcast('show-errors-check-validity', 'vm.form.articleForm');
    //     return false;
    //   }

    //   // TODO: move create/update logic to service
    //   if (vm.accountTypeSelected._id) {
    //     vm.accountTypeSelected.$update(successCallback, errorCallback);
    //   } else {
    //     vm.accountTypeSelected.$save(successCallback, errorCallback);
    //   }

    //   function successCallback(res) {      }

    //   function errorCallback(res) {
    //     vm.error = res.data.message;
    //   }
    // }
    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
      if ($scope.inserted){
        $(".categories-table td .editable-click").last().click();
      }
    });


  }
})();

function NewTodoCategoryDialogController($scope, $mdDialog, category) {
  'use strict';
  $scope.data = {};
  $scope.data.title = category.title;
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  // $scope.save = function() {
  //   $mdDialog.cancel();
  // };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
