(function () {
  'use strict';

  angular
    .module('todo-categories.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      // .state('home', {
      //   url: '/',
      //   controller: 'SpearController',
      //   controllerAs: 'vm',
      //   templateUrl: 'modules/todo-categories/client/views/todo-categories.client.view.html',
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Spearity :'
      //   }
      // })
      .state('todo-categories', {
        abstract: true,
        url: '/todo-categories',
        template: '<ui-view/>'
      });
      // .state('accounts.list', {
      //   url: '',
      //   templateUrl: 'modules/accounts/client/views/list-accounts.client.view.html',
      //   controller: 'AccountsListController',
      //   controllerAs: 'vm',
      //   data: {
      //     pageTitle: 'Accounts List'
      //   }
      // })
      // .state('accounts', {
      //   // abstract: true,
      //   url: '/accounts',
      //   templateUrl: 'modules/accounts/client/views/accounts.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   data: {
      //     roles: ['superAdmin'],
      //     pageTitle : 'Manage Accounts'
      //   }
      // })
      // .state('accounts.edit', {
      //   url: '/:accountId/edit',
      //   templateUrl: 'modules/accounts/client/views/form-account.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     accountResolve: getAccount
      //   },
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Edit Account {{ accountResolve.title }}'
      //   }
      // })
      // .state('accounts.view', {
      //   url: '/:accountId',
      //   templateUrl: 'modules/accounts/client/views/view-account.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     accountResolve: getAccount
      //   },
      //   data:{
      //     pageTitle: 'Account {{ accountResolve.title }}'
      //   }
      // });
  }

  getSpear.$inject = ['$stateParams', 'TodoCatService'];

  function getSpear($stateParams, TodoCatService) {
    return TodoCatService.get({
      todoCatId: $stateParams.todoCatId
    }).$promise;
  }

  newSpear.$inject = ['TodoCatService'];

  function newSpear(TodoCatService) {
    return new TodoCatService();
  }
})();
