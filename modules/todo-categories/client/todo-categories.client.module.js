(function (app) {
  'use strict';

  app.registerModule('todo-categories');
  app.registerModule('todo-categories.services');
  app.registerModule('todo-categories.routes', ['ui.router', 'todo-categories.services']);
})(ApplicationConfiguration);
