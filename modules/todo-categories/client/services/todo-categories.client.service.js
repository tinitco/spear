(function () {
  'use strict';

  angular
    .module('todo-categories.services')
    .factory('TodoCategoriesService', TodoCategoriesService);

  TodoCategoriesService.$inject = ['$resource'];

  function TodoCategoriesService($resource) {
    return $resource('./api/todoCat/:todoCatId', { 
      todoCatId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
})();
