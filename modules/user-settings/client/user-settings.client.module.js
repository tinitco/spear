(function (app) {
  'use strict';

  app.registerModule('user-settings');
  app.registerModule('user-settings.services');
  // app.registerModule('todo-categories.routes', ['ui.router', 'todo-categories.services']);
})(ApplicationConfiguration);
