(function () {
  'use strict';

  angular
    .module('user-settings.services')
    .factory('UserSettingsService', UserSettingsService);

  UserSettingsService.$inject = ['$resource'];

  function UserSettingsService($resource) {
    return $resource('./api/settings/:settingsId', { 
      settingsId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
})();
