'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * UserCalendars Schema
 */
var UserSettingsSchema = new Schema({
  // UserCalendars model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  hideCompleted:{
    type: Boolean,
    default: false,
  },
  displayLength: {
    type: Number,
    default: 10,
  },
  category: {
    type: String,
    trim: true,
    default: '',
  },
  belongsTo: {
    type: Schema.ObjectId,
    ref: 'User',
    required: 'User cannot be blank'
  }
});

mongoose.model('UserSettings', UserSettingsSchema);
