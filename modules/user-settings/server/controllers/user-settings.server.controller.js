'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  UserSettings = mongoose.model('UserSettings'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


/**
 * Create a 
 */
exports.create = function (req, res) {

};

/**
 * Show the current 
 */
exports.read = function (req, res) {
  var setting = req.setting ? req.setting.toJSON() : {};
  res.json(setting);
};

/**
 * Update a 
 */
exports.update = function (req, res) {
  var setting = req.setting;

  setting.hideCompleted = req.body.hideCompleted;
  setting.displayLength = req.body.displayLength;
  setting.category = req.body.category;

  // setting.max_users = req.body.max_users;

  setting.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(setting);
    }
  });

};

/**
 * Delete an 
 */
// exports.delete = function (req, res) {

// };

/**
 * List of 
 */
exports.list = function (req, res) {
  UserSettings.find({'belongsTo': req.user}).populate('').exec(function (err, settings) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      if (!settings.length){
        var set = new UserSettings(req.body);
        set.belongsTo = req.user;
        set.save(function (err) {
          if (err) {
            console.log(errorHandler.getErrorMessage(err));
            // return null;
          } else {
            res.json([set]);
          }
      });
      }else{
        res.json(settings);
      }
    }
  });
};


exports.settingsByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Spear is invalid'
    });
  }

  UserSettings.findById(id).populate('').exec(function (err, setting) {
    if (err) {
      return next(err);
    } else if (!setting) {
      return res.status(404).send({
        message: 'No setting with that identifier has been found'
      });
    }
    req.setting = setting;
    next();
  });
};