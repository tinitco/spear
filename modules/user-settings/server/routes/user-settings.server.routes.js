'use strict';

var settings = require('../controllers/user-settings.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/settings')
    .get(settings.list);
    // .post(settings.create);

  // Single article routes
  app.route('/api/settings/:settingsId')
    // .get(settings.read)
    .put(settings.update);
    // .delete(settings.delete);

  // Finish by binding the article middleware
  app.param('settingsId', settings.settingsByID);
};
