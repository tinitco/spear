'use strict';

var goals = require('../controllers/quarter-goals.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/goals')
    .get(goals.list)
    .post(goals.create);

  // Single article routes
  app.route('/api/goals/:goalId')
    .get(goals.read)
    .put(goals.update)
    .delete(goals.delete);

  app.route('/api/goals/:goalId/copy')
    .put(goals.copy);

  // Finish by binding the article middleware
  app.param('goalId', goals.goalByID);
};
