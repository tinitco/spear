'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * QuarterGoals Schema
 */
var QuarterGoalsSchema = new Schema({
  // QuarterGoals model fields
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    trim: true,
    // required: 'Title cannot be blank'
  },
  active: {
    type: Boolean,
    default: true,
    // required: 'Title cannot be blank'
  },
  progress: {
    type: Number,
    default: 0
    // required: 'Set Max Number of Users'
  },
  precedence: {
    type: Number,
    default: 0
  },
  spear: {
    type: Schema.ObjectId,
    ref: 'Spear',
    required: 'You\'ll need to choose a Spear'
  },
  belongsTo: {
    type: Schema.ObjectId,
    ref: 'User',
    required: 'You\'ll need to choose a User'
  }
});

mongoose.model('QuarterGoals', QuarterGoalsSchema);
