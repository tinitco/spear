'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  QGoals = mongoose.model('QuarterGoals'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  moment = require('moment'),
  Todo = mongoose.model('Todo'),
  User = mongoose.model('User'),
  async = require('async'),
  Spear = mongoose.model('Spear');

/**
 * Create a
 */
exports.create = function (req, res) {
  // console.log(req.body);
  var goal = new QGoals(req.body);
  // console.log(goal);
  goal.belongsTo = req.user;
  // goal.assignedTo = req.user;
  goal.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(goal);
    }
  });
};

/**
 * Show the current
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var goal = req.goal ? req.goal.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // goal.isCurrentUserOwner = req.user && goal.user && goal.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(goal);
};

/**
 * Update a
 */
exports.update = function (req, res) {
  var goal = req.goal;
  console.log(req.body.active);
  goal.title = req.body.title;
  goal.active = req.body.active;
  goal.due_by = req.body.due_by;
  goal.category = req.body.category;
  goal.assignedTo = req.body.assignedTo;
  goal.progress = req.body.progress;
  goal.precedence = req.body.precedence;
  if(!goal.belongsTo){goal.belongsTo = req.user;}

  goal.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(goal);
    }
  });

};

exports.copy = function (req, res) {
  var goal = req.goal;
  if(req.query && req.query.spear){
    // console.log(req.query.copy_to);
    var queryDate = moment(req.query.copy_to).toISOString();
    var spear = req.query.spear;
  // Spear.findOne({
  //   'spear_type':'quarter',
  //   'begin': { '$lte': queryDate },
  //   'end': { '$gte': queryDate },
  //   'belongs_to': req.user
  // }).populate('type', 'title').exec(function (err, spear) {
  //   if (err) {
  //     return res.status(400).send({
  //       message: errorHandler.getErrorMessage(err)
  //     });
  //   } else {
      var goal_copy = new QGoals();
      // console.log(goal);
      goal_copy.title = goal.title;
      goal_copy.spear = spear;
      goal_copy.belongsTo = req.user;
      goal_copy.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          Todo.find({
            'quarterGoal': goal,
            'belongsTo': req.user
          }).exec(function (err, todos) {
            if (err) {
              return res.status(400).send({message: errorHandler.getErrorMessage(err)});
            }else {
              async.forEachOf(todos, function (todo, key, callback) {
                var td = new Todo();
                td.title = todo.title;
                td.belongsTo = todo.belongsTo;
                td.quarterGoal = goal_copy;
                td.save(function(err){
                  // console.log('saved a todo');
                  if (err) {
                    callback(err, td);
                  } else { callback(null, td);}
                });
              },function (err, results) {
                if (err){
                  return res.status(400).send({message: errorHandler.getErrorMessage(err)});
                }else{
                  res.json(goal_copy);
                }
              });
            }
          });
        }
      });
    // }
  // });
  }
};

/**
 * Delete an
 */
exports.delete = function (req, res) {
  var goal = req.goal;
  if(goal && req.user){
    Todo.find({'quarterGoal':goal, 'belongsTo': req.user}).remove().exec(function(err, todos){
      if (err) {
        return res.status(400).send({message: errorHandler.getErrorMessage(err)});
      }else{
        goal.remove(function (err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          }else{
            res.json(goal);
          }
        });
        // res.json(goal);
      }
    });
  }
};

/**
 * List of
 */
exports.list = function (req, res) {
  if (req.query.spear){
    // var queryDate = moment(req.query.date).toISOString();
    QGoals.find({
      'spear':req.query.spear,
      // 'belongsTo': req.user
    }).sort('precedence').populate('type', 'title').exec(function (err, goal) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else {
        res.json(goal);
      }
    });

  }else if (req.query.managerMetrics) {
  if(req.user.roles.indexOf('manager') !== -1){
    User.find({manager : req.user}).exec(function (err, users) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else{
        // console.log(users.length);
        var userData = [];
        async.forEachOf(users, function (user, key, callback) {
          var md = moment();
          // console.log('at metrics');
          Spear.find({
            'spear_type':'quarter',
            'begin': { '$gte': md.startOf('quarter').toISOString() },
            'end': { '$lte':  md.endOf('quarter').toISOString()},
            'belongs_to': user
          }).populate('').exec(function (err, spear) {
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callback(err);
            } else {
              var spearIds = spear.map(function(sp){
                return sp._id;
              });
              QGoals.find({
                'spear':{$in: spearIds},
                'belongsTo': user
              }).populate('').exec(function (err, goals) {
                if (err) {
                  console.log(errorHandler.getErrorMessage(err));
                  callback(err);
                }else {
                  userData.push({user: user, data: goals});
                  callback(null,goals);
                }
              });
            }
          });
        },function (err, results) {
          if (err){
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          }else{
            // console.log(userData);
            res.json(userData);
          }
          // configs is now a map of JSON data
          // doSomethingWith(configs);
          // callbackfn(null, retVal);
        });
      }
    });
  }
  }else{
    QGoals.find({ 'belongsTo': req.user }).sort('-created').
    populate('').exec(function (err, goal) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(goal);
      }
    });
  }
};


/**
 * Article middleware
 */
exports.goalByID = function (req, res, next, id) {
  // console.log(id);
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'QGoals is invalid'
    });
  }
  QGoals.findById(id).populate('belongsTo', 'displayName').exec(function (err, goalC) {
    if (err) {
      return next(err);
    } else if (!goalC) {
      return res.status(404).send({
        message: 'No Todo with that identifier has been found'
      });
    }
    req.goal = goalC;
    next();
  });
};
