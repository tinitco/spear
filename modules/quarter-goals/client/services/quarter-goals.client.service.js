(function () {
  'use strict';

  angular
    .module('quarter-goals.services')
    .factory('QuarterGoalsService', QuarterGoalsService);

  QuarterGoalsService.$inject = ['$resource'];

  function QuarterGoalsService($resource) {
    return $resource('./api/goals/:goalId/:action', { 
      goalId: '@_id',
      action: '@action'
    },{
      update: { method: 'PUT' },
      copy:{method: 'PUT', params:{action: 'copy'}}
    });
  }
})();
