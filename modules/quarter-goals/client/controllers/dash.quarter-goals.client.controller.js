(function () {
  'use strict';

  angular
    .module('quarter-goals')
    .controller('DashQuarterGoalsController', DashQuarterGoalsController);

  DashQuarterGoalsController.$inject = ['$scope', '$attrs', '$state', 'Authentication',
   'QuarterGoalsService', 'SpearService', '$mdDialog', '$mdMedia', 'moment'];

  function DashQuarterGoalsController($scope, $attrs, $state, Authentication, QuarterGoalsService, SpearService, 
    $mdDialog, $mdMedia, moment) {
    $scope.spear = null;
    $scope.goals = null;
    $scope.type = 'quarter';
    $scope.date = moment().format("YYYY-MM-DD");

    $scope.pieOptions = {
      // donut: true,
      // donutWidth: 3,
      width: 30,
      height: 30,
      showLabel: false
    };

    $scope.chartdata = {
      series:[10, 90]
    };

    var ret = SpearService.query({ type:$scope.type, date:$scope.date}, function(){
        $scope.spear = ret[0];
        $scope.goals = QuarterGoalsService.query({ spear: $scope.spear._id }, function(){
          // $scope.spear = data.spear_id;
          $scope.inserted = null;
          $scope.goals.forEach(function(goal){
            goal.chartdata = {series:[goal.progress, 100 - goal.progress]};
          });
        });
    });

    $scope.toggleCheck = function(goal) {
        $scope.loading = true;
        goal.active=!goal.active;
        $scope.updateGoal(goal);
    };

    $scope.updateGoal = function(goal, data){
        if(goal === $scope.inserted){
            goal.$save({},function(){
                $scope.goals.push(goal);
                $scope.inserted = '';
            });
        }else{
            if(data === ""){
                $scope.deleteGoal(goal);
            }else{
                goal.$update();
            }
        }
    };

  }
})();
