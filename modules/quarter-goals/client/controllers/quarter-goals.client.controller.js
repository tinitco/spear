(function () {
  'use strict';

  angular
    .module('quarter-goals')
    .controller('QuarterGoalsController', QuarterGoalsController)
    .config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('M/D');
      };
    });

  QuarterGoalsController.$inject = ['$scope', '$attrs', '$state', '$mdToast', 'Authentication',
   'QuarterGoalsService', '$mdDialog', '$mdMedia', 'moment', 'Notification'];

  function QuarterGoalsController($scope, $attrs, $state, $mdToast, Authentication, QuarterGoalsService,
    $mdDialog, $mdMedia, moment, Notification) {

    $scope.relative = '';
    if($attrs.prev){$scope.relative = 'prev';}

    // $scope.progressVals = [0,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100];
    $scope.progressVals = [];

    for (var i = 0; i <105 ; i= i+5) {
      $scope.progressVals.push(i);
    }

    $scope.loading = true;
    $scope.spearUp = false;
    if($attrs.spearup && $attrs.spearup === "true"){ $scope.spearUp= true; }

    $scope.$on('spearChanged', function (event,data) {
      $scope.class = data.type;
      $scope.spear = data.spear_id;
      $scope.spear_nxt = data.nextSpearId;
      var spearId = data.spear_id;
      if ($scope.relative === 'prev'){
        spearId = data.prevSpearId;
        $scope.spear_nxt = null;
      }else if ($scope.spearUp){
        spearId = data.spear_up;
      }
      if(spearId){
        $scope.spear = spearId;
        $scope.loadGoals();
      }
    });

    $scope.loadGoals = function(){
      $scope.goals = QuarterGoalsService.query({ spear:$scope.spear }, function(){
        // $scope.spear = data.spear_id;
        $scope.inserted = null;
        $scope.goals.forEach(function(goal){
          $scope.$watch(
            function() {
                return goal.progress;
            },
            function(newValue, oldValue) {
              if(newValue === oldValue){return;
                // $mdToast.show($mdToast.simple().content("Slider=" + goal.progress).position("top right").hideDelay(1500));
              }
              goal.$update();
            });
        });
      });
    };

    // $scope.goals = QuarterGoalsService.query({},function(){
    //     $scope.quarter= SpearService.getSpear();
    // });

    $scope.toggleCheck = function(goal) {
        $scope.loading = true;
        goal.status=!goal.status;
        $scope.updateGoal(goal);
    };

    $scope.addGoal= function(data){
        if($scope.goals && $scope.goals.length>=4){
          $mdDialog.show(
          $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Too Many Goals')
            .textContent('Sorry.. you cannot have more than Four Goals in a Quarter.')
            .ariaLabel('Too Many Goals')
            .ok('Got it!')
          );
        }

        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
        $mdDialog.show({
          controller: NewGoalDialogController,
          templateUrl: 'modules/quarter-goals/client/views/new-goal.client.template.html',
          parent: angular.element(document.body),
          // targetEvent: ev,
          clickOutsideToClose:false,
          locals:{
            // goal: goal,
            type: $scope.class
          },
          fullscreen: true
        })
        .then(function(goal) {
          $scope.inserted = new QuarterGoalsService();
          $scope.inserted.spear = $scope.spear;
          $scope.inserted.title = goal.goalTitle.$viewValue;
          $scope.updateGoal($scope.inserted);
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
          $scope.customFullscreen = (wantsFullScreen === true);
        });

    };

    $scope.editGoal = function(goal){
      $mdDialog.show({
        controller: EditGoalDialogController,
        templateUrl: 'modules/quarter-goals/client/views/edit-goal.client.template.html',
        parent: angular.element(document.body),
        locals:{
          goal: goal,
          type: $scope.class
        },
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(todo_data) {
        $scope.updateGoal(goal);
        if(todo_data.reload){$scope.loadGoals();}
      }, function(){
        $scope.updateGoal(goal);
      });
    };

    $scope.updateGoal = function(goal, data){
        if(goal === $scope.inserted){
            goal.$save({},function(){
                $scope.goals.push(goal);
                $scope.inserted = '';
            });
        }else{
            if(data === ""){
                $scope.deleteGoal(goal);
            }else{
                goal.$update();
            }
        }
    };

    $scope.deleteGoal= function (goal) {
        // ask for conformation first...
      var confirm = $mdDialog.confirm()
          .title('Delete Quarter Goal?')
          .textContent('Are you sure you want to delete the Goal? All goal Tasks will be lost?')
          .ariaLabel('Delete?')
          // .targetEvent(ev)
          .ok('Yes')
          .cancel('Cancel');
    $mdDialog.show(confirm).then(function() {
      // goal.status =  goal.status ==='started'?'finished':'started';
        goal.$delete({},function(){
            $scope.goals.splice( $scope.goals.indexOf(goal), 1 );
            Notification.success('Goal and associated tasks removed');
            // also remove the plans in the goal.
        });
    }, function() {});
    };

    $scope.dropCallback = function(event,index,goal){
      var goals = $scope.goals;

      var tdData = $.grep(goals, function(e){ return e._id === goal._id; })[0];
      goals.splice(goals.indexOf(tdData), 1);
      goals.splice(index, 0, tdData);

      for (var i = 0; i < goals.length; i++) {
        if (i === goals[i].precedence){continue;}
        goals[i].precedence = i;
        $scope.updateGoal(goals[i]);
      }
    };
  }
})();

function NewGoalDialogController($scope, $mdDialog, TodoCategoriesService, type) {
  'use strict';
  $scope.type = type;
  $scope.todoCategories = TodoCategoriesService.query();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}

function EditGoalDialogController($scope, $mdDialog, moment, goal, type,
  UserCalendarService, TodoFunctions, Notification, SpearService, Authentication) {
  'use strict';
  $scope.goal = goal;
  $scope.type = type;
  $scope.data = {};
  $scope.data.title = goal.title;
  $scope.data.date = moment().format("ddd, MM/DD/YYYY");
  $scope.calendars = UserCalendarService.query();
  $scope.data.options = ['Quarter'];
  $scope.data.selected = 'Quarter';

$scope.minDate = new Date(); // for the copy to function

  $scope.options = {
    step: 30,
    // timeFormat: 'h:i A',
    // scrollDefault: 'now'
  };

  // $scope.notAPlan = function(todo){
  //   if(todo.spear)
  // }

  $scope.inlineOptions = {
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  };

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.format = 'EEE, M/d/yyyy';

  $scope.popup1 = {
    opened: false
  };

  $scope.markDone = function(){
    goal.active = !goal.active;
  };

  $scope.dateChanged = function(){
    var d = new Date($scope.data.date);
    d.setHours($scope.data.start.getHours());
    d.setMinutes($scope.data.start.getMinutes());
    $scope.data.start = d;

    var e = new Date($scope.data.date);
    e.setHours($scope.data.end.getHours());
    e.setMinutes($scope.data.end.getMinutes());
    $scope.data.end = e;
  };
  $scope.startChanged = function(){
    var d = new Date($scope.data.end);
    d.setTime($scope.data.start.getTime()+3600000);
    $scope.data.end = d;
    // console.log($scope.data.start);
  };

  $scope.calendars = UserCalendarService.query();
  // $scope.data = {};
  $scope.data.date = moment().format("ddd, MM/DD/YYYY");
  var d = new Date();
    d.setHours( 12 );
    d.setMinutes( 0 );
  $scope.data.start = d;
  $scope.startChanged();


  $scope.copyPlan = function(selected){
    if (!selected){selected = $scope.data.selected;}
    // TodoFunctions.copyPlan(todo, selected, $scope.data.due_by);
    // TodoFunctions.copyPlan(todo,)
    // $scope.goal.copy_to = $scope.data.date;
    var spear = SpearService.query({type: 'quarter', date:moment($scope.data.date).toDate()}, function(){
      $scope.goal.$copy({spear: spear[0]._id},function(){
        Notification.success('Copied Goal and Plans.');
        $mdDialog.hide({reload: true});
      }, function(){
        Notification.error('Failed to copy Goal');
      });
    });
  };

  $scope.deletePlan = function(){
    // var confirm = $mdDialog.confirm()
    //         .title('Delete Goal ?')
    //         .textContent('Are you sure you want to Delete the Goal ?')
    //         .ariaLabel('Delete')
    //         // .targetEvent(ev)
    //         .ok('Yes')
    //         .cancel('Cancel');
    //   $mdDialog.show(confirm).then(function() {
        goal.$delete({},function(){
      // $mdDialog.hide({reload: true});
          $scope.save({reload: true});
        });
      // }, function() {});

  };

  $scope.addToReport = function(period){
    $scope.reportPeriod = period;
  };

  $scope.saveToReport = function(){
    if($scope.reportPeriod === 'month'){
      $scope.todo.reportInmonth = true;
    }
  };

  $scope.addToCalender = function(todo){
    if(!$scope.data.calendar){
      $scope.errors = "You have to choose a Calender..";
      return;
    }
    TodoFunctions.addToCalender({title: $scope.data.title, description: $scope.data.calDescription},
      $scope.data.calendar.calendarId, $scope.data.start, $scope.data.end);
  };

$scope.goalText = angular.copy($scope.goal.title);
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $scope.goal.title = $scope.goalText;
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
