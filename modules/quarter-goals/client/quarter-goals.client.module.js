(function (app) {
  'use strict';

  app.registerModule('quarter-goals');
  app.registerModule('quarter-goals.services');
  app.registerModule('quarter-goals.routes', ['ui.router', 'quarter-goals.services']);
})(ApplicationConfiguration);
