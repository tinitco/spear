'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  QuarterGoals = mongoose.model('QuarterGoals');

/**
 * Globals
 */
var user, quarterGoals;

/**
 * Unit tests
 */
describe('Quarter goals Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() { 
      quarterGoals = new QuarterGoals({
        // Add model fields
        // ...
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      return quarterGoals.save(function(err) {
        should.not.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) { 
    QuarterGoals.remove().exec();
    User.remove().exec();

    done();
  });
});
