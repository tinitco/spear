(function (app) {
  'use strict';

  app.registerModule('accountTypes', ['core.admin']);
  app.registerModule('accountTypes.services');
  app.registerModule('accountTypes.routes', ['ui.router', 'accountTypes.services', 
  	'core.admin.routes']);
})(ApplicationConfiguration);

// ApplicationConfiguration.registerModule('users.admin', );