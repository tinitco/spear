(function () {
  'use strict';

  angular
    .module('accountTypes')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {

    Menus.addSubMenuItem('topbar', 'superAdmin', {
      title: 'Account Types',
      state: 'accountTypes'
    });


    // Menus.addMenuItem('topbar', {
    //   title: 'Account Types',
    //   state: 'accountTypes',
    //   type: 'dropdown',
    //   roles: ['superAdmin']
    // });

    // Add the dropdown list item
    // Menus.addSubMenuItem('topbar', 'accountTypes', {
    //   title: 'List Account Types',
    //   state: 'accountTypes.list'
    // });

    // // Add the dropdown create item
    // Menus.addSubMenuItem('topbar', 'accountTypes', {
    //   title: 'Create Article',
    //   state: 'accountTypes.create',
    //   roles: ['user']
    // });
  }
})();
