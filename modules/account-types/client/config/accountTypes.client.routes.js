(function () {
  'use strict';

  angular
    .module('accountTypes.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('accountTypes', {
        // abstract: true,
        url: '/accountTypes',
        templateUrl: 'modules/account-types/client/views/account-types.client.view.html',
        controller: 'AccountTypesController',
        controllerAs: 'vm',
        data: {
          roles: ['superAdmin'],
          pageTitle: 'Manage Account Types'
        }
      });
      // .state('accountTypes.list', {
      //   url: '',
      //   templateUrl: 'modules/articles/client/views/list-articles.client.view.html',
      //   controller: 'ArticlesListController',
      //   controllerAs: 'vm',
      //   data: {
      //     pageTitle: 'Articles List'
      //   }
      // })
      // .state('accountTypes.create', {
      //   url: '/create',
      //   templateUrl: 'modules/articles/client/views/form-article.client.view.html',
      //   controller: 'ArticlesController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     articleResolve: newArticle
      //   },
      //   data: {
      //     roles: ['superAdmin'],
      //     pageTitle : 'Articles Create'
      //   }
      // })
      // .state('articles.edit', {
      //   url: '/:articleId/edit',
      //   templateUrl: 'modules/articles/client/views/form-article.client.view.html',
      //   controller: 'ArticlesController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     articleResolve: getArticle
      //   },
      //   data: {
      //     roles: ['superAdmin'],
      //     pageTitle: 'Edit Account Type {{ articleResolve.title }}'
      //   }
      // })
      // .state('articles.view', {
      //   url: '/:articleId',
      //   templateUrl: 'modules/articles/client/views/view-article.client.view.html',
      //   controller: 'ArticlesController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     articleResolve: getArticle
      //   },
      //   data:{
      //     pageTitle: 'Article {{ articleResolve.title }}'
      //   }
      // });
  }

  getAccountTypes.$inject = ['$stateParams', 'AccountTypesService'];

  function getAccountTypes($stateParams, AccountTypesService) {
    return AccountTypesService.get({
      Id: $stateParams.Id
    }).$promise;
  }

  newAccountTypes.$inject = ['AccountTypesService'];

  function newAccountTypes(AccountTypesService) {
    return new AccountTypesService();
  }
})();
