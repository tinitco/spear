(function () {
  'use strict';

  angular
    .module('accountTypes')
    .controller('AccountTypesController', AccountTypesController);

  AccountTypesController.$inject = ['$scope', '$state', 'Authentication', 
  'AccountTypesService', '$mdDialog', '$mdMedia'];

  function AccountTypesController($scope, $state, Authentication,
  AccountTypesService, $mdDialog, $mdMedia) {
    var vm = this;

    vm.accountTypeSelected = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.createAccountType = createAccountType;
    vm.toggleStatus = toggleStatus;
    vm.remove = remove;
    // vm.save = save;

    // list 
    vm.accountTypes = AccountTypesService.query();

    // update
    function toggleStatus(ev, accountType) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
            .title('Change Account Type Status?')
            .textContent('Are you sure you want to change the Account Type Status?')
            .ariaLabel('Change Status')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        accountType.active =!accountType.active;
        accountType.$update();
      }, function() {});
    }

    // Create 
    function createAccountType(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewAccountTypeDialogController,
        templateUrl: 'modules/account-types/client/views/new-account-type.client.template.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(accountType) {
        // $scope.status = 'You said the information was "' + answer + '".';
        var ac = new AccountTypesService();
        ac.title = accountType.accountTypeName.$viewValue;
        ac.max_users = accountType.accountTypeUsers.$viewValue;
        ac.billing_cycle = accountType.accountBillingCycle.$viewValue;
        ac.$save({}, function () {
          vm.accountTypes.push(ac);
        });
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    // delete existing Account Type
    function remove(ev, accountType) {
      var confirm = $mdDialog.confirm()
            .title('Delete Account Type?')
            .textContent('Are you sure you want to Delete the Account Type ?')
            .ariaLabel('Delete')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        accountType.$remove(
          vm.accountTypes.splice(vm.accountTypes.indexOf(accountType), 1)
        );
      }, function() {});
    }

    // // Save Account Type
    // function save(isValid) {
    //   if (!isValid) {
    //     $scope.$broadcast('show-errors-check-validity', 'vm.form.articleForm');
    //     return false;
    //   }

    //   // TODO: move create/update logic to service
    //   if (vm.accountTypeSelected._id) {
    //     vm.accountTypeSelected.$update(successCallback, errorCallback);
    //   } else {
    //     vm.accountTypeSelected.$save(successCallback, errorCallback);
    //   }

    //   function successCallback(res) {      }

    //   function errorCallback(res) {
    //     vm.error = res.data.message;
    //   }
    // }



  }
})();

function NewAccountTypeDialogController($scope, $mdDialog) {
  'use strict';
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  // $scope.save = function() {
  //   $mdDialog.cancel();
  // };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}