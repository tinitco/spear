(function () {
  'use strict';

  angular
    .module('accountTypes.services')
    .factory('AccountTypesService', AccountTypesService);

  AccountTypesService.$inject = ['$resource'];

  function AccountTypesService($resource) {
    return $resource('./api/accountTypes/:accountTypeId', { 
      accountTypeId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }
})();
