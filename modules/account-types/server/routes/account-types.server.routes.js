'use strict';

var accountTypes = require('../controllers/account-types.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/accountTypes')
    .get(accountTypes.list)
	.post(accountTypes.create);

  // Single article routes
  app.route('/api/accountTypes/:accountTypeId')
    .get(accountTypes.read)
    .put(accountTypes.update)
    .delete(accountTypes.delete);

  // Finish by binding the article middleware
  app.param('accountTypeId', accountTypes.accountTypeByID);
};
