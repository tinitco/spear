'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * AccountTypes Schema
 */
var AccountTypesSchema = new Schema({
  // AccountTypes model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  },
  title: {
    type: String,
    default: 'hello',
    trim: true,
    required: 'Title cannot be blank'
  },
  max_users: {
    type: Number,
    required: 'Set Max Number of Users'
  },
  billing_cycle: {
    type: Number,
    default: 1,
    // required: 'Set Number of months for Billing Cycle'
  }
});

mongoose.model('AccountTypes', AccountTypesSchema);
