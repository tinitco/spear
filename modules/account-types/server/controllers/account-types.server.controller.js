'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  AccountTypes = mongoose.model('AccountTypes'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a 
 */
exports.create = function (req, res) {
  // console.log(req.body);
  var accountTypes = new AccountTypes(req.body);
  // console.log(accountTypes);
  // accountTypes.user = req.user;
  accountTypes.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(accountTypes);
    }
  });
};

/**
 * Show the current 
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var accountTypes = req.accountTypes ? req.accountTypes.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // accountTypes.isCurrentUserOwner = req.user && accountTypes.user && accountTypes.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(accountTypes);
};

/**
 * Update a 
 */
exports.update = function (req, res) {
  var accountTypes = req.accountTypes;

  accountTypes.title = req.body.title;
  accountTypes.active = req.body.active;
  accountTypes.max_users = req.body.max_users;

  accountTypes.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(accountTypes);
    }
  });

};

/**
 * Delete an 
 */
exports.delete = function (req, res) {
  var accountTypes = req.accountTypes;

  accountTypes.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(accountTypes);
    }
  });
};

/**
 * List of 
 */
exports.list = function (req, res) {
  AccountTypes.find().sort('-created').populate('user', 'displayName').exec(function (err, accountTypes) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(accountTypes);
    }
  });
};



/**
 * Article middleware
 */
exports.accountTypeByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Account Type is invalid'
    });
  }

  AccountTypes.findById(id).populate('user', 'displayName').exec(function (err, accountType) {
    if (err) {
      return next(err);
    } else if (!accountType) {
      return res.status(404).send({
        message: 'No Account Type with that identifier has been found'
      });
    }
    req.accountTypes = accountType;
    next();
  });
};