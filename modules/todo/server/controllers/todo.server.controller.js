'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

var path = require('path'),
  mongoose = require('mongoose'),
  moment = require('moment'),
  Todo = mongoose.model('Todo'),
  User = mongoose.model('User'),
  Spear = mongoose.model('Spear'),
  QGoals = mongoose.model('QuarterGoals'),
  async = require('async'),
  _ = require('underscore'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
var anaRel = require(path.resolve('./modules/analysis-realigns/server/controllers/analysis-realigns.server.controller'));

function getMetricsFor(type, user, callback){
  var md = moment();
  // console.log('at metrics');
  Spear.find({
    'spear_type':type,
    'begin': { '$gte': md.startOf('quarter').toISOString() },
    'end': { '$lte':  md.endOf('quarter').toISOString()},
    'belongs_to': user
  }).populate('').exec(function (err, spear) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
    } else {
      var spearIds = spear.map(function(sp){
        return sp._id;
      });
      // console.log(spearIds);
      Todo.count({
        'spear':{$in: spearIds},
        'belongsTo': user
      }).populate('').exec(function (err, todocount) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(null);
        }else {
          callback(todocount);
        }
      });
    }
  });
}

function getOpenTodos(past, delegated, user, callback){
  var cond = {
    'assignedTo': user,
    'belongsTo': user,
    // 'due_by': {'$gte':moment().toISOString()},
    'active':true
  };
  cond.due_by = past? {'$lte':moment().toISOString()} : {'$gte':moment().toISOString()};
  if (delegated){
    // cond.assignedTo = user;
    cond.belongsTo = {$ne: user};
  }

  Todo.count(cond).populate('').exec(function (err, todoCount) {
    if (err) {
      console.log(errorHandler.getErrorMessage(err));
      callback(null);
    } else {
      callback(todoCount);
    }
  });
}


/**
 * Create a
 */
exports.create = function (req, res) {
  // console.log(req.body);
  var todo = new Todo(req.body);
  // console.log(todo);
  todo.belongsTo = req.user;
  todo.assignedTo = req.user;
  todo.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todo);
    }
  });
};

/**
 * Show the current
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var todo = req.todo ? req.todo.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  // todo.isCurrentUserOwner = req.user && todo.user && todo.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(todo);
};

/**
 * Update a
 */
exports.update = function (req, res) {
  var todo = req.todo;
  // console.log(req.body.active);
  todo.title = req.body.title;
  todo.active = req.body.active;

  todo.priority = req.body.priority;

  todo.due_by = req.body.due_by;
  todo.delegate_reminder = req.body.delegate_reminder;
  todo.reminder = req.body.reminder;

  todo.status = req.body.status;
  todo.spear = req.body.spear;
  
  todo.category = req.body.category;
  todo.reportInmonth = req.body.reportInmonth;

  todo.assignedTo = req.body.assignedTo;
  todo.precedence = req.body.precedence;

  todo.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      // console.log(todo.category);
      res.json(todo);
    }
  });

};

exports.updateReportIn = function(req, res){
  var todo = req.todo;
  // console.log(req.todo);
  todo.reportInmonth = req.body.reportInmonth;
  todo.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      // console.log(todo.category);
      res.json(todo);
    }
  });
};

/**
 * Delete an
 */
exports.delete = function (req, res) {
  var todo = req.todo;

  todo.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(todo);
    }
  });

};

/**
 * List of
 */
exports.list = function (req, res) {
  if (req.query.spear){
    // var queryDate = moment(req.query.date).toISOString();
    Todo.find({
      'spear':req.query.spear,
      // 'belongsTo': req.user
    }).sort('precedence').populate('type', 'title').exec(function (err, todo) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else {
        res.json(todo);
      }
    });

  }else if (req.query.goal) {
    Todo.find({
      'quarterGoal':req.query.goal,
      // 'belongsTo': req.user
    }).sort('precedence').populate('type', 'title').exec(function (err, todo) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else {
        res.json(todo);
      }
    });
  }else if (req.query.metrics) {

    async.parallel([
      function(callback){
        getMetricsFor('week', req.user, function(res){
          if(res === null){callback('cannot find metircs');}
          callback(null, {'weekCount':res});
      });},
      function(callback){
        getMetricsFor('day', req.user, function(res){
          callback(null, {'dayCount': res});
      });},
      function(callback){
        getOpenTodos(true, false, req.user, function(res){
          callback(null, {'duetodoCount': res});
      });},
      function(callback){
        getOpenTodos(false, false, req.user, function(res){
          callback(null, {'pasttodoCount': res});
      });},
      function(callback){
        getOpenTodos(true, true, req.user, function(res){
          callback(null, {'delegatedduetodoCount': res});
      });},
      function(callback){
        getOpenTodos(false, true, req.user, function(res){
          callback(null, {'delegatedpasttodoCount': res});
      });},
      ],
      function(err, results){
        // console.log(results);
        var resArray = {};
        results.forEach(function(element){
          resArray = _.extend(resArray, element);
        });
        res.json([resArray]);
    });
  }else if (req.query.managerMetrics) {
  if(req.user.roles.indexOf('manager') !== -1){
    User.find({manager : req.user}).exec(function (err, users) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else{
        // console.log(users.length);
        var userData = [];
        async.forEachOf(users, function (user, key, callback) {
          getMetricsForUser(user,function(data){
            // console.log(data);
            if(data === null){callback("err: cannot find metircs");}
            else{
              data.user = user;
              userData.push(data);
              callback(null, "");
            }
          });
        },function (err, results) {
          if (err){
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          }else{
            // console.log(userData);
            res.json(userData);
          }
          // configs is now a map of JSON data
          // doSomethingWith(configs);
          // callbackfn(null, retVal);
        });
      }
    });
  }
  }else{
    Todo.find({$and:[
      {$or:[{'assignedTo': req.user}, {'belongsTo': req.user}]},
      { 'spear': null}, {'quarterGoal':null }
      ]}
      ).sort('-created').
    populate('type', 'title').exec(function (err, todo) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(todo);
      }
    });
  }
};

function getMetricsForUser(user, callbackfn){
  var data = {day:{}, week:{}, quarter:{goal1:0, goal2:0, goal3:0, goal4:0 }, todo:{}};
  async.parallel([
    function(callback){
      getMetricsFor('day', user,function(dayMetrics){
        if (dayMetrics === null){
          callback('cannot find day metircs');
        }else{
          data.day.plan = dayMetrics;
          callback(null, dayMetrics);
        }
      });
    },
    function(callback){
      getMetricsFor('week', user,function(weekMetrics){
        if (weekMetrics === null){
            callback('cannot find week metircs');
        }else{
          data.week.plan = weekMetrics;
          callback(null, weekMetrics);
        }
      });
    },
    function(callback){
      anaRel.getMetricsFor('week', user,function(weekAnaRelMetrics){
        if (weekAnaRelMetrics === null){
            callback('cannot find week anaRel metircs');
        }else{
          data.week = Object.assign(data.week, weekAnaRelMetrics);
          callback(null, weekAnaRelMetrics);
        }
      });
    },
    function(callback){
      anaRel.getMetricsFor('day', user,function(dayAnaRelMetrics){
        if (dayAnaRelMetrics === null){
            callback('cannot find day anaRel metircs');
        }else{
          data.day = Object.assign(data.day, dayAnaRelMetrics);
          callback(null, dayAnaRelMetrics);
        }
      });
    },
    function(callback){
      getOpenTodos(false, false, user,function(openTodos){
        if (openTodos === null){
          callback('cannot find open metircs');
        }else{
          data.todo.open = openTodos;
          callback(null, openTodos);
        }
      });
    },
    function(callback){
      getOpenTodos(true, false, user,function(pastTodos){
        if (pastTodos === null){
          callback('cannot find past metircs');
        }else{
          data.todo.past = pastTodos;
          callback(null, pastTodos);
        }
      });
    },
    function(callback){
      var md = moment();
      Spear.find({
        'spear_type':'quarter',
        'begin': { '$gte': md.startOf('quarter').toISOString() },
        'end': { '$lte':  md.endOf('quarter').toISOString()},
        'belongs_to': user
      }).populate('').exec(function (err, spear) {
        if (err) {
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        } else {
          var spearIds = spear.map(function(sp){
            return sp._id;
          });
          QGoals.find({
            'spear':{$in: spearIds},
            'belongsTo': user
          }).sort('created').populate('').exec(function (err, goals) {
            if (err) {
              console.log(errorHandler.getErrorMessage(err));
              callback(err);
            }else {
              var idx = "";
              goals.forEach(function(goal, index){
                idx = "goal" + (index+1);
                data.quarter[idx] = goal.progress;
              });
              callback(null,goals);
            }
          });
        }
      });
    },
    function(callback){
      var cond = {
        'assignedTo': user,
        'belongsTo': user,
        // 'due_by': {'$gte':moment().toISOString()},
        'active':false
      };
      Todo.count(cond).populate('').exec(function (err, todoDone) {
        if (err) {
          console.log('connot find done todos');
          console.log(errorHandler.getErrorMessage(err));
          callback(err);
        } else {
          data.todo.completed = todoDone;
          callback(todoDone);
        }
      });
    },],
      function(err, results){
        // console.log(err);
        if(err){ callbackfn(null);}
        else{callbackfn(data);}
        // var resArray = {};
        // results.forEach(function(element){
        //   resArray = _.extend(resArray, element);
        // });
    });
}

/**
 * Article middleware
 */
exports.todoByID = function (req, res, next, id) {
  // console.log(id);
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Todo is invalid'
    });
  }
  Todo.findById(id).populate('belongsTo', 'displayName').exec(function (err, todoC) {
    if (err) {
      return next(err);
    } else if (!todoC) {
      return res.status(404).send({
        message: 'No Todo with that identifier has been found'
      });
    }
    req.todo = todoC;
    next();
  });
};
