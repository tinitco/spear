'use strict';

var todo = require('../controllers/todo.server.controller');

module.exports = function(app) {
  // Routing logic   
  // ...
  app.route('/api/todo')
    .get(todo.list)
    .post(todo.create);

  // Single article routes
  app.route('/api/todo/:todoId')
    .get(todo.read)
    .put(todo.update)
    .delete(todo.delete);

  app.route('/api/todoSearch/:todoId')
    .put(todo.updateReportIn);
  // Finish by binding the article middleware
  app.param('todoId', todo.todoByID);
};
