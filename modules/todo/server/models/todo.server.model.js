'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Todo Schema
 */
var TodoSchema = new Schema({
  // Todo model fields   
  // ...
  created: {
    type: Date,
    default: Date.now
  },
  lastModified: {
    type: Date,
    default: Date.now
  },
  active: {
    type: Boolean,
    default: true
  },
  important: {
    type: Boolean,
    default: false
  },
  reportInmonth: {
    type: Boolean,
    default: false
  },
  priority: {
    type: Boolean,
    default: false
  },
  precedence: {
    type: Number,
    default: 0
  },
  due_by: {
    type: Date,
    // default: Date.now
  },
  reminder: {
    type: Date,
    // default: Date.now
  },
  delegate_reminder: {
    type: Date,
    // default: Date.now
  },
  title: {
    type: String,
    trim: true,
    // required: 'Title cannot be blank'
  },
  status: {
    type: String,
    enum: ['notStarted', 'started', 'finished'],
    default: 'notStarted',
    required: 'Please provide one Status'
  },
  account: {
    type: Schema.ObjectId,
    ref: 'Account',
    // required: 'Account cannot be blank'
  },
  spear: {
    type: Schema.ObjectId,
    ref: 'Spear',
    // required: 'You\'ll need to choose an Spear'
  },
  quarterGoal: {
    type: Schema.ObjectId,
    ref: 'QuarterGoals',
    // required: 'You\'ll need to choose an Spear'
  },
  category: {
    type: Schema.ObjectId,
    ref: 'TodoCategories',
    default: null
    // required: 'You\'ll need to choose an Category'
  },
  assignedTo: {
    type: Schema.ObjectId,
    ref: 'User',
    // tags: { type: [String], index: true }
  },
  belongsTo: {
    type: Schema.ObjectId,
    ref: 'User',
    // tags: { type: [String], index: true },
    required: 'User Not set'
  }
});

TodoSchema.index({ assignedTo: 1, belongsTo: 1});
TodoSchema.index({ quarterGoal:1});
TodoSchema.index({ spear: 1});

mongoose.model('Todo', TodoSchema);
