(function () {
  'use strict';

  angular.module('todo')
    .directive('calenderButton', calenderButton)
    .directive('deleteButton', deleteButton)
    .directive('copyButton', copyButton)
    .directive('doneButton', doneButton);
    // .directive('editInPlace', editInPlace);

  // function editInPlace($timeout) {
  //   return {
  //     // restrict: 'A',
  //     template: '<a href="#" editable-text="todo.title" buttons="no" onbeforesave="onEdit({todo: todo})"> {{todo.title | limitTo: 70 || \'New Plan\' }} </a>',
  //     scope:{todo: '=', onEdit: '&'}
  //   };
  // }

  function doneButton($timeout) {
    return {
      // restrict: 'A',
      template: '<md-button class="md-icon-button pull-right" aria-label="Toggle Done"'+
                ' ng-click ="onEdit({todo: todo})">'+
                  '<i class="mdi md-18 mdi-checkbox-marked-outline txt-color-blueLight" ng-if="!todo.active"></i>'+
                  '<i class="mdi md-18 mdi-checkbox-blank-outline txt-color-blueLight" ng-if="todo.active"></i>'+
                  '<md-tooltip>Done</md-tooltip></md-button>',
      scope:{todo: '=', onEdit: '&'}
    };
  }

  function copyButton(){
    return{
      // restrict: 'A',
      template: '<md-button class="md-icon-button pull-right" aria-label =  "Copy to .."'+
                  'ng-click="onEdit({todo: todo})">'+
                      '<i class="mdi mdi-content-copy md-18"></i>'+
                    '<md-tooltip>Copy to ..</md-tooltip>'+
                  '</md-button>',
      scope:{todo: '=', onEdit: '&'}
    };
  }

  function deleteButton() {
    return {
        restrict: 'A',
        template: '<md-button class="md-icon-button" aria-label ="Delete Plan" ng-click="onEdit({todo: todo})">'+
                        '<i class="mdi mdi-delete-variant md-18 txt-color-red"></i>'+
                      '<md-tooltip>Delete</md-tooltip>'+
                    '</md-button>',
        scope:{todo: '=', onEdit: '&'}
    };
  }

  calenderButton.$inject=['$timeout'];

  function calenderButton($timeout){
    return {
      restrict: 'A',
      template: '<md-button class="md-icon-button" aria-label =  "Add to Google Calender"'+
                    'ng-click="onEdit({todo: todo})">'+
                        '<i class="mdi md-18 mdi-calendar-plus txt-color-green"></i>'+
                      '<md-tooltip>Add to Calender</md-tooltip>'+
                    '</md-button>',
      scope:{todo: '=', onEdit: '&'}
    };
  }
})();
