(function () {
  'use strict';

  angular
    .module('todo.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      // .state('home', {
      //   url: '/',
      //   controller: 'SpearController',
      //   controllerAs: 'vm',
      //   templateUrl: 'modules/spear/client/views/spear.client.view.html',
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Spearity :'
      //   }
      // })
      .state('todo', {
        abstract: true,
        url: '/todo',
        template: '<ui-view/>'
      })
      .state('todo.list', {
        url: '',
        templateUrl: 'modules/todo/client/views/list-todo.client.view.html',
        controller: 'TodosListController',
        controllerAs: 'vm',
        data: {
          roles: ['user', 'admin', 'manager'],
          pageTitle: 'Todo List'
        }
      });
      // .state('accounts', {
      //   // abstract: true,
      //   url: '/accounts',
      //   templateUrl: 'modules/accounts/client/views/accounts.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   data: {
      //     roles: ['superAdmin'],
      //     pageTitle : 'Manage Accounts'
      //   }
      // })
      // .state('accounts.edit', {
      //   url: '/:accountId/edit',
      //   templateUrl: 'modules/accounts/client/views/form-account.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     accountResolve: getAccount
      //   },
      //   data: {
      //     roles: ['user', 'admin'],
      //     pageTitle: 'Edit Account {{ accountResolve.title }}'
      //   }
      // })
      // .state('accounts.view', {
      //   url: '/:accountId',
      //   templateUrl: 'modules/accounts/client/views/view-account.client.view.html',
      //   controller: 'AccountsController',
      //   controllerAs: 'vm',
      //   resolve: {
      //     accountResolve: getAccount
      //   },
      //   data:{
      //     pageTitle: 'Account {{ accountResolve.title }}'
      //   }
      // });
  }

  getTodo.$inject = ['$stateParams', 'TodoService'];

  function getTodo($stateParams, TodoService) {
    return TodoService.get({
      todoId: $stateParams.todoId
    }).$promise;
  }

  newTodo.$inject = ['TodoService'];

  function newTodo(TodoService) {
    return new TodoService();
  }
})();
