(function () {
  'use strict';

  angular
    .module('todo.services')
    .factory('TodoService', TodoService)
    .factory('TodoSearchService', TodoSearchService)
    .factory('TodoFunctions', TodoFunctions);

  TodoService.$inject = ['$resource'];

  function TodoService($resource) {
    return $resource('./api/todo/:todoId', {
      todoId: '@_id'
    },{
      update: { method: 'PUT' }
    });
  }

  TodoSearchService.$inject = ['$resource'];

  function TodoSearchService($resource) {
    return $resource('./api/todoSearch/:todoId', {
      todoId: '@_id'
    },{
      update: { method: 'PUT' }
    });
  }

  TodoFunctions.$inject = ['moment', 'TodoService', 'SpearService',
  'UserCalendarEventService', 'Notification'];

  function TodoFunctions(moment, TodoService, SpearService,
    UserCalendarEventService, Notification) {
    var copy = function (todo, destination, due_by, assign_to){
      var ac = new TodoService();
      ac.title = todo.title;
      // ac.type = data;
      if (destination === 'To-Do'){
        ac.due_by = moment(due_by).format("YYYY-MM-DD");
        if(assign_to){ac.assignedTo = assign_to;}
        ac.$save({}, function () {
          Notification.success('Task Successfully Copied');
        }, function(response){
          Notification.error('Error Copying the task: '+ response);
        });
      }else{
        var spear = SpearService.query({ type:destination.toLowerCase(),
          date:moment(due_by).format("YYYY-MM-DD")}, function(){
            if(!todo.spear && !todo.quarterGoal){ // i.e this is a list todo item 
              todo.spear = spear[0]._id;
              todo.$update({}, function () {
                Notification.success("Task Moved");
              }, function(response){
                Notification.error("Error moving Task " + response);
              });
            }else{
              ac.spear = spear[0]._id;
              ac.$save({}, function () {
                Notification.success('Task Successfully Copied');
              }, function(response){
                Notification.error('Error Copying the task: '+ response);
              });
            }
        });
      }
    };
    var move = function (scope, todo, destination, due_by){
      var spear = SpearService.query({ type:destination.toLowerCase(),
        date:moment(due_by).format("YYYY-MM-DD")}, function(){
        todo.spear = spear[0]._id;
        todo.$update({}, function () {
          scope.todos.splice( scope.todos.indexOf(todo), 1 );
          // $mdToast.show($mdToast.simple().content("Task moved"));
          Notification.success("Task Moved");
        }, function(response){
          Notification.error("Error moving Task " + response);

          // $mdToast.show($mdToast.simple().content("Error moving Task "+ response));
        });
      });
    };
    var calendar = function (cal, calendarId, start, end){
      var calEvent = new UserCalendarEventService();
      calEvent.calendar = calendarId;
      calEvent.summary = cal.title;
      calEvent.description = cal.description;
      calEvent.start = start.toISOString();
      calEvent.end = end.toISOString();
      calEvent.$save({},function(){
        Notification.success('Task Added to Calendar');
      }, function(err){
        Notification.error('Problem Adding Task to Calendar' + err);
      });
    };
    return {copyPlan:copy,
            moveTodo: move,
            addToCalender: calendar};
  }
})();
