(function () {
  'use strict';

  angular
    .module('todo')
    .controller('NewTodoDialogController', NewTodoDialogController)
    .controller('TodoController', TodoController)
    .config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('ddd, M/D/YYYY');
      };
    });

  TodoController.$inject = ['$scope', '$attrs', '$state', 'Authentication',
   'TodoService','TodoFunctions',  '$mdDialog', '$mdMedia', 'moment', 'SpearService', 'Notification', 'UserCalendarEventService'];

  function TodoController($scope, $attrs, $state, Authentication, TodoService, TodoFunctions,
    $mdDialog, $mdMedia, moment, SpearService, Notification, UserCalendarEventService) {
    $scope.relative = '';
    $scope.goal= false;
    if($attrs.prev){ $scope.relative= 'prev'; }
    $scope.spearUp = false;
    if($attrs.spearup){ $scope.spearUp= true; }
    if($attrs.goal){ $scope.goal= true; }

    var vm = this;
    vm.spear = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    // vm.remove = remove;
    vm.createPlan = createPlan;
    vm.addPlan = addPlan;
    vm.showAnalyseRealign = showAnalyseRealign;
    vm.toggleStatus = toggleStatus;
    vm.showGoalPlans = showGoalPlans;
    vm.addGoalPlan = addGoalPlan;
    // vm.progress = 50;
    vm.class = null;
    vm.todos = [];
    vm.inserted = null;
    vm.spear_nxt = null;

    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.$on('spearChanged', function (event,data) {
      vm.class = data.type;
      vm.spear = data.spear_id;
      vm.spear_nxt = data.nextSpearId;
      var spearId = data.spear_id;
      vm.todos = [];
      // vm.spear = null;
      vm.inserted = null;
      if ($scope.relative === 'prev'){
        vm.spear = data.prevSpearId;
        vm.spear_nxt = null;
      }else if ($scope.spearUp){
        vm.spear = data.spear_up;
      }
      if(vm.spear && !$scope.goal){
        $scope.getTodos();
      }
    });

    $scope.getTodos = function(){
      vm.todos = TodoService.query({ spear:vm.spear }, function(){
        // vm.spear = data.spear_id;
        vm.inserted = null;
        vm.todos.forEach(function(todo){$scope.dueByProxy(todo);});
      });
    };

    $scope.init = function(goal){
      showGoalPlans(goal);
    };

     $scope.dropCallback = function(event, index, todo ) {
      var tdTemp = $.grep(vm.todos, function(e){ return e._id === todo._id; })[0];
      // tdTemp = tdTemp[0];
      vm.todos.splice(vm.todos.indexOf(tdTemp), 1);
      vm.todos.splice(index, 0, tdTemp);

      // if (index === tdTemp.precedence){return;}
      for (var i = 0; i < vm.todos.length; i++) {
        if (i === vm.todos[i].precedence){continue;}
        vm.todos[i].precedence = i;
        vm.todos[i].$update(function(){
          $scope.dueByProxy(vm.todos[i]);
        });
      }
    };

    function getOptionsSelecetedType(){
      if($scope.type === 'day'|| $scope.type === "daily"){
        return "Day" ;
      }else if($scope.type === 'week'|| $scope.type === "weekly"){
        return "Week" ;
      }else if($scope.type === 'quarter'|| $scope.type === "quarterly"){
        return "Quarter" ;
      }
      return "To-Do";
    }

    $scope.copyPalnPopoup = function(todo){
      if(!todo){return;}
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      var data = {
          options: ['To-Do', 'Day', 'Week'],
          selected: getOptionsSelecetedType(),
          // category: $scope.todoCategories,
          dt: ''
      };
      $mdDialog.show({
        controller: CopyTodoDialogController,
        templateUrl: 'modules/todo/client/views/copy-todo.client.template.html',
        parent: angular.element(document.body),
        locals:{
          data: data
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo_data) {
        // copyPlan(todo, data.selected, data.due_by);
        TodoFunctions.copyPlan(todo, data.selected, data.due_by);
      });
    };

    $scope.toggleCheck = function(plan) {
      // $scope.loading = true;
      plan.active=!plan.active;
      $scope.updatePlan(plan);
    };


    $scope.addToCalender = function(todo) {
      var data = {
          dt: ''
      };
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: AddToCalendarDialogController,
        templateUrl: 'modules/todo/client/views/add-to-calendar.client.template.html',
        parent: angular.element(document.body),
        locals:{
          data: data,
          todo: todo
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo_data) {
        todo.description = data.description;
        TodoFunctions.addToCalender(todo, data.calendar.calendarId, data.start, data.end);
        // addToCalender(todo, data.calendar.calendarId, data.start, data.end);
      });
    };

    // function addToCalender(todo, calendarId, start, end){
    //   var calEvent = new UserCalendarEventService();
    //   calEvent.calendar = calendarId;
    //   calEvent.summary = todo.title;
    //   calEvent.description = todo.title;
    //   calEvent.start = start.toISOString();
    //   calEvent.end = end.toISOString();
    //   calEvent.$save({},function(){
    //     Notification.success('Task Added to Calendar');
    //   }, function(err){
    //     Notification.error('Problem Adding Task to Calendar' + err);
    //   });
    // }

    $scope.updatePlan = function(todo, data){
      if(todo === vm.inserted){
        if(todo.title !== ""){
          todo.$save({},function(){
            $scope.dueByProxy(todo);
            vm.inserted = null;
          });
        }else{
          // $scope.deletePlan(todo);
          removePlanFromList(todo);
          vm.inserted = null;
        }
      }else{
        if(data === "" || todo.title === ""){
          $scope.deletePlan(todo);
        }else{
          todo.$update(function(){
          $scope.dueByProxy(todo);
          });
        }
      }
    };

    $scope.addEditTodoDialog = function(todo){
      var text = todo.title;
      // todo.due_by = moment($('#datePicker .md-datepicker-input').val()).toDate();
      // $scope.dueByProxy(todo);
      // if (text === ''){text = null;}
      // console.log($('#datePicker').innerHtml());
      $mdDialog.show({
        controller: NewTodoDialogController,
        templateUrl: 'modules/todo/client/views/edit-todo.client.template.html',
        parent: angular.element(document.body),
        locals:{
          todo: todo,
          data: {
            options: ['To-Do', 'Day', 'Week','Quarter'],
            selected: getOptionsSelecetedType(),
            dt: ''
          },
        },
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(todo_data) {
        $scope.updatePlan(todo);
        if(todo_data.refresh){
          vm.goal?getGoalPlans(vm.goal):$scope.getTodos();
        }
      }, function(){
        if(!todo._id ||todo.delete){
          removePlanFromList(todo);
          vm.inserted = '';
          // $scope.updateTodo($scope.inserted);
        }
        // $scope.updatePlan(todo);
      });
    };

    $scope.deletePlanConfirm = function(todo){
      var confirm = $mdDialog.confirm()
            .title('Delete Plan ?')
            .textContent('Are you sure you want to Delete the Plan ?')
            .ariaLabel('Delete')
            // .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        $scope.deletePlan(todo);
      }, function() {});
    };

    $scope.deletePlan=function(todo){
      todo.$delete({},function(){
        removePlanFromList(todo);
      });
    };

    function removePlanFromList(todo){
      if($scope.todos && $scope.todos.length){
          $scope.todos.splice($scope.todos.indexOf(todo), 1);
        }else{
          vm.todos.splice(vm.todos.indexOf(todo), 1);
        }
    }

    // function SpearChanged(spear_type){
    //   if (vm.type !== spear_type){
    //     vm.type = spear_type;
    //     updateSpear();
    //   }
    // }

    function addPlanScope(container){
      if(container.length <6 && !vm.inserted){
        vm.inserted = new TodoService();
        vm.inserted.spear = vm.spear;
        vm.inserted.title = "";
        vm.inserted.active = true;
        container.push(vm.inserted);
        $scope.addEditTodoDialog(vm.inserted);
      }else if(container.length >= 6){
        Notification.error('Can\'t have more than 6 plans');
        // $mdDialog.show(
        //   $mdDialog.alert()
        //     .clickOutsideToClose(true)
        //     .title('Too Many Plans')
        //     .textContent('Sorry you cannot have more than Six Plans in a SPEAR.')
        //     .ariaLabel('Too Many Plans')
        //     .ok('Got it!')
        // );
        // dialogs.error("","", {size:'md'});
      }else{
        Notification.error('An Empty Plan already Exists');
        // $mdDialog.show(
        //   $mdDialog.alert()
        //     .clickOutsideToClose(true)
        //     .title('An Empty Plan Exists')
        //     .textContent('You\'ll have to fill that out to add new ones.')
        //     .ariaLabel('Alert Empty Plan')
        //     .ok('Got it!')
        // );
          // dialogs.notify("An Empty Plan Exists","You'll have to fill that out to add new ones.", {size:'md'});
      }
    }

    $scope.createDueByProxy = function(){
      $scope.todos.forEach(function(todo){
          $scope.dueByProxy(todo);
      });
    };

    $scope.dueByProxy = function(todo){
      todo.due_by = moment(todo.due_by).utc().format("ddd, MM/DD/YYYY");
      // if(todo.reminder){todo.reminder = moment(todo.reminder).toDate();}
      // if($scope.todoReadOnly(todo) && todo.delegate_reminder){
      //   todo.delegate_reminder = moment(todo.delegate_reminder).toDate();
      // }
    };

    function addPlan(){
      if (!vm.spear){ return; }
      addPlanScope(vm.todos);
    }

    function showGoalPlans(goal){
      if(!goal){return;}
      if (vm.goal && vm.goal._id === goal._id){return;}
      getGoalPlans(goal);
    }

    function getGoalPlans(goal) {
      vm.todos = TodoService.query({ goal:goal._id }, function(){
        vm.goal = goal;
        vm.inserted = null;
        vm.todos.forEach(function(todo){$scope.dueByProxy(todo);});
      });
    }

    function addGoalPlan(goal){
      if(!vm.goal){return;}
      if(vm.todos.length <6 && !vm.inserted){
        vm.inserted = new TodoService();
        vm.inserted.quarterGoal = vm.goal._id;
        vm.inserted.title = "";
        vm.todos.push(vm.inserted);
        $scope.addEditTodoDialog(vm.inserted);
      }else if(vm.todos.length >= 6){
        Notification.error('Can\'t have more than 6 plans');
        // $mdDialog.show(
        //   $mdDialog.alert()
        //     .clickOutsideToClose(true)
        //     .title('Too Many Plans')
        //     .textContent('Sorry you cannot have more than Six Plans in a SPEAR.')
        //     .ariaLabel('Too Many Plans')
        //     .ok('Got it!')
        // );
        // dialogs.error("","", {size:'md'});
      }else{
        Notification.error('An Empty Plan already Exists');
        // $mdDialog.show(
        //   $mdDialog.alert()
        //     .clickOutsideToClose(true)
        //     .title('An Empty Plan Exists')
        //     .textContent('You\'ll have to fill that out to add new ones.')
        //     .ariaLabel('Alert Empty Plan')
        //     .ok('Got it!')
        // );
          // dialogs.notify("An Empty Plan Exists","You'll have to fill that out to add new ones.", {size:'md'});
      }
    }


    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
      if (vm.inserted){
        $(".plan-table li .editable-click").last().click();
      }
    });

    function showAnalyseRealign(){}

    // update
    function toggleStatus(ev, todo) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
            .title('Change TODO Status?')
            .textContent('Are you sure you want to change the Todo Status?')
            .ariaLabel('Change Status')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        todo.active =!todo.active;
        todo.$update(function(){
          $scope.dueByProxy(todo);
        });
      }, function() {});
    }

    // create
    function createPlan(ev) {
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewTodoDialogController,
        templateUrl: 'modules/todo/client/views/new-todo.client.template.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo) {
        // $scope.status = 'You said the information was "' + answer + '".';
        var ac = new TodoService();
        ac.title = todo.todoName.$viewValue;
        ac.type = todo.todoType.$viewValue;
        // ac.billing_cycle = todoType.todoBillingCycle.$viewValue;
        ac.$save({}, function () {
          $scope.dueByProxy(todo);
          vm.todos.push(ac);
        });
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    }

    // Delete  existing Account
    function remove(ev, todo) {
      var confirm = $mdDialog.confirm()
            .title('Delete Plan ?')
            .textContent('Are you sure you want to Delete the Plan ?')
            .ariaLabel('Delete')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        todo.$remove(
          vm.todos.splice(vm.todos.indexOf(todo), 1)
        );
      }, function() {});
    }

    // function getWeek(day){
    //   var week = parseInt(day.week()%13);
    //   week = week === 0?13:week;
    //   return week;
    // }

    // function updateProgress() {
    //   var d = moment(vm.date);
    //   vm.day_num = d.day();
    //   vm.week_num = getWeek(d);
    //   vm.quarter_num = d.quarter();
    // }

    // function nextSpear(){
    //   vm.relative = 'next';
    //   if(vm.type === 'day'|| vm.type === "daily"){
    //     vm.date = moment(vm.date)
    //       .add(1, 'days').toDate();
    //   }else if(vm.type === 'week'|| vm.type === "weekly"){
    //     vm.date = moment(vm.date)
    //       .add(7, 'days').toDate();
    //   }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
    //     vm.date = moment(vm.date)
    //       .add(3, 'months').toDate();
    //   }
    //   dateChanged("");
    // }

    // function prevSpear(){
    //   vm.relative = 'prev';
    //   if(vm.type === 'day'|| vm.type === "daily"){
    //     vm.date = moment(vm.date)
    //       .subtract(1, 'days').toDate();
    //   }else if(vm.type === 'week'|| vm.type === "weekly"){
    //     vm.date = moment(vm.date)
    //       .subtract(7, 'days').toDate();
    //   }else if(vm.type === 'quarter'|| vm.type === "quarterly"){
    //     vm.date = moment(vm.date)
    //       .subtract(3, 'months').toDate();
    //   }
    //   dateChanged('');
    // }

    // function dateChanged(day){
    //   updateProgress();
    //   //var d = $.datepicker.parseDate("MM dd, DD - yy",  $scope.date);
    //   //$scope.date1 = $.datepicker.formatDate( "yy-mm-dd", d);
    //   updateSpear();
    // }

    // function updateSpear() {
    //   vm.date1 = moment(vm.date).format("YYYY-MM-DD");
    //   if(!moment(vm.date1).isValid()){
    //     console.log(vm.date1);
    //     return;
    //   }
    //   var ret = TodoService.query({ type:vm.type, date:vm.date1 }, function(){
    //     if(!vm.spear ||(ret.id !== vm.spear.id)){
    //       vm.spear = ret;
    //       // SpearService.setSpear(vm.spear.id);
    //       // SpearService.setUser(vm.user);
    //       $scope.$broadcast('spearChanged',{ date:vm.date1, user:vm.user });
    //     }
    //   });
    // }

  }
})();

function NewTodoDialogController($scope, $mdDialog, moment, TodoCategoriesService, Users, TodoFunctions, UserCalendarService, todo, data) {
  'use strict';

  $scope.todo = todo;
  $scope.data = data;
  $scope.data.title = todo.title;
  $scope.data.copyTo = todo.due_by;
  $scope.data.assignTo = todo.assignedTo;
  $scope.data.options = ['To-Do', 'Day', 'Week'];


  $scope.users = Users.query({list:'account'}, function(){});

  $scope.todoCategories = TodoCategoriesService.query();

  $scope.minDate = new Date(); // for the copy to function

  $scope.options = {
    step: 30,
    // timeFormat: 'h:i A',
    // scrollDefault: 'now'
  };

  // $scope.notAPlan = function(todo){
  //   if(todo.spear)
  // }

  $scope.inlineOptions = {
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  };

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.format = 'EEE, M/d/yyyy';

  $scope.popup1 = {
    opened: false
  };

  $scope.markDone = function(){
    todo.active = !todo.active;
  };

  $scope.dateChanged = function(){
    var d = new Date($scope.data.date);
    d.setHours($scope.data.start.getHours());
    d.setMinutes($scope.data.start.getMinutes());
    $scope.data.start = d;

    var e = new Date($scope.data.date);
    e.setHours($scope.data.end.getHours());
    e.setMinutes($scope.data.end.getMinutes());
    $scope.data.end = e;
  };
  $scope.startChanged = function(){
    var d = new Date($scope.data.end);
    d.setTime($scope.data.start.getTime()+3600000);
    $scope.data.end = d;
    // console.log($scope.data.start);
  };

  $scope.calendars = UserCalendarService.query();
  // $scope.data = {};
  $scope.data.date = moment().format("ddd, MM/DD/YYYY");
  var d = new Date();
    d.setHours( 12 );
    d.setMinutes( 0 );
  $scope.data.start = d;
  $scope.startChanged();
  $scope.refresh = false;


  $scope.copyPlan = function(selected){
    if (!selected){selected = $scope.data.selected;}
    TodoFunctions.copyPlan(todo, selected, $scope.data.date);
    $scope.refresh = true;
    if(!todo.spear && !todo.quarterGoal){
      todo.delete = true;
      $mdDialog.cancel({deleted: todo});
    }
    // TodoFunctions.copyPlan(todo,)
  };

  $scope.deletePlan = function(){
    // var confirm = $mdDialog.confirm()
    //         .title('Delete Todo ?')
    //         .textContent('Are you sure you want to Delete the Todo ?')
    //         .ariaLabel('Delete')
    //         // .targetEvent(ev)
    //         .ok('Yes')
    //         .cancel('Cancel');
    //   $mdDialog.show(confirm).then(function() {
        todo.$delete({},function(){
          delete todo._id;
          delete todo.title;
          $mdDialog.cancel({deleted: todo});
        });
      // }, function() {});

  };

  $scope.assignPlan = function(selected){
    if (!data.assignTo){return;}
    $scope.todo.assignedTo = data.assignTo;
    TodoFunctions.copyPlan(todo, 'To-Do', $scope.data.due_by, data.assignTo);
    // TodoFunctions.copyPlan(todo,)
  };
  $scope.todo.reportInmonthproxy = $scope.todo.reportInmonth;

  $scope.addToReport = function(period){
    $scope.reportPeriod = period;
    if($scope.reportPeriod === 'month'){
      $scope.todo.reportInmonthproxy = !$scope.todo.reportInmonthproxy;
    }
  };

  $scope.saveToReport = function(){
    if($scope.todo.reportInmonthproxy){
      $scope.todo.reportInmonth = true;
    }
  };

  $scope.addToCalender = function(todo){
    if(!$scope.data.calendar){
      $scope.errors = "You have to choose a Calender..";
      return;
    }
    TodoFunctions.addToCalender({title: data.title, description: data.calDescription}, $scope.data.calendar.calendarId,
      $scope.data.start, $scope.data.end);
  };

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel({refresh: $scope.refresh});
  };
  $scope.save = function(answer) {
    answer.refresh = $scope.refresh;
    todo.title = data.title;
    $mdDialog.hide(answer);
  };
}

function AddToCalendarDialogController($scope, $mdDialog, UserCalendarService, data, todo) {
  'use strict';

  $scope.minDate = new Date(); // for the copy to function

  $scope.dateChanged = function(){
    var d = new Date($scope.data.date);
    d.setHours($scope.data.start.getHours());
    d.setMinutes($scope.data.start.getMinutes());
    $scope.data.start = d;

    var e = new Date($scope.data.date);
    e.setHours($scope.data.end.getHours());
    e.setMinutes($scope.data.end.getMinutes());
    $scope.data.end = e;
  };
  $scope.startChanged = function(){
    var d = new Date($scope.data.end);
    d.setTime($scope.data.start.getTime()+3600000);
    $scope.data.end = d;
    // console.log($scope.data.start);
  };

  $scope.calendars = UserCalendarService.query();
  $scope.data = data;
  $scope.data.description = todo.title;

  $scope.data.date = new Date();
  var d = new Date();
    d.setHours( 12 );
    d.setMinutes( 0 );
  $scope.data.start = d;
  $scope.startChanged();

  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    if(!$scope.data.calendar){
      $scope.errors = "You have to choose a Calender..";
      return;
    }else{
      $mdDialog.hide(answer);
    }
  };
}

function CopyTodoDialogController($scope, $mdDialog, TodoCategoriesService, data) {
  'use strict';
  // $scope.todoCategories = TodoCategoriesService.query();
  $scope.data = data;
  // $scope.data.selected = "Week";
  // $scope.selectChanged = function(){
  //   console.log($scope.data.selected);
  // };
  $scope.minDate = new Date();
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
