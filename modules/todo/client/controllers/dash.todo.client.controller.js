(function () {
  'use strict';

  angular
    .module('todo')
    .controller('DashTodoController', DashTodoController);

  DashTodoController.$inject = ['$scope', '$attrs', '$state', 'Authentication',
   'TodoService', 'SpearService', '$mdDialog', '$mdMedia', 'moment'];

  function DashTodoController($scope, $attrs, $state, Authentication, TodoService, SpearService,
    $mdDialog, $mdMedia, moment) {
    $scope.type = 'day';
    if($attrs.type){ $scope.type= $attrs.type; }

    $scope.spear = null;

    $scope.date = moment().format("YYYY-MM-DD");

    if($scope.type){
      var ret = SpearService.query({ type:$scope.type, date:$scope.date}, function(){
          $scope.spear = ret[0];
          $scope.todos = TodoService.query({ spear:$scope.spear._id }, function(){
            // $scope.spear = data.spear_id;
            $scope.todos.forEach(function(todo){$scope.dueByProxy(todo);});
            $scope.inserted = null;
          });
      });
    }
    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.dueByProxy = function(todo){
      todo.due_by = moment(todo.due_by).format("ddd, MM/DD/YYYY");
    };
    
    // $scope.toggleStatus = toggleStatus;
    function getOptionsSelecetedType(){
      if($scope.type === 'day'|| $scope.type === "daily"){
        return "Day" ;
      }else if($scope.type === 'week'|| $scope.type === "weekly"){
        return "Week" ;
      }else if($scope.type === 'quarter'|| $scope.type === "quarterly"){
        return "Quarter" ;
      }
      return "To-Do";
    }

    $scope.addEditTodoDialog = function(todo){
      var text = todo.title;
      // todo.due_by = moment($('#datePicker .md-datepicker-input').val()).toDate();
      // $scope.dueByProxy(todo);
      // if (text === ''){text = null;}
      // console.log($('#datePicker').innerHtml());
      $mdDialog.show({
        controller: NewTodoDialogController,
        templateUrl: 'modules/todo/client/views/edit-todo.client.template.html',
        parent: angular.element(document.body),
        locals:{
          todo: todo,
          data: {
            options: ['To-Do', 'Day', 'Week','Quarter'],
            selected: getOptionsSelecetedType(),
            dt: ''
          },
        },
        clickOutsideToClose:false,
        fullscreen: true
      })
      .then(function(todo_data) {
        $scope.updatePlan(todo);
      }, function(){
        $scope.updatePlan(todo);
      });
    };

    $scope.copyPalnPopoup = function(todo){
      if(!todo){return;}
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      var data = {
          options: ['To-Do', 'Day', 'Week'],
          selected: getOptionsSelecetedType(),
          // category: $scope.todoCategories,
          dt: ''
      };
      $mdDialog.show({
        controller: CopyTodoDialogController,
        templateUrl: 'modules/todo/client/views/copy-todo.client.template.html',
        parent: angular.element(document.body),
        locals:{
          data: data
        },
        // targetEvent: ev,
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo_data) {
        // $scope.status = 'You said the information was "' + answer + '".';
        // console.log(todo_data);
        // console.log(data);
        var ac = new TodoService();
        ac.title = todo.title;
        // ac.type = data;
        if (data.selected === 'To-Do'){
          ac.due_by = moment(data.due_by).format("YYYY-MM-DD");
          ac.$save({}, function () {});
        }else{
          var spear = SpearService.query({ type:data.selected.toLowerCase(),
            date:moment(data.due_by).format("YYYY-MM-DD")}, function(){
            ac.spear = spear[0]._id;
            ac.$save({}, function () {
              Notification.success('Task Successfully Copied');
            });
          });
        }
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    };

    $scope.toggleCheck = function(plan) {
      // $scope.loading = true;
      plan.active=!plan.active;
      $scope.updatePlan(plan);
    };

    $scope.updatePlan = function(todo, data){
      if(todo === $scope.inserted){
        todo.$save({},function(){
          $scope.dueByProxy(todo);
          $scope.inserted = null;
        });
      }else{
        if(data === ""){
          $scope.deletePlan(todo);
        }else{
          todo.$update({},function(){
            $scope.dueByProxy(todo);
          });
        }
      }
    };
  }
})();

function CopyTodoDialogController($scope, $mdDialog, TodoCategoriesService, data) {
  'use strict';
  $scope.data = data;
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
