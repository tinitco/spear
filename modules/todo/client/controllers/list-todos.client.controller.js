(function () {
  'use strict';

  angular
    .module('todo')
    .controller('TodosListController', TodosListController)
    .config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
        // return moment(date).format('ddd, M/D/YYYY');
      };
    });

  TodosListController.$inject = ['$scope', '$mdToast','$q', '$state', 'Authentication',
   'TodoService', 'TodoFunctions', 'TodoCategoriesService', 'Users', '$mdDialog', '$mdMedia',
   'moment', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'SpearService', 'UserSettingsService', 'Notification'];

  function TodosListController($scope, $mdToast, $q, $state, Authentication, TodoService, TodoFunctions,
    TodoCategoriesService, Users, $mdDialog, $mdMedia, moment, DTOptionsBuilder, DTColumnDefBuilder,
     SpearService, UserSettingsService, Notification) {
    var vm = this;
    $scope.displayedtodo = [];
    $scope.itemsByPage=10;
    $scope.itemsOptions = [10,25,50,100];


    $scope.init = function(goal_id)
    {

      $scope.goal_id = goal_id;
      $scope.initized = true;
      $scope.todos = TodoService.query({goal:goal_id},function(){
          $scope.todos_orig = $scope.todos;
          $scope.createDueByProxy();
      });
    };

    $scope.hideCompleted = false;
    $scope.settingsInit = false;

    if(!$scope.initized){
      $scope.todos = TodoService.query({},function(){
        $scope.displayOptions = UserSettingsService.query({}, function(){
          $scope.displayOptions = $scope.displayOptions[0];
          $scope.itemsByPage = $scope.displayOptions.displayLength;
            $scope.settingsInit = true;
            $scope.syncOptionsToCategories();
        });

        $scope.todos_orig = $scope.todos;
        $scope.createDueByProxy();
        if($scope.categorySelected || $scope.hideCompleted){$scope.categoryChanged();}
      });
    }


    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.todoCategories = TodoCategoriesService.query({},function(){
        $scope.syncOptionsToCategories();
    });

    $scope.synced = false;
    $scope.settingscategorySelected = null;

    $scope.syncOptionsToCategories = function(){
      if(!($scope.todoCategories.length && $scope.settingsInit))return;
      $scope.hideCompleted = $scope.displayOptions.hideCompleted;
        if ($scope.displayOptions.category ){
        for(var i=0; i<$scope.todoCategories.length; i++){
          if ($scope.todoCategories[i]._id === $scope.displayOptions.category){
            // $scope.settingscategorySelected = $scope.todoCategories[i];
            $scope.categorySelected = $scope.todoCategories[i];
            $scope.synced= true;
            break;
          }
        }
      }
      if($scope.categorySelected || $scope.hideCompleted){
        $scope.categoryChanged();
      }
    };

    $scope.myId = null;

    $scope.users = Users.query({list:'account'}, function(){
        console.log('users fetched');
      for (var i = 0; i<$scope.users.length;i++){
        if ($scope.users[i].username === Authentication.user.username){
          $scope.myId = $scope.users[i]._id;
          break;
        }
      }
    });


    // function rowCallback(nRow, aData, iDataIndex) {
    //   var val = parseInt($("select[name='todo-data-table_length']").val());
    //   if ($scope.settingscategorySelected){
    //     $scope.categorySelected = $scope.settingscategorySelected;
    //     $scope.settingscategorySelected = null;
    //     $scope.categoryChanged();
    //   }
    //   if (!val || val === $scope.displayOptions.displayLength){return;}
    //   $scope.displayOptions.displayLength = val;
    //   // $('#todo-data-table').dataTable( {
    //   // "iDisplayLength": $scope.displayOptions.displayLength
    //   // });
    //   // vm.dtOptions.withDisplayLength(2);
    //   $scope.updateSettings();
    //   // $update();
    // }

    $scope.max_priority = 5; // setting for priority star rating on the front end.


    $scope.showUser = function(todo) {
      if(!todo.assignedTo){return 'Not Set';}
        var selected = $.grep($scope.users, function( a ) {
            return (a._id === todo.assignedTo);
        });
        return (selected.length) ? selected[0].firstName : 'Not set';
    };

    $scope.showCategory = function(todo) {
      if(!todo.category){return 'Not Set';}
        var selected = $.grep($scope.todoCategories, function( a ) {
            return (a._id === todo.category || a._id === todo.category);
        });
        return (todo.category && selected.length) ? selected[0].title : 'Not set';
    };

    $scope.updateAssignedTo= function (todo, data) {
        if(data){
            todo.assignedTo = data;
            $scope.updateTodo(todo);
        }
    };

    $scope.updateCategory= function (todo, data) {
      if(data){
        todo.category = data;
        $scope.updateTodo(todo);
      }
    };

    $scope.setPriority = function(todo, value) {
        $scope.updateTodo(todo);
    };

    $scope.createDueByProxy = function(){
      $scope.todos.forEach(function(todo){
          $scope.dueByProxy(todo);
      });
    };

    $scope.dueByProxy = function(todo){
      todo.due_by = moment(todo.due_by).utc().format("ddd, MM/DD/YYYY");
      if(todo.reminder){todo.reminder = moment(todo.reminder).utc().format("ddd, MM/DD/YYYY");}
      if($scope.todoReadOnly(todo) && todo.delegate_reminder){
        todo.delegate_reminder = moment(todo.delegate_reminder).utc().format("ddd, MM/DD/YYYY");
      }
    };

    $scope.isReminderSet= function(todo) {
      if($scope.todoReadOnly(todo) && !todo.delegate_reminder){return false;}
      if (!todo.reminder){return false;}
      return true;
    };

    $scope.sortableOptions = {
        update: function(e, ui) {
        },
        'ui-floating': true,
        axis: 'y'
    };


    $scope.toggleCheck = function(todo) {
        $scope.loading = true;
        todo.active=!todo.active;
        $scope.updateTodo(todo);
        if (todo.active !== 1 && $scope.hideCompleted && $scope.todos.indexOf(todo)){
            $scope.todos.splice( $scope.todos.indexOf(todo), 1 );
        }
    };

    $scope.togglePriority = function(todo) {
        $scope.loading = true;
        todo.priority=!todo.priority;
        $scope.updateTodo(todo);
    };

    $scope.updateTodo = function(todo, data){
      if(todo === $scope.inserted){
        if(todo.title !== ""){
          todo.$save({},function(){
            $scope.dueByProxy(todo);
              $scope.inserted = '';
          });
        }else{
          removePlanFromList(todo);
          $scope.inserted = '';
        }
      }else{
          if(data === "" || todo.title === ""){
              $scope.deleteTodo(todo);
          }else{
            todo.$update(function(){
              $scope.dueByProxy(todo);
            });
          }
      }
    };

    $scope.updateSettings = function(){
      if($scope.settingsInit){
        $scope.displayOptions.displayLength = $scope.itemsByPage;
        $scope.displayOptions.$update();
      }
    };

    $scope.updateDueBy = function(todo){
        // var t = moment(todo.due_by).toISOString();
        // if (t){
            // todo.due_by = t;
            $scope.updateTodo(todo);
        // }
    };

    $scope.addTodoInline = function(){
        if($scope.inserted){return;}
        if($scope.todos.length < 6){
            $scope.inserted = new TodoService();
            $scope.inserted.goal = $scope.goal.id;
            $scope.inserted.task = "";
            $scope.inserted.active = "1";
            $scope.todos.push($scope.inserted);
        }else{
            dialogs.error("Too Many Plans","Sorry you cannot have more than Six Plans", {size:'md'});
        }
    };


    // Create
    $scope.addTodo = function(ev) {
      $scope.inserted = new TodoService();
      $scope.inserted.title = '';
      // $scope.inserted.due_by = moment(todo.due_by.$viewValue).toDate();
      // $scope.inserted.assignedTo = todo.assignedTo.$viewValue;
      // $scope.inserted.category = todo.category.$viewValue;
        $scope.todos.push($scope.inserted);
      // $scope.inserted.$save({}, function () {
      //   $scope.dueByProxy($scope.inserted);
      //   $scope.inserted = '';
      // });
      $scope.editTodo($scope.inserted);
    };

    $scope.editTodo = function(todo){
      var data = {
        users: $scope.users,
        category: $scope.todoCategories,
        options: ['To-Do', 'Day', 'Week','Quarter'],
        selected: 'To-Do',
        dt: ''
      };
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      $mdDialog.show({
        controller: NewTodoDialogController,
        templateUrl: 'modules/todo/client/views/edit-todo.client.template.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals:{
          todo: todo,
          data: data
        },
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(dialog_data) {
          $scope.updateTodo(todo);
        // $scope.status = 'You said the information was "' + answer + '".';
      }, function(dialog_data) {
        if(!todo._id ||dialog_data.deleted){
          removePlanFromList(todo);
          $scope.inserted = '';
          // $scope.updateTodo($scope.inserted);
        }
        // $scope.status = 'You cancelled the dialog.';
      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    };

    $scope.deleteTodo=function(todo){
      todo.$delete({},function(){
        removePlanFromList(todo);
      });
    };

    function removePlanFromList(todo){
      $scope.todos.splice( $scope.todos.indexOf(todo), 1 );
      if($scope.todos === $scope.todos_orig){
        return;
      }
      $scope.todos_orig.splice( $scope.todos_orig.indexOf(todo), 1 );
    }


    $scope.copyPalnPopoup = function(todo){
      if(!todo){return;}
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
      var data = {
          options: ['Day', 'Week','Quarter'],
          selected: '',
          // category: $scope.todoCategories,
          dt: ''
      };
      $mdDialog.show({
        controller: CopyTodoDialogController,
        templateUrl: 'modules/todo/client/views/copy-todo.client.template.html',
        parent: angular.element(document.body),
        locals:{
          data: data
        },
        // targetEvent: ev,
        clickOutsideToClose:false,
        fullscreen: useFullScreen
      })
      .then(function(todo_data) {
        TodoFunctions.moveTodo($scope,todo, data.selected, data.due_by);

        // $scope.status = 'You said the information was "' + answer + '".';
        // console.log(todo_data);
        // console.log(data);
        // var ac = new TodoService();
        // ac.title = todo.title;
        // ac.type = data;
        // var spear = SpearService.query({ type:data.selected.toLowerCase(),
        //   date:moment(data.due_by).format("YYYY-MM-DD")}, function(){
        //   todo.spear = spear[0]._id;
        //   todo.$update({}, function () {
        //     $scope.todos.splice( $scope.todos.indexOf(todo), 1 );
        //     // $mdToast.show($mdToast.simple().content("Task moved"));
        //     Notification.success("Task Moved");
        //   }, function(response){
        //     Notification.error("Error moving Task " + response);

        //     // $mdToast.show($mdToast.simple().content("Error moving Task "+ response));
        //   });
        // });

      });
      $scope.$watch(function() {
        return $mdMedia('xs') || $mdMedia('sm');
      }, function(wantsFullScreen) {
        $scope.customFullscreen = (wantsFullScreen === true);
      });
    };

    // $scope.isReadonly = false;
    $scope.todoReadOnly = function(todo){
      if (!(todo.assignedTo === todo.belongsTo || todo.assignedTo === todo.belongsTo._id) &&
        todo.assignedTo === $scope.myId){return true;}
      return false;
    };

    $scope.isDeletegated = function(todo){
      if (!(todo.assignedTo === todo.belongsTo || todo.assignedTo === todo.belongsTo._id) &&
        todo.belongsTo === $scope.myId){return true;}
      return false;
  };

    // update
    $scope.changeStatus = function(ev, todo) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
            .title('Change TO-DO Status?')
            .textContent('Are you sure you want to change the Todo Status?')
            .ariaLabel('Change Status')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        todo.status =  todo.status ==='started'?'finished':'started';
        todo.$update({},function(){
          $scope.dueByProxy(todo);
          Notification.success('Task Status Changed');
        });
      }, function() {});
    };

    $scope.toggleHideCompleted = function(){
        $scope.hideCompleted =!$scope.hideCompleted;
        $scope.displayOptions.hideCompleted = $scope.hideCompleted;
        $scope.updateSettings();
        $scope.categoryChanged();
        //if($scope.hideCompleted){
        //    $scope.filterHidden($scope.todos_orig)
        //}else{
        //    $scope.todos = $scope.todos_orig;
        //}
    };

    $scope.filterHidden = function (todo_scope) {
        var todos = $.grep(todo_scope, function( a ) {
            return (a.active === true);
        });
        return todos;
    };

    $scope.categoryChanged = function(){
      var todo_s = null;
      $scope.displayOptions.category = $scope.categorySelected;
      $scope.updateSettings();
        if(!$scope.categorySelected){
            todo_s = $scope.todos_orig;
        }else{
            todo_s =  $scope.applyCatFilter($scope.todos_orig);
        }
        if($scope.hideCompleted){
            todo_s = $scope.filterHidden(todo_s);
        }
        $scope.todos = todo_s;
      // vm.dtOptions.withDisplayLength($scope.displayOptions.displayLength);

    };

    $scope.applyCatFilter= function(todo_scope){
      if(!todo_scope.length){return todo_scope;}
        var todos = $.grep(todo_scope, function( a ) {
            return (a.category && $scope.categorySelected && (a.category === $scope.categorySelected._id));
        });
        return todos;
    };

  }

})();


function NewTodoPopupDialogController($scope, $mdDialog, data) {
  'use strict';
  $scope.data = data;
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}

function CopyTodoDialogController($scope, $mdDialog, TodoCategoriesService, data) {
  'use strict';
  // $scope.todoCategories = TodoCategoriesService.query();
  $scope.data = data;
  // $scope.data.selected = "Week";
  // $scope.selectChanged = function(){
  //   console.log($scope.data.selected);
  // };
  $scope.minDate = new Date();
  $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.save = function(answer) {
    $mdDialog.hide(answer);
  };
}
