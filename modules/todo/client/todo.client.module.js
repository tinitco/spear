(function (app) {
  'use strict';

  app.registerModule('todo');
  app.registerModule('todo.services');
  app.registerModule('todo.routes', ['ui.router', 'todo.services']);
})(ApplicationConfiguration);
