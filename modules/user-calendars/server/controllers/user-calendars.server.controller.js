'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  _ = require('lodash'),
  UserCalendars = mongoose.model('UserCalendars');

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var path = require('path');

var appDir = path.dirname(require.main.filename);

var SCOPES = ['https://www.googleapis.com/auth/calendar'];
var TOKEN_DIR = appDir + '/credentials/';
var TOKEN_PATH = '';//TOKEN_DIR + 'calendar-nodejs-quickstart.json';

var CLIENT = appDir + '/credentials/client_secret.json';

var errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
// var calendar = google.calendar('v3');
  // console.log(calendar);


function getOAuth(callback){
  // var credentials = null;
  fs.readFile(CLIENT, function processClientSecrets(err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err);
      callback(null);
      return;
    }
    // console.log(content);
    var credentials =JSON.parse(content);
    // console.log(credentials);
    if(!credentials){
      callback(null);
      return;
    }
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    // var redirectUrl = credentials.web.redirect_uris[0];
    var redirectUrl = "https://app.spearity.com/oauth2callback";
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
    // console.log(auth);
    // console.log(oauth2Client);
    callback(oauth2Client);
    // return ;
  });
  // return null;
}

exports.oauthCallback = function(req, res){
  var calendar = google.calendar('v3');
  getOAuth(function(oauth2Client){
    // console.log(req.query);
    oauth2Client.getToken(req.query.code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(req, token, function(){
        // console.log('token');
      });
      getCallenders(req, oauth2Client,function(){});
      return res.status(200).send("All Set. You can close this tab now.");
    });
  }); 
};

function getUserTokenPath(user){
  return TOKEN_DIR + user._id+ '.json';
}

function storeToken(req,token, callback) {
  try {
    fs.mkdirSync(TOKEN_DIR); 
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err;
    }
  }
  var tokenPath = getUserTokenPath(req.user);
  fs.writeFile(tokenPath, JSON.stringify(token), function(err){
    console.log('Token stored to ' + tokenPath);
    console.log('saving fin');
    if (err) {
      console.log('Error saving client secret file: ' + err);
      // callback(null);
      return;
    }
    callback();
  });
}


/**
 * Create a 
 */
exports.create = function (req, res) {
    getOAuth(function(oauth2Client){
    if(!oauth2Client){
      return res.status(400).send({
        message: errorHandler.getErrorMessage("oauth Error")
      });
      // return;
    }
    var authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    });
    res.json({url:authUrl});
  });
};

function getCallenders(req, auth, callback){
  UserCalendars.remove({
    'belongsTo': req.user
  }).exec(function (err, calendars) {
    if (err) {
      console.log(err);
      return;
    }else {
      // res.json(calendars);
      var calendar = google.calendar('v3');
      calendar.calendarList.list({
        auth: auth,
      }, function(err, response) {
        if (err) {
          console.log('The API returned an error: ' + err);
          return;
        }
        var cal = response.items;
        if (cal.length === 0) {
          console.log('No calendars found.');
        } else {
          for (var i = 0; i < cal.length; i++) {
            // console.log(cal[i]);
            var calModel = new UserCalendars(req.body);
            calModel.belongsTo = req.user;
            calModel.summary = cal[i].summary;
            calModel.calendarId = cal[i].id;
            calModel.save(function (err) {
              if (err) {
                console.log("error : "+ err);
                // return res.status(400).send({
                  // message: errorHandler.getErrorMessage(err)
                // });
              } else {
                  // console.log('success');
                // res.json(todo);
              }
            });
          }
          callback();
        }
      });
    }
  });
}


exports.createEvent = function(req, res){
  var calendar = google.calendar('v3');
  getOAuth(function(auth){
    fs.readFile(getUserTokenPath(req.user), function(err, token) {
      if (err) {
        // no we dont .. so wreate a new token and return that to a new site
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
      } else {
        auth.credentials = JSON.parse(token);
        // console.log(req.body.calendar);
        // console.log(req.body);
        var event = {
          'summary': req.body.summary,
          'description': req.body.description,
          'start': {
            'dateTime': req.body.start,
          },
          'end': {
            'dateTime': req.body.end,
          }
        };
        // console.log(event);
        calendar.events.insert({
          auth: auth,
          calendarId: req.body.calendar,
          resource: event,
        }, function(err, event) {
          if (err) {
            // console.log(event);
            console.log("error form api" + err);
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          }
          return res.status(200).send('event Created');
        });


      }
    });
  });
};

/**
 * Show the current 
 */
exports.read = function (req, res) {

};

/**
 * Update a 
 */
exports.update = function (req, res) {
  var calendar = req.calendar;
  calendar.active = req.body.active;
  
  calendar.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      // console.log(calendar.category);
      res.json(calendar);
    }
  });
};

/**
 * Delete an 
 */
exports.delete = function (req, res) {

};

/**
 * List of 
 */
exports.list = function (req, res) {
  var active = [true];
  if (req.query.all && req.query.all === 'true'){
    active = [false, true];
  }
  // console.log(req.query);
  UserCalendars.find({
      // 'quarterGoal':req.query.goal,
      'belongsTo': req.user ,
      'active': {$in: active},
    }).populate('').exec(function (err, calendars) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      }else {
        if (!calendars.length){
          getOAuth(function(auth){
            // Check if we have previously stored a token. 
            fs.readFile(getUserTokenPath(req.user), function(err, token) {
              if (err) {
                // no we dont .. so wreate a new token and return that to a new site
              } else {
                auth.credentials = JSON.parse(token);
                getCallenders(req, auth, function(){
                  UserCalendars.find({
                    'belongsTo': req.user ,
                    'active': {$in: active}
                  }).populate('').exec(function (err, calendars) {
                    if (err) {
                      return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                      });
                    }else {
                      res.json(calendars);
                    }
                  });
                    
                });
                // return res.status(200).send('');
              }
            });
          });
        }else{
          res.json(calendars);
        }
      }
    });
};


exports.calendarByID = function (req, res, next, id) {
  // console.log(id);
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Calendar is invalid'
    });
  }
  UserCalendars.findById(id).populate('belongsTo', 'displayName').exec(function (err, cal) {
    if (err) {
      return next(err);
    } else if (!cal) {
      return res.status(404).send({
        message: 'No Calendar with that identifier has been found'
      });
    }
    req.calendar = cal;
    next();
  });
};