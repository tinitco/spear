'use strict';

module.exports = function (app) {
  // User Routes
  var calendar = require('../controllers/user-calendars.server.controller');

  // Setting up the users profile api
  // app.route('/api/userCalendar').get(calendar);
  app.route('/api/userCalendar').get(calendar.list);
  // app.route('/api/userCalendar').delete(calendar.removeOAuthProvider);
  app.route('/api/userCalendar').post(calendar.create);

  app.route('/api/userCalendarEvent').post(calendar.createEvent);
  
  app.route('/oauth2callback').get(calendar.oauthCallback);
  // app.route('/api/userCalendar/picture').post(calendar.changeProfilePicture);
  app.route('/api/userCalendar/:calanderId')
    .put(calendar.update);

  // Finish by binding the user middleware
  app.param('calanderId', calendar.calendarByID);
};
