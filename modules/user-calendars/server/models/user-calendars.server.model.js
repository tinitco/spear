'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * UserCalendars Schema
 */
var UserCalendarsSchema = new Schema({
  // UserCalendars model fields   
  // ...

  summary: {
    type: String,
    trim: true,
    required: 'Summary cannot be blank'
  },
  active: {
    type: Boolean,
    default: true
  },
  calendarId: {
    type: String,
    trim: true,
    required: 'Id cannot be blank'
  },
  belongsTo: {
    type: Schema.ObjectId,
    ref: 'User',
    required: 'User cannot be blank'
  }
});

mongoose.model('UserCalendars', UserCalendarsSchema);
