(function (app) {
  'use strict';

  app.registerModule('user-calendars');
  app.registerModule('user-calendars.services');
  app.registerModule('user-calendars.routes', ['ui.router', 'user-calendars.services']);
})(ApplicationConfiguration);
