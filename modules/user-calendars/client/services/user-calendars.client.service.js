(function () {
  'use strict';

  angular
    .module('user-calendars.services')
    .factory('UserCalendarService', UserCalendarService)
    .factory('UserCalendarEventService', UserCalendarEventService);

  UserCalendarService.$inject = ['$resource'];

  function UserCalendarService($resource) {
    return $resource('./api/userCalendar/:calId', { 
      calId: '@_id' 
    },{
      update: { method: 'PUT' }
    });
  }

  UserCalendarEventService.$inject = ['$resource'];

  function UserCalendarEventService($resource) {
    return $resource('./api/userCalendarEvent/:eventId', { 
      eventId: '@_id' 
    });
  }
})();
