(function () {
  'use strict';

  angular
    .module('user-calendars')
    .controller('UserCalendarController', UserCalendarController);

  UserCalendarController.$inject = ['$scope','$window', '$state', 'Authentication', 
  'UserCalendarService', '$mdDialog', '$mdMedia'];

  function UserCalendarController($scope, $window, $state, Authentication,
  UserCalendarService, $mdDialog, $mdMedia) {
    var vm = this;

    // vm.accountTypeSelected = null;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    $scope.inserted = null;

    // list 
    $scope.calendars = UserCalendarService.query({all:true});

    $scope.bindAccount = function(){
        var res = new UserCalendarService();
        res.$save({},function(){
            console.log(res);
            $window.open(res.url, '_blank');
            $scope.calendars = UserCalendarService.query({all:true});
        });
    };
    
    $scope.toggleStatus = function (calendar, data) {
        calendar.active = !calendar.active;
        calendar.$update();
    };

    $scope.createCalendarEvent = function(){

    };

  }
})();
