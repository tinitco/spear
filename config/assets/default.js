'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.min.css',
        'public/lib/font-awesome/css/font-awesome.min.css',
        'public/lib/angular-xeditable/dist/css/xeditable.min.css',
        'public/lib/angular-dialog-service/dialogs.min.css',
        'public/lib/angular-material/angular-material.min.css',
        'public/lib/datatables/media/css/dataTables.material.min.css',
        'public/lib/angular-bootstrap/ui-bootstrap-csp.css',
        'public/lib/chartist/dist/chartist.min.css',
        'public/lib/angular-ui-notification/dist/angular-ui-notification.min.css',
        'public/lib/angular-chart.js/dist/angular-chart.min.css',
        'public/lib/mdi/css/materialdesignicons.min.css',
        'public/lib/jquery-timepicker-jt/jquery.timepicker.css',
        'public/lib/animate.css/animate.min.css',
        'public/lib/pikaday/css/pikaday.css',
        'public/lib/angular-loading-bar/build/loading-bar.min.css',
        // 'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
        // 'public/lib/datatables/media/css/jquery.dataTables.css'
      ],
      js: [
        'public/lib/jquery/dist/jquery.min.js',
        'public/lib/datatables/media/js/jquery.dataTables.min.js',
        'public/lib/datatables/media/js/dataTables.material.min.js',
        'public/lib/angular/angular.min.js',
        'public/lib/angular-aria/angular-aria.min.js',
        'public/lib/angular-resource/angular-resource.min.js',
        'public/lib/angular-animate/angular-animate.min.js',
        'public/lib/angular-material/angular-material.min.js',
        'public/lib/angular-messages/angular-messages.min.js',
        'public/lib/angular-ui-router/release/angular-ui-router.min.js',
        'public/lib/angular-ui-utils/ui-utils.min.js',
        'public/lib/angular-xeditable/dist/js/xeditable.min.js',
        'public/lib/angular-file-upload/angular-file-upload.min.js',
        'public/lib/angular-dialog-service/dialogs.min.js',
        'public/lib/angular-dialog-service/dialogs-default-translations.min.js',
        'public/lib/angular-datatables/dist/angular-datatables.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
        'public/lib/moment/min/moment.min.js',
        'public/lib/angular-moment/angular-moment.min.js',
        'public/lib/chartist/dist/chartist.min.js',
        'public/lib/angular-chartist.js/dist/angular-chartist.min.js',
        'public/lib/angular-ui-notification/dist/angular-ui-notification.min.js',
        'public/lib/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
        'public/lib/Chart.js/Chart.min.js',
        'public/lib/angular-chart.js/dist/angular-chart.min.js',
        'public/lib/angular-smart-table/dist/smart-table.min.js',
        'public/lib/jquery-timepicker-jt/jquery.timepicker.min.js',
        'public/lib/angular-jquery-timepicker/src/timepickerdirective.min.js',
        'public/lib/angular-svg-round-progressbar/build/roundProgress.min.js',
        'public/lib/pikaday/pikaday.js',
        'public/lib/pikaday-angular/pikaday-angular.js',
        'public/lib/ng-idle/angular-idle.min.js',
        'public/lib/angular-loading-bar/build/loading-bar.min.js',
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    img: [
      'modules/**/*/img/**/*.jpg',
      'modules/**/*/img/**/*.png',
      'modules/**/*/img/**/*.gif',
      'modules/**/*/img/**/*.svg'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: ['gruntfile.js'],
    gulpConfig: ['gulpfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: ['modules/*/server/config/*.js'],
    policies: 'modules/*/server/policies/*.js',
    views: ['modules/*/server/views/*.html']
  }
};
